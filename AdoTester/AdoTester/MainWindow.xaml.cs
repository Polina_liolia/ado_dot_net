﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AdoTester
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DBConfiguration cf = new DBConfiguration();
            Departments.ItemsSource = DBAccess.GetDepartments();
            Employees.ItemsSource = DBAccess.getEmployees();
        }

        private void btnAddEmployee_Click(object sender, RoutedEventArgs e)
        {

            DataRow newRow = DBAccess.data.Tables["Employees"].NewRow();
            newRow["EmployeesName"] = "Ivanov 4";
            newRow["DepartmantsID"] = "2";
            newRow["Job"] = "-";
            DBAccess.data.Tables["Employees"].Rows.Add(newRow);
            DBAccess.InsetEmployee();
            //DBAccess.UpdateEmployees();
        }
    }
}
