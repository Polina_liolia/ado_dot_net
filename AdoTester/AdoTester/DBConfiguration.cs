﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoTester
{
    class DBConfiguration
    {
        private static string dbConnectionString;
        //private static string dbProviderName;

        //вызывается перед первым вызовом любого метода или обращением к статическому свойству
        static DBConfiguration()
        {
            dbConnectionString = "Data Source=D:\\MyData\\MyData;";
        }

        public static string DbConnectionString
        {
            get { return dbConnectionString; }
        }

        //public static string DbProviderName
        //{
        //    get { return dbProviderName; }
        //}
    }
}
