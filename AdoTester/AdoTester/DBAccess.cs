﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace AdoTester
{
    //all queries texts
    public class DBAccess
    {
        public static DataSet data;//аналог БД

        static DBAccess()
        {
            data = new DataSet("DB");//аналог БД
            data.Locale = System.Globalization.CultureInfo.InvariantCulture;
        }
       
        public static DataView GetDepartments()
        {
            //DataSet - отсоединенный режим работы в ADO.NET
                       
            #region Manual dataset creating
            //creating analog of SQLite DB 'MyData' in memory:
            /* DataTable table = new DataTable("Departments");
             table.Columns.Add("DepartmentsID", typeof(int));
             table.Columns.Add("DepartmentsName", typeof(string));
             table.Columns.Add("DepartmentsLocation", typeof(string));

             //PKs and other constraints:
             DataColumn[] keys = new DataColumn[1]; //PK consists of one column
             keys[0] = table.Columns["DepartmentsID"];
             //keys[1] = table.Columns["DepartmentsName"]; //if there were two columns set as PK
             table.PrimaryKey = keys; //PKs are always presented as array

             //other settings:
             table.Columns["DepartmentsName"].AllowDBNull = false;
             table.Columns["DepartmentsName"].Unique = true;
             table.Columns["DepartmentsName"].Caption = "Название отдела";

             //dataset fulfilling:
             table.Rows.Add(1, "Head Office", "New York");
             table.Rows.Add(2, "IT Dep", "Kharkiv");
             table.Rows.Add(3, "Account Dep", "London");

             //adding table to dataset
             data.Tables.Add(table);*/
            #endregion

            #region Filling in of Data from DB

            string connectionString = DBConfiguration.DbConnectionString;
            SQLiteConnection connection = new SQLiteConnection(connectionString);
            connection.Open();
            SQLiteDataAdapter masterDataAdapter = new SQLiteDataAdapter(DepartmentsSQLs.selectAllTxt(), connection);
            masterDataAdapter.Fill(data, "Departments");

            //saving everything from dataset in XML:
            //data.WriteXml("dataset.txt");

            //reading from existing XML:
            //data.ReadXml("dataset.txt");
            #endregion

            DataView dv = data.Tables["Departments"].DefaultView;
            return dv;
        }

        public static DataView getEmployees()
        {
            string connectionString = DBConfiguration.DbConnectionString;
            SQLiteConnection connection = new SQLiteConnection(connectionString);
            try
            {
                connection.Open();
            SQLiteDataAdapter masterDataAdapter = new SQLiteDataAdapter(EmployeesSQLs.selectAllTxt(), connection);
            masterDataAdapter.Fill(data, "Employees");
            return data.Tables["Employees"].DefaultView;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                connection.Close();
            }
            return null;
        }

        public static void InsetEmployee()
        {
            // create connection
            SQLiteConnection conn =
            new SQLiteConnection(DBConfiguration.DbConnectionString);
            // Create data adapter
            SQLiteDataAdapter da = new SQLiteDataAdapter();
            // create commands
            SQLiteCommand insertCmd = new SQLiteCommand(EmployeesSQLs.insertEmployee(),
                                                        conn);
            //SQLiteCommand getIDCmd = new SQLiteCommand(getID, conn);
            // map parameters
            SQLiteParameter parm1 = insertCmd.Parameters.Add("@EmployeesName",//имя параметра в запросе
                DbType.String, //тип
                100,//размер
                "EmployeesName"//имя столбца из таблицы
                );
            parm1.SourceVersion = DataRowVersion.Current;

            SQLiteParameter parm2 = insertCmd.Parameters.Add("@Job", DbType.String, 100, "Job");
            parm2.SourceVersion = DataRowVersion.Current;
            SQLiteParameter parm3 = insertCmd.Parameters.Add("@Salary", DbType.Double, 0, "Salary");
            parm3.SourceVersion = DataRowVersion.Current;
            SQLiteParameter parm4 = insertCmd.Parameters.Add("@DepartmantsID", DbType.Int32, 0, "DepartmantsID");
            parm1.SourceVersion = DataRowVersion.Current;

            // EmployeeID
            //SQLiteParameter parm = updateCmd.Parameters.Add("@EmployeesID", DbType.Int32, 0, "EmployeesID");
            //parm.SourceVersion = DataRowVersion.Current;

            // Update database
            try
            {
                da.InsertCommand = insertCmd;
                // da.SelectCommand = getIDCmd;
                da.Update(DBAccess.data, "Employees");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }
            /*My variant:
            string connectionString = DBConfiguration.DbConnectionString;
            SQLiteConnection connection = new SQLiteConnection(connectionString);
            connection.Open();

            // SQL to insert into customers
            string insert = EmployeesSQLs.insertCustomer();
            SQLiteCommand cmd = new SQLiteCommand(insert, connection);
           
            cmd.Parameters.AddRange(new SQLiteParameter[]
            {
                new SQLiteParameter(parameterName: "EmployeesName", value: "NewEmployee"),
                new SQLiteParameter(parameterName: "DepartmantsID", value: 1),
                new SQLiteParameter(parameterName: "Job", value: "simpleJob"),
                new SQLiteParameter(parameterName: "Salary", value: 1200)
            });
            cmd.ExecuteNonQuery();
          
            data.AcceptChanges();
            //string getID = @"SELECT @@IDENTITY";//для MS SQL Server
            */
        }

        public static void UpdateEmployees()
        {
            //create connection
            SQLiteConnection conn = new SQLiteConnection(DBConfiguration.DbConnectionString);
            try
            {
                //Create data adapter
                SQLiteDataAdapter da = new SQLiteDataAdapter();

                //create command
                SQLiteCommand updateCmd = new SQLiteCommand(EmployeesSQLs.updateEmployee(), conn);

                //map parameters
                SQLiteParameter parm1 = updateCmd.Parameters.Add("@EmployeesName", DbType.String, 100, "EmployeesName");
                parm1.SourceVersion = DataRowVersion.Current;

                SQLiteParameter parm2 = updateCmd.Parameters.Add("@Job", DbType.String, 100, "Job");
                parm2.SourceVersion = DataRowVersion.Current;
                SQLiteParameter parm3 = updateCmd.Parameters.Add("@Salary", DbType.Double, 0, "Salary");
                parm3.SourceVersion = DataRowVersion.Current;

                SQLiteParameter parm4 = updateCmd.Parameters.Add("@DepartmantsID", DbType.Int32, 0, "DepartmantsID");
                parm1.SourceVersion = DataRowVersion.Current;

                //EmployeeID
                SQLiteParameter parm = updateCmd.Parameters.Add("@EmployeesID", DbType.Int32, 0, "EmployeesID");
                parm.SourceVersion = DataRowVersion.Current;

                //Update database
                da.UpdateCommand = updateCmd;
                DataSet dsCopy = data.GetChanges();
                da.Update(dsCopy, "Employees");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error" + e);
            }
            finally
            {
                //close connection
                conn.Close();
            }
        }
    }
}
