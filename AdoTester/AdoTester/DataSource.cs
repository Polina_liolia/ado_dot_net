﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoTester
{
    public class DepartmentsSQLs
    {
        //DataView - представление данных - для интерфейсных компонентов
        //WinForms: DataGridView, ComboBox
        //WPF - GridView

        public static string selectAllTxt()
        {
            return "select * from Departments";
        }

    }

    public class EmployeesSQLs
    {
        public static string selectAllTxt()
        {
            return "select * from Employees";
        }

        public static string insertEmployee()
        {
            return @"
                INSERT into Employees
                (
                   EmployeesName,
                   DepartmantsID,
                   Job,
                   Salary
                )
                values
                (
                   @EmployeesName,
                   @DepartmantsID,
                   @Job,
                   @Salary
                )
             ";
        }

        public static string updateEmployee()
        {
            return @"
                UPDATE Employees
                SET
                   EmployeesName = @EmployeesName,
                   DepartmantsID = @DepartmantsID,
                   Job = @Job,
                   Salary = @Salary
                 WHERE
                    EmployeesID = @EmployeesID";
        }
    }
}
