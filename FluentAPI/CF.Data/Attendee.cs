﻿using System;

namespace CF.Data
{
    //обычный класс .Net. Классом-сущностью он становится при помощи Fluent Api в методе OnModelCreating
    //класса CodeContext - наследника DbContext
    //Fluent Api - это аналог атрибутов Data Annotation
    public class Attendee
    {
        public int AttendeeTrackingID { get; set; }
        public string LastName { get; set; }
        public DateTime? DateAdded { get; set; }
    }
}
