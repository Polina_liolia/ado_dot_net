﻿using CF.Data;
using System.Data.Entity.ModelConfiguration;

namespace CF.DataAccess
{
    public class AttendeeConfig : EntityTypeConfiguration<Attendee>
    {
        public AttendeeConfig()
        {
            HasKey(p => p.AttendeeTrackingID); ;
            Property(p => p.LastName).IsRequired().HasMaxLength(100);
            //этот столбец будет создан в базе под другим именем ("Created" вместо "DateAdded")
            Property(p => p.DateAdded).IsOptional().HasColumnName("Created").HasColumnType("datetime2");
        }
    }
}
