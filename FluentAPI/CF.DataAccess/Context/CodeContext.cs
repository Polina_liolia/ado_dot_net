﻿using System.Data.Entity;
using CF.Data;

namespace CF.DataAccess
{
    public class CodeContext : DbContext
    {
        public CodeContext()
            : base("data source=PC36-10-Z;initial catalog=MyDbCodeFirstFA;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework")
        {

        }

        public DbSet<Attendee> Attendees { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // modelBuilder.Configurations.Add(new AttendeeConfig()); //здесь хранятся все конфигрурации для класса Attendee
            
            //modelBuilder.Configurations.Add(new AttendeeRenameTableConfig());

            modelBuilder.Configurations.Add(new AttendeeSplittedEntityConfig());
        }
    }
}
