namespace EF_CodeSecond_MyTest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EMPLOYEES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EMPLOYEES()
        {
            EMPLOYEES_TO_GROUPS = new HashSet<EMPLOYEES_TO_GROUPS>();
            EMPLOYEES_TO_GROUPS_HISTORY = new HashSet<EMPLOYEES_TO_GROUPS_HISTORY>();
        }

        [Key]
        public int EMPLOYEES_ID { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string EMPLOYEES_NAME { get; set; }

        [Column(TypeName = "text")]
        public string PHONE { get; set; }

        [Column(TypeName = "text")]
        public string MAIL { get; set; }

        public int? SALARY { get; set; }

        public int? DEPARTMENTS_ID { get; set; }

        public virtual DEPARTMENTS DEPARTMENT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EMPLOYEES_TO_GROUPS> EMPLOYEES_TO_GROUPS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EMPLOYEES_TO_GROUPS_HISTORY> EMPLOYEES_TO_GROUPS_HISTORY { get; set; }
    }
}
