namespace EF_CodeSecond_MyTest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BANKS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BANKS()
        {
            ACCOUNTS = new HashSet<ACCOUNTS>();
        }

        [Column(TypeName = "text")]
        [Required]
        public string BANKS_NAME { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string REGION_INFO { get; set; }

        [Key]
        public int BANKS_ID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACCOUNTS> ACCOUNTS { get; set; }
    }
}
