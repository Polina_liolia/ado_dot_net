namespace EF_CodeSecond_MyTest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class GROUPS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GROUPS()
        {
            EMPLOYEES_TO_GROUPS = new HashSet<EMPLOYEES_TO_GROUPS>();
            EMPLOYEES_TO_GROUPS_HISTORY = new HashSet<EMPLOYEES_TO_GROUPS_HISTORY>();
        }

        [Key]
        public int GROUPS_ID { get; set; }

        [Column(TypeName = "text")]
        public string DESCRIPTION { get; set; }

        [Column(TypeName = "text")]
        public string GROUPS_NAME { get; set; }

        public int? GROUPS_ROLE { get; set; }

        public int? MAIN_GROUPS_ID { get; set; }

        [Column(TypeName = "text")]
        public string GROUPS_TITLE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EMPLOYEES_TO_GROUPS> EMPLOYEES_TO_GROUPS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EMPLOYEES_TO_GROUPS_HISTORY> EMPLOYEES_TO_GROUPS_HISTORY { get; set; }
    }
}
