namespace EF_CodeSecond_MyTest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MY_LOGS
    {
        [Key]
        public int MY_LOGS_ID { get; set; }

        [Column(TypeName = "text")]
        public string ACTION { get; set; }

        [Column(TypeName = "text")]
        public string TBL_NAME { get; set; }

        public int? ID_ROW { get; set; }

        [Column(TypeName = "text")]
        public string LOG_INFO { get; set; }

        [Column(TypeName = "text")]
        public string LOG_TIME { get; set; }

        [Column(TypeName = "text")]
        public string COLUMN_NAME { get; set; }

        [Column(TypeName = "text")]
        public string OLD_VALUE { get; set; }

        [Column(TypeName = "text")]
        public string NEW_VALUE { get; set; }
    }
}
