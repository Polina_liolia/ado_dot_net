namespace EF_CodeSecond_MyTest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PRODUCTS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PRODUCTS()
        {
            ORDERS_POSITIONS = new HashSet<ORDERS_POSITIONS>();
        }

        [Key]
        public int PRODUCTS_ID { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string PRODUCTS_NAME { get; set; }

        public float PRICE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDERS_POSITIONS> ORDERS_POSITIONS { get; set; }
    }
}
