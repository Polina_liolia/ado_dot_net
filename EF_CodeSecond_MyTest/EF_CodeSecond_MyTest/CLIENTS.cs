namespace EF_CodeSecond_MyTest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CLIENTS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CLIENTS()
        {
            ACCOUNTS_TO_CLIENTS = new HashSet<ACCOUNTS_TO_CLIENTS>();
            ORDERS = new HashSet<ORDERS>();
        }

        [Key]
        public int CLIENTS_ID { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string CLIENTS_NAME { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string CLIENTS_PHONE { get; set; }

        [Column(TypeName = "text")]
        public string CLIENTS_MAIL { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACCOUNTS_TO_CLIENTS> ACCOUNTS_TO_CLIENTS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDERS> ORDERS { get; set; }
    }
}
