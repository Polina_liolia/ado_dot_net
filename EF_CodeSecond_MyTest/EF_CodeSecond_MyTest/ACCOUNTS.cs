namespace EF_CodeSecond_MyTest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ACCOUNTS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ACCOUNTS()
        {
            ACCOUNTS_TO_CLIENTS = new HashSet<ACCOUNTS_TO_CLIENTS>();
        }

        [Key]
        public int ACCOUNTS_ID { get; set; }

        [Column(TypeName = "text")]
        public string DESCRIPTION { get; set; }

        public int? BANKS_ID { get; set; }

        public float ACCOUNTS_SUM { get; set; }

        public int ACCOUNT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACCOUNTS_TO_CLIENTS> ACCOUNTS_TO_CLIENTS { get; set; }

        public virtual BANKS BANK { get; set; }
    }
}
