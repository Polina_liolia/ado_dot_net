namespace EF_CodeSecond_MyTest
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ADOTestDataModel : DbContext
    {
        public ADOTestDataModel()
            : base("name=ADOTestDataModel")
        {
        }

        public virtual DbSet<ACCOUNTS> ACCOUNTS { get; set; }
        public virtual DbSet<ACCOUNTS_TO_CLIENTS> ACCOUNTS_TO_CLIENTS { get; set; }
        public virtual DbSet<BANKS> BANKS { get; set; }
        public virtual DbSet<CLIENTS> CLIENTS { get; set; }
        public virtual DbSet<DEPARTMENTS> DEPARTMENTS { get; set; }
        public virtual DbSet<EMPLOYEES> EMPLOYEES { get; set; }
        public virtual DbSet<EMPLOYEES_TO_GROUPS> EMPLOYEES_TO_GROUPS { get; set; }
        public virtual DbSet<EMPLOYEES_TO_GROUPS_HISTORY> EMPLOYEES_TO_GROUPS_HISTORY { get; set; }
        public virtual DbSet<GROUPS> GROUPS { get; set; }
        public virtual DbSet<MY_LOGS> MY_LOGS { get; set; }
        public virtual DbSet<ORDERS> ORDERS { get; set; }
        public virtual DbSet<ORDERS_POSITIONS> ORDERS_POSITIONS { get; set; }
        public virtual DbSet<PRODUCTS> PRODUCTS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ACCOUNTS>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ACCOUNTS>()
                .HasMany(e => e.ACCOUNTS_TO_CLIENTS)
                .WithOptional(e => e.ACCOUNT)
                .WillCascadeOnDelete();

            modelBuilder.Entity<BANKS>()
                .Property(e => e.BANKS_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<BANKS>()
                .Property(e => e.REGION_INFO)
                .IsUnicode(false);

            modelBuilder.Entity<BANKS>()
                .HasMany(e => e.ACCOUNTS)
                .WithOptional(e => e.BANK)
                .WillCascadeOnDelete();

            modelBuilder.Entity<CLIENTS>()
                .Property(e => e.CLIENTS_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<CLIENTS>()
                .Property(e => e.CLIENTS_PHONE)
                .IsUnicode(false);

            modelBuilder.Entity<CLIENTS>()
                .Property(e => e.CLIENTS_MAIL)
                .IsUnicode(false);

            modelBuilder.Entity<CLIENTS>()
                .HasMany(e => e.ACCOUNTS_TO_CLIENTS)
                .WithOptional(e => e.CLIENT)
                .WillCascadeOnDelete();

            modelBuilder.Entity<CLIENTS>()
                .HasMany(e => e.ORDERS)
                .WithOptional(e => e.CLIENT)
                .WillCascadeOnDelete();

            modelBuilder.Entity<DEPARTMENTS>()
                .Property(e => e.DEPARTMENTS_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<DEPARTMENTS>()
                .Property(e => e.REGION_INFO)
                .IsUnicode(false);

            modelBuilder.Entity<DEPARTMENTS>()
                .HasMany(e => e.EMPLOYEES)
                .WithOptional(e => e.DEPARTMENT)
                .WillCascadeOnDelete();

            modelBuilder.Entity<EMPLOYEES>()
                .Property(e => e.EMPLOYEES_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLOYEES>()
                .Property(e => e.PHONE)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLOYEES>()
                .Property(e => e.MAIL)
                .IsUnicode(false);

            modelBuilder.Entity<EMPLOYEES>()
                .HasMany(e => e.EMPLOYEES_TO_GROUPS)
                .WithOptional(e => e.EMPLOYEE)
                .WillCascadeOnDelete();

            modelBuilder.Entity<EMPLOYEES>()
                .HasMany(e => e.EMPLOYEES_TO_GROUPS_HISTORY)
                .WithOptional(e => e.EMPLOYEE)
                .WillCascadeOnDelete();

            modelBuilder.Entity<EMPLOYEES_TO_GROUPS>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<GROUPS>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<GROUPS>()
                .Property(e => e.GROUPS_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<GROUPS>()
                .Property(e => e.GROUPS_TITLE)
                .IsUnicode(false);

            modelBuilder.Entity<GROUPS>()
                .HasMany(e => e.EMPLOYEES_TO_GROUPS)
                .WithOptional(e => e.GROUP)
                .WillCascadeOnDelete();

            modelBuilder.Entity<GROUPS>()
                .HasMany(e => e.EMPLOYEES_TO_GROUPS_HISTORY)
                .WithOptional(e => e.GROUP)
                .WillCascadeOnDelete();

            modelBuilder.Entity<MY_LOGS>()
                .Property(e => e.ACTION)
                .IsUnicode(false);

            modelBuilder.Entity<MY_LOGS>()
                .Property(e => e.TBL_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<MY_LOGS>()
                .Property(e => e.LOG_INFO)
                .IsUnicode(false);

            modelBuilder.Entity<MY_LOGS>()
                .Property(e => e.LOG_TIME)
                .IsUnicode(false);

            modelBuilder.Entity<MY_LOGS>()
                .Property(e => e.COLUMN_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<MY_LOGS>()
                .Property(e => e.OLD_VALUE)
                .IsUnicode(false);

            modelBuilder.Entity<MY_LOGS>()
                .Property(e => e.NEW_VALUE)
                .IsUnicode(false);

            modelBuilder.Entity<ORDERS>()
                .Property(e => e.DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<ORDERS>()
                .Property(e => e.ORDERS_DATE)
                .IsUnicode(false);

            modelBuilder.Entity<ORDERS>()
                .HasMany(e => e.ORDERS_POSITIONS)
                .WithOptional(e => e.ORDER)
                .WillCascadeOnDelete();

            modelBuilder.Entity<PRODUCTS>()
                .Property(e => e.PRODUCTS_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<PRODUCTS>()
                .HasMany(e => e.ORDERS_POSITIONS)
                .WithOptional(e => e.PRODUCT)
                .WillCascadeOnDelete();
        }
    }
}
