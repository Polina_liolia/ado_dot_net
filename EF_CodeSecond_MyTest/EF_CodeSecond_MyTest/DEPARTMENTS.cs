namespace EF_CodeSecond_MyTest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DEPARTMENTS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DEPARTMENTS()
        {
            EMPLOYEES = new HashSet<EMPLOYEES>();
        }

        [Key]
        public int DEPARTMENTS_ID { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string DEPARTMENTS_NAME { get; set; }

        [Column(TypeName = "text")]
        public string REGION_INFO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EMPLOYEES> EMPLOYEES { get; set; }
    }
}
