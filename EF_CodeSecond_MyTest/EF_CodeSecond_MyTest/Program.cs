﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_CodeSecond_MyTest
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ADOTestDataModel model = new ADOTestDataModel())
            {
                
                model.CLIENTS.Add(new CLIENTS()
                {
                    CLIENTS_NAME = "EF_CLIENT4",
                    CLIENTS_PHONE = "+380689621212",
                    CLIENTS_MAIL = "EF_CLIENT@GMAIL.COM"
                });

                CLIENTS client_to_delete = model.CLIENTS.FirstOrDefault(client => client.CLIENTS_NAME.EndsWith("EF_CLIENT"));
                 
                if (client_to_delete != null)
                    model.CLIENTS.Remove(client_to_delete);

                model.SaveChanges();

                foreach (CLIENTS client in model.CLIENTS)
                    Console.WriteLine(client);
                    

                /*
                model.PRODUCTS.AddRange(new PRODUCTS[]
                {
                    new PRODUCTS() { PRODUCTS_NAME = "prod1", PRICE = 10 },
                    new PRODUCTS() { PRODUCTS_NAME = "prod2", PRICE = 30 },
                    new PRODUCTS() { PRODUCTS_NAME = "prod3", PRICE = 100 },
                    new PRODUCTS() { PRODUCTS_NAME = "prod4", PRICE = 200 },
                    new PRODUCTS() { PRODUCTS_NAME = "prod5", PRICE = 45 },
                    new PRODUCTS() { PRODUCTS_NAME = "prod6", PRICE = 70 },
                    new PRODUCTS() { PRODUCTS_NAME = "prod7", PRICE = 12 },
                    new PRODUCTS() { PRODUCTS_NAME = "prod8", PRICE = 1 }
                });
               

                model.ORDERS.AddRange(new ORDERS[]
                {
                    new ORDERS() { CLIENTS_ID = 3 },
                    new ORDERS() { CLIENTS_ID = 4 },
                    new ORDERS() { CLIENTS_ID = 4 }
                });
               
                

                model.ORDERS_POSITIONS.AddRange( new ORDERS_POSITIONS[]
                {
                    new ORDERS_POSITIONS() { ORDERS_ID = 4, ITEM_COUNT = 2, PRODUCTS_ID = 1 },
                    new ORDERS_POSITIONS() { ORDERS_ID = 5, ITEM_COUNT = 3, PRODUCTS_ID = 2 },
                    new ORDERS_POSITIONS() { ORDERS_ID = 6, ITEM_COUNT = 1, PRODUCTS_ID = 3 },
                    new ORDERS_POSITIONS() { ORDERS_ID = 6, ITEM_COUNT = 1, PRODUCTS_ID = 4 }
                });
                model.SaveChanges();
                */

                var query = from prod in model.PRODUCTS
                           orderby prod.PRICE
                           group prod by prod.PRODUCTS_ID into prodGrouped
                           let expensiveProd = prodGrouped.FirstOrDefault()
                           from orderPos in model.ORDERS_POSITIONS
                           where orderPos.PRODUCT  == expensiveProd
                           select orderPos.PRODUCT.PRODUCTS_NAME;
                foreach (var prodName in query)
                    Console.WriteLine(prodName);
            }

        }
    }
}
