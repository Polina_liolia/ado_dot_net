namespace EF_CodeSecond_MyTest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ORDERS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ORDERS()
        {
            ORDERS_POSITIONS = new HashSet<ORDERS_POSITIONS>();
        }

        [Key]
        public int ORDERS_ID { get; set; }

        [Column(TypeName = "text")]
        public string DESCRIPTION { get; set; }

        [Column(TypeName = "text")]
        public string ORDERS_DATE { get; set; }

        public float? TOTAL_COSTS { get; set; }

        public int? CLIENTS_ID { get; set; }

        public virtual CLIENTS CLIENT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDERS_POSITIONS> ORDERS_POSITIONS { get; set; }
    }
}
