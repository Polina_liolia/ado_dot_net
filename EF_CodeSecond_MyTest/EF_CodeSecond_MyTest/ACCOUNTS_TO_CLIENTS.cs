namespace EF_CodeSecond_MyTest
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ACCOUNTS_TO_CLIENTS
    {
        [Key]
        public int ACCOUNTS_TO_CLIENTS_ID { get; set; }

        public int? ACCOUNTS_ID { get; set; }

        public int? CLIENTS_ID { get; set; }

        public int? INDICATION { get; set; }

        public virtual ACCOUNTS ACCOUNT { get; set; }

        public virtual CLIENTS CLIENT { get; set; }
    }
}
