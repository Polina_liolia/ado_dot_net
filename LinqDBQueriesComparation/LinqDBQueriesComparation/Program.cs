﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdventureWorksModel;
using NorthwindModel;

namespace LinqDBQueriesComparation
{
    class Program
    {
        static void Main(string[] args)
        {
            using (AdventureWorksModel.AdventureWorksModel advModel = new AdventureWorksModel.AdventureWorksModel())
            {
                //1. Список должностей, на которых заработную плату получают раз в месяц и список должностей, на которых – дважды

                //SELECT
                //Employee.JobTitle,
                //PayHistory.PayFrequency
                //FROM[HumanResources].[Employee]
                //        AS Employee
                //LEFT JOIN[HumanResources].[EmployeePayHistory]
                //        AS PayHistory
                //ON Employee.BusinessEntityID = PayHistory.BusinessEntityID
                //GROUP BY Employee.JobTitle, PayHistory.PayFrequency
                //ORDER BY PayHistory.PayFrequency

                var query1 = from employee in advModel.Employee
                             join emplPayHist in advModel.EmployeePayHistory
                             on employee.BusinessEntityID equals emplPayHist.BusinessEntityID into emplPayHistJoined
                             from eph in emplPayHistJoined.DefaultIfEmpty()
                            
                             group employee by new { employee.JobTitle, eph.PayFrequency } into emplGrouped
                             orderby emplGrouped.Key.PayFrequency
                             select new
                             {
                                 emplGrouped.Key.JobTitle,
                                 emplGrouped.Key.PayFrequency
                             };

                Console.WriteLine(query1);
                Console.WriteLine();

                foreach (var item in query1)
                {
                    Console.WriteLine(item);
                }
                Console.WriteLine();

                /*
                 --2. Список отделов, в которых работают сотрудники на должностях, оплачиваемых раз в месяц
                SELECT 
                Department.Name AS 'Department name',
                PayHistory.PayFrequency
                FROM [HumanResources].[Department] AS Department
                LEFT JOIN [HumanResources].[EmployeeDepartmentHistory] AS DepartmentHistory
                ON Department.DepartmentID = DepartmentHistory.DepartmentID
                LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
                ON DepartmentHistory.BusinessEntityID = PayHistory.BusinessEntityID
                WHERE PayHistory.PayFrequency = 1
                GROUP BY Department.Name, PayHistory.PayFrequency
                ORDER BY Department.Name;    
             */
                var query2 = from department in advModel.Department
                             join emplDeptHist in advModel.EmployeeDepartmentHistory
                             on department.DepartmentID equals emplDeptHist.DepartmentID into emplDeptHistJoined
                             from edh in emplDeptHistJoined.DefaultIfEmpty()
                             join emplPayHist in  advModel.EmployeePayHistory
                             on edh.BusinessEntityID equals emplPayHist.BusinessEntityID into emplPayHistJoined
                             from eph in emplPayHistJoined.DefaultIfEmpty()...



            }

            using (NorthwindModel.NorthwindModel northwindModel = new NorthwindModel.NorthwindModel())
            {

            }

            
        }
    }
}
