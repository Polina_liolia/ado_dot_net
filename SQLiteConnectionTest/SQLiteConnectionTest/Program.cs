﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteConnectionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            testNativeDriver();
        }

        private static void testNativeDriver()
        {
            createDataBase();
            Console.WriteLine("Press any key");
            createTable();
            Console.WriteLine("Press any key");
            GetTableNames();
            testInsert();
            Console.WriteLine("Press any key");
            testSelect();
        }

        private static void createDataBase()
        {
            const string databaseName = @"d:\MyData\test.db";
            SQLiteConnection.CreateFile(databaseName);
            Console.WriteLine(File.Exists(databaseName) ? "База данных создана" : "Возникла ошибка при создании базы данных");
            Console.ReadKey(true);
        }

        private static void createTable()
        {
            const string databaseName = @"d:\MyData\test.db";
            SQLiteConnection connection =
                new SQLiteConnection(string.Format("Data Source={0};", databaseName));
            SQLiteCommand command =
                new SQLiteCommand("CREATE TABLE example (id INTEGER PRIMARY KEY, value TEXT);", connection);
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
            Console.ReadKey(true);
        }

        private static void GetTableNames()
        {
            const string databaseName = @"d:\MyData\test.db";
            SQLiteConnection connection =
                new SQLiteConnection(string.Format("Data Source={0};", databaseName));
            connection.Open();
            SQLiteCommand command = new SQLiteCommand("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;", connection);
            SQLiteDataReader reader = command.ExecuteReader();

            foreach (DbDataRecord record in reader)
                Console.WriteLine("Таблица: " + record["name"]);
            connection.Close();
            Console.WriteLine("Готово");
            Console.ReadKey(true);
        }

        private static void testInsert()
        {
            const string databaseName = @"d:\MyData\test.db";
            SQLiteConnection connection = new SQLiteConnection(string.Format("Data Source={0};", databaseName));
            connection.Open();
            SQLiteCommand command = new SQLiteCommand("INSERT INTO 'example' ('id', 'value') VALUES (1, 'Вася');", connection);
            command.ExecuteNonQuery();
            connection.Close();
            Console.WriteLine("Готово");
            Console.ReadKey(true);
        }

        private static void testSelect()
        {
            const string databaseName = @"d:\MyData\test.db";
            var connectionString = ConfigurationManager.ConnectionStrings["SQLLite3"].ConnectionString;
            SQLiteConnection connection =
                new SQLiteConnection(connectionString);
            connection.Open();
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM 'example';", connection);
            SQLiteDataReader reader = command.ExecuteReader();
            Console.Write("\u250C" + new string('\u2500', 5) + "\u252C" + new string('\u2500', 60) + "\u2510");
            Console.WriteLine("\n\u2502" + "  id \u2502" + new string(' ', 30) + "value" + new string(' ', 25) + "\u2502");
            Console.Write("\u251C" + new string('\u2500', 5) + "\u253C" + new string('\u2500', 60) + "\u2524\n");
            foreach (DbDataRecord record in reader)
            {
                string id = record["id"].ToString();
                id = id.PadLeft(5 - id.Length, ' ');
                string value = record["value"].ToString();//record["Text"].ToString();//
                string result = "\u2502" + id + " \u2502";
                value = value.PadLeft(60, ' ');
                result += value + "\u2502";
                Console.WriteLine(result);
            }
            Console.Write("\u2514" + new string('\u2500', 5) + "\u2534" + new string('\u2500', 60) + "\u2518");
            connection.Close();
            Console.ReadKey(true);
        }
    }
}
