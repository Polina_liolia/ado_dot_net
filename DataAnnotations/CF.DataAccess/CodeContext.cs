﻿using System.Data.Entity;
using CF.Data;

namespace CF.DataAccess
{
    public class CodeContext : DbContext
    {
        static CodeContext()
        {

            //БД будет каждый раз удаляться и пересоздаваться:
            //Database.SetInitializer(new DropCreateDatabaseAlways<CodeContext>()); //для автоматизации тестирования

            //БД пересоздается, только если модель была изменена:
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<CodeContext>());

            //DropCreateDatabaseAlways<CodeContext>
        }


        public CodeContext(): base("dbContext")
        {           
        }

        public DbSet<Attendee> Attendees { get; set; }
    }
}
