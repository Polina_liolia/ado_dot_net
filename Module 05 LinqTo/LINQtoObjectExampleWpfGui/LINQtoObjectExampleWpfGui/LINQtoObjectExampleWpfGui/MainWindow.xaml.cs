﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LINQtoObjectExampleWpfGui
{
   
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string[] presidents = { "Adams","Arthur","Buchanan","Bush","Carter","Clevelend","Clinton","Coolidge","Eisenhower","Fillmore",
                                          "Ford","Grant","Harrison","Kennedy","Lincoln","Nixon","Obama","Reagan","Roosevelt","Truman","Washington","Wilson"
                                      };
        private ArrayList arrPresidents;
        private List<Department> Departments = new List<Department>();
        private List<Employee> Employees = new List<Employee>();
        private Dictionary<Department, IEnumerable<Employee>> GroupEmployee;

        private void loadDepartments()
        {
            Departments.Add(new Department(10, "Sales", "Hyderabad"));
            Departments.Add(new Department(20, "Purchases", "Mumbai"));
            Departments.Add(new Department(30, "Admin", "Mumbai"));
            Departments.Add(new Department(40, "Accounts", "Mumbai"));
            Departments.Add(new Department(50, "Training", "Hyderabad"));
            Departments.Add(new Department(60, "Stores", "Hyderabad"));
            //
            Departments.Add(new Department(70, "Presidents", "Washington"));
        }
        private void loadEmployees()
        {
            Employees.Add(new Employee(1, "Krishna Prasad", 10, "Manager", 30000));
            Employees.Add(new Employee(2, "Rajesh", 10, "Clerk", 10000));
            Employees.Add(new Employee(3, "Ramesh", 10, "Assistant", 5000));
            Employees.Add(new Employee(4, "Ragesh", 10, "Computer Operator", 4000));

            Employees.Add(new Employee(5, "Murali", 20, "Manager", 30000));
            Employees.Add(new Employee(6, "Anil", 20, "Clerk", 10000));

            Employees.Add(new Employee(7, "Karthik", 30, "Manager", 30000));
            Employees.Add(new Employee(8, "Anirudh", 30, "Assistant", 7000));

            Employees.Add(new Employee(9, "Sarma Chada", 40, "Manager", 15000));
            Employees.Add(new Employee(10, "Anupama", 40, "Assistant", 3000));
            Employees.Add(new Employee(10, "Anirudh", 40, "Accountant", 7000));

            Employees.Add(new Employee(11, "Sailesh", 60, "Store Keeper", 4000));


        }

        private void dgDepartments2_LoadingRowDetails(object sender, DataGridRowDetailsEventArgs e)
        {
            Department curRow = (Department)e.Row.DataContext;
            DataGrid dgEmployees2 = e.DetailsElement.FindName("dgEmployees2") as DataGrid;
            if ((dgEmployees2 != null)&&(curRow!=null))
            {
                dgEmployees2.ItemsSource = curRow.Employees;
            }
        }


        private Dictionary<int, IOrderedEnumerable<Employee>> GroupEmployee2;
        private Dictionary<Department, IOrderedEnumerable<Employee>> GroupEmployee3;
        public MainWindow()
        {
            InitializeComponent();
            loadDepartments();
            loadEmployees();
            //dgDepartments2.ItemsSource = Departments;
            //Без Linq To пример простых преобразующих преобразований : Cast<>() ToList<>() ToArray<>()
            makeManForBining();
            //При помощи Linq: Пример простых порождающих преобразований
            makeEmployees();
            //примеры linq - выборок
            testLinqToObjects();
            makeMatching();//example "from join into select" and example "how to use ToDictionary"
            AddEmployeesToDepartments();
            dgDepartments2.ItemsSource = Departments;
        }

        private void testLinqToObjects()
        {
            #region orderby examples
            /*
            var listEmployees1 = from e in Employees
                                 orderby e.Salary //"ascending" is default
                                 where e.EmployeeName.StartsWith("A")
                                 orderby e.EmployeeNo descending
                                 select e;
            this.dGridTest1.ItemsSource = listEmployees1;
            
            var listEmployees2 = from e in Employees
                                 orderby e.Salary descending//"ascending" is default
                                 where e.Job.Length >3
                                 orderby e.DeptNo ascending, e.Job ascending, e.EmployeeName ascending
                                 select e;
            this.dGridTest2.ItemsSource = listEmployees2;
            */
            #endregion
            #region кванторы
            bool any1 = Enumerable.Empty<string>().Any();
            bool any2 = presidents.Any();
            bool any3 = presidents.Any(s => s.StartsWith("z"));
            bool any4 = presidents.Any(s => s.StartsWith("A"));
            bool all1 = presidents.All(s => s.Length > 5);
            bool all2 = presidents.All(s => s.Length > 3);
            #endregion
            #region сумматоры
            int count1 = presidents.Count();
            int count2 = presidents.Count(s => s.Length == 3);
            int count3 = presidents.Count(s => s.StartsWith("A"));
            string minname = presidents.Min();
            string maxname = presidents.Max();
            double minSalary = Employees.Min(e => e.Salary);
            double maxSalary = Employees.Max(e => e.Salary);
            double averageSalary = Employees.Average(e => e.Salary);
            #endregion
            
            #region selects
            
            var listEmployees4 = presidents.Select((p, i) => new Employee { EmployeeNo = (i + 12), EmployeeName = p, DeptNo = 70, Job = "President", Salary = 25000 }).Union<Employee>(Employees);
            List<Employee> empAll = listEmployees4.ToList<Employee>();
            //вычислить общие затраты на зарплату для каждого подразделения
            var em = from e in empAll
                     group e by new { e.DeptNo }
                         into gEmp
                         where gEmp.Count() > 1
                         select new { gEmp.Key.DeptNo, salary = gEmp.Sum(t => t.Salary), count = gEmp.Count() };
            this.dGridTest1.ItemsSource = em;
            //вычислить среднюю заработную плату по подразделению
            var em2 = from e in empAll
                      group e by new { e.DeptNo }
                         into gEmp
                         let avgsal = (gEmp.Sum(t => t.Salary) / gEmp.Count())
                         select new { avgsal, gEmp.Key.DeptNo };
            //this.dGridTest2.ItemsSource = em2;
            //всем служащим проставить среднюю ЗП
            var em3 = from d in em2
                      join e in empAll on d.DeptNo equals e.DeptNo
                      select new Employee { DeptNo=e.DeptNo, EmployeeNo=e.EmployeeNo,EmployeeName=e.EmployeeName, Job=e.Job, Salary=d.avgsal };
            //this.dGrid3.ItemsSource = em3.ToList<Employee>();
            //у кого больше средней - проставить среднюю вариант 1
            var em4 = from d in em2
                      join e in empAll on d.DeptNo equals e.DeptNo
                      where e.Salary >= d.avgsal
                      select new Employee { DeptNo = e.DeptNo, EmployeeNo = e.EmployeeNo, EmployeeName = e.EmployeeName, Job = e.Job, Salary = d.avgsal };
            //this.dGrid3.ItemsSource = em4.ToList<Employee>();
            //this.dGridTest3.ItemsSource = empAll.ToList<Employee>();
            //у кого больше средней - проставить среднюю вариант 2
            var em5 = from eA in empAll
                      let emplInfo = from d in em2
                                     join e in empAll on d.DeptNo equals e.DeptNo
                                     where e.Salary >= d.avgsal
                                     select e.EmployeeNo
                      where emplInfo.Contains(eA.EmployeeNo)
                      select new Employee { DeptNo = eA.DeptNo, EmployeeNo = eA.EmployeeNo, EmployeeName = eA.EmployeeName, Job = eA.Job, Salary = (from d in em2 where d.DeptNo==eA.DeptNo select d.avgsal).First<double>() };

            //this.dGrid3.ItemsSource = em5.ToList<Employee>();
            //this.dGridTest3.ItemsSource = em4.ToList<Employee>();

            //у кого больше средней - проставить среднюю вариант 1
            var em6 = from d in em2
                      join e in empAll on d.DeptNo equals e.DeptNo
                      where e.Salary < d.avgsal
                      select new Employee { DeptNo = e.DeptNo, EmployeeNo = e.EmployeeNo, EmployeeName = e.EmployeeName, Job = e.Job, Salary = e.Salary };
            //this.dGrid3.ItemsSource = em4.ToList<Employee>();

            this.dGrid3.ItemsSource = empAll.ToList<Employee>();
            this.dGridTest3.ItemsSource = em4.ToList<Employee>().Union<Employee>(em6.ToList<Employee>());



            #endregion
            
        }
        /// <summary>
        /// Пример простых порождающих преобразований
        /// </summary>
        private void makeEmployees()
        {
            var listPresidents1 = from p in presidents
                                  select new Man { Name = p };
            /*
             Аналог:
             foreach (string s in presidents)
            {
                //arrPresidents.Add(s);
                arrPresidents.Add(new Man { Name = s });
            }
            или
                arrPresidents = new ArrayList(presidents.ToList<string>());
             */
            var listPresidents2 = presidents.Select(p => new Man() { Name = p });
            
            //измененный синтаксис преыдущего linq с явным использованием элемента и его индексатора
            //p - строка - тип элемента
            //i - индекс
            var listEmployees = presidents.Select((p, i) => new Employee { EmployeeNo = (i + 12), EmployeeName = p, DeptNo = 70, Job = "President", Salary = 25000 });
            //this.dGrid3.ItemsSource = listEmployees.ToList<Employee>();

            //то же самое, но создается сразу упорядоченная коллекция:
            var listEmployees2 = presidents.Select((p, i) => new Employee { EmployeeNo = (i + 12), EmployeeName = p, DeptNo = 70, Job = "President", Salary = 25000 }).OrderByDescending(e => e.EmployeeName);
            var listEmployees3 = presidents.OrderByDescending(p => p).Select((p, i) => new Employee { EmployeeNo = (i + 12), EmployeeName = p, DeptNo = 70, Job = "President", Salary = 25000 });
            var listEmployees4 = presidents.Select((p, i) => new Employee { EmployeeNo = (i + 12), EmployeeName = p, DeptNo = 70, Job = "President", Salary = 25000 }).Union<Employee>(Employees);
            //this.dGrid3.ItemsSource = listEmployees4.ToList<Employee>();
            /*
            Последующие вызовы OrderBy и OrderByDescending не принимают во внимание порядок созданный предыдущими вызовами этих операций,
             * это означает, что передавать последовательность, возвращенную из OrderBy и OrderByDescending в последующий вызов
             * OrderBy и OrderByDescending не имеет смысла.
             * Если требуется большая ступень упорядочения, чем можно достич OrderBy и OrderByDescending, то нужно последовательно вызвать
             * ThenBy или ThenByDescending.
             * ThenBy или ThenByDescending - создают устойчивую сортировку, то есть такую, которую можно пердавать далее по методам упорядочивания.
             */
            //this.dGrid3.ItemsSource = listEmployees4.ToList<Employee>().OrderByDescending(e => e.EmployeeName).ThenByDescending(e => e.DeptNo);

            //this.dGrid3.ItemsSource = (listEmployees.ToList<Employee>().Concat<Employee>(Employees)).OrderByDescending(e => e.EmployeeName);//.OrderByDescending(e => e.DeptNo);
            List<Employee> empPresidents = listEmployees.ToList<Employee>();
            var listEmployee5 = empPresidents.Take(5);//возвращает указанное число элементов из входной последовательности, начиная с ее начала
            List<Employee> listAllEmployees = listEmployees4.ToList<Employee>();
            var listEmployee6 = listAllEmployees.TakeWhile(e => e.Salary < 25000 && e.DeptNo != 70);//возвращает элементы из входной последовательности
                                                                                   //пока истинно какое-то условие, начиная с начала последовательности
            var listEmployee7 = empPresidents.Skip(5);//пропускает указанное число элементов из входной последовательности, начиная с ее начала, и выыодит остальные
            var listEmployee8 = listAllEmployees.SkipWhile(e => e.DeptNo == 70);//Пропускает элементы из входной последовательности
                                                                               //пока истинно какое-то условие, и выводит остальные
            
        }
        /// <summary>
        /// пример простых преобразующих преобразований : Cast<>() ToList<>() ToArray<>()
        /// </summary>
        private void makeManForBining()
        {
            arrPresidents = new ArrayList();
            foreach (string s in presidents)
            {
                //arrPresidents.Add(s);
                arrPresidents.Add(new Man { Name = s });
            }
            //foreach (object o in arrPresidents)
            //foreach (string o in arrPresidents.Cast<string>())
            foreach (Man m in arrPresidents.Cast<Man>())
            {
                this.dGrid1.Items.Add(m);
            }
            this.dGrid2.ItemsSource = arrPresidents;
            //arrPresidents = new ArrayList(presidents.ToArray<string>());
            //arrPresidents = new ArrayList(presidents.ToList<string>());
        }
        /// <summary>
        ///example "from join into select" and example "how to use ToDictionary"
        /// </summary>
        private void makeMatching()
        {
           //четверг - пятница 

        }

        private void AddEmployeesToDepartments()
        {
            for(int i = 0; i < Departments.Count; i++)
            {
                Departments[i].GroupEmployee = this.GroupEmployee;
            }
        }

    }
    

}
