﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQtoObjectExampleWpfGui
{
    public class Man
    {
       public string Name { set; get; }
    }
    public class Department
    {
        private int deptNo = 0;
        private String deptName = String.Empty;
        private String deptLoc = String.Empty;

        public Department() { }

        public Department(int DeptNo, String DeptName, String DeptLoc)
        {
            deptNo = DeptNo;
            deptName = DeptName;
            deptLoc = DeptLoc;
        }
        

        public int DeptNo
        {
            set { deptNo = value; }
            get { return deptNo; }
        }

        public String DeptName
        {
            set { deptName = value; }
            get { return deptName; }
        }

        public String DeptLoc
        {
            set { deptLoc = value; }
            get { return deptLoc; }
        }

        public Dictionary<Department, IEnumerable<Employee>> GroupEmployee;
        public List<Employee> Employees
        {
            get{
                if (GroupEmployee != null)
                {
                    return GroupEmployee[this].ToList<Employee>();
                }
                else
                    return new List<Employee>();
            }
        }

    }
    public class Employee
    {
        private int employeeNo;
        private String employeeName;
        private int deptNo;
        private String job;
        private Double salary;

        public Employee() { }

        public Employee(int EmployeeNo, String EmployeeName, int DeptNo, String Job, Double Salary)
        {
            employeeNo = EmployeeNo;
            employeeName = EmployeeName;
            deptNo = DeptNo;
            job = Job;
            salary = Salary;
        }
        public int EmployeeNo
        {
            set { employeeNo = value; }
            get { return employeeNo; }
        }
        public String EmployeeName
        {
            set { employeeName = value; }
            get { return employeeName; }
        }
        public int DeptNo
        {
            set { deptNo = value; }
            get { return deptNo; }
        }

        public String Job
        {
            set { job = value; }
            get { return job; }
        }

        public Double Salary
        {
            set { salary = value; }
            get { return salary; }
        }
    }
}
