﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTableSelect
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Simple select
            // Create a table of five different people.
            // ... Store their size and sex.
            DataTable table = new DataTable("Players");
            table.Columns.Add(new DataColumn("Size", typeof(int)));
            table.Columns.Add(new DataColumn("Sex", typeof(char)));

            table.Rows.Add(100, 'f');
            table.Rows.Add(235, 'f');
            table.Rows.Add(250, 'm');
            table.Rows.Add(310, 'm');
            table.Rows.Add(150, 'm');

            // Search for people above a certain size.
            // ... Require certain sex.
            DataRow[] result = table.Select("Size >= 230 AND Sex = 'm'");
            foreach (DataRow row in result)
            {
                Console.WriteLine("{0}, {1}", row[0], row[1]);
            }
            #endregion

            #region Select with date
            //  Create table.
            // ... Add two columns and three rows.
            DataTable table2 = new DataTable("Widgets");
            table2.Columns.Add(new DataColumn("ID", typeof(int)));
            table2.Columns.Add(new DataColumn("Date", typeof(DateTime)));
            table2.Rows.Add(100, new DateTime(2001, 1, 1));
            table2.Rows.Add(200, new DateTime(2002, 1, 1));
            table2.Rows.Add(300, new DateTime(2003, 1, 1));

            // Select by date.
            DataRow[] result2 = table2.Select("Date > #6/1/2001#");

            // Display.
            foreach (DataRow row in result2)
            {
                Console.WriteLine(row["ID"]);
            }
            #endregion

            #region Program that causes EvaluateException
            // Create simple DataTable.
            DataTable table3 = new DataTable();
            table3.Columns.Add("A", typeof(int));
            table3.Rows.Add(1);
            table3.Rows.Add(2);
            table3.Rows.Add(3);

            // Call Select.
            //DataRow[] rows = table3.Select("A"); //error - select expression has to be boolean
            DataRow[] rows = table3.Select("A > 3");
            System.Console.WriteLine(rows.Length);
            #endregion

            #region Row filter
            DataView dataView = table.DefaultView;
            dataView.RowFilter = "id = 10";      // no special character in column name "id"
            dataView.RowFilter = "$id = 10";     // no special character in column name "$id"
            dataView.RowFilter = "[#id] = 10";   // special character "#" in column name "#id"
            dataView.RowFilter = @"[[id\]] = 10"; // special characters in column name "[id]"

            dataView.RowFilter = "Name = 'John'";        // string value
            dataView.RowFilter = "Name = 'John ''A'''";  // string with single quotes "John 'A'"
            dataView.RowFilter = String.Format("Name = '{0}'", "John 'A'".Replace("'", "''"));

            dataView.RowFilter = "Year = 2008";          // integer value
            dataView.RowFilter = "Price = 1199.9";       // float value
            dataView.RowFilter = String.Format(CultureInfo.InvariantCulture.NumberFormat,
                     "Price = {0}", 1199.9f);

            dataView.RowFilter = "Date = #12/31/2008#";          // date value (time is 00:00:00)
            dataView.RowFilter = "Date = #2008-12-31#";          // also this format is supported
            dataView.RowFilter = "Date = #12/31/2008 16:44:58#"; // date and time value

            dataView.RowFilter = String.Format(CultureInfo.InvariantCulture.DateTimeFormat,
                     "Date = #{0}#", new DateTime(2008, 12, 31, 16, 44, 58));

            dataView.RowFilter = "Date = '12/31/2008 16:44:58'"; // if current culture is English
            dataView.RowFilter = "Date = '31.12.2008 16:44:58'"; // if current culture is German

            dataView.RowFilter = "Price = '1199.90'";            // if current culture is English
            dataView.RowFilter = "Price = '1199,90'";            // if current culture is German
            #endregion

            #region Row filter with comparison
            dataView.RowFilter = @"Num = 10";             // number is equal to 10
            dataView.RowFilter = @"Date < #1/1/2008#";    // date is less than 1/1/2008
            dataView.RowFilter = @"Name <> 'John'";       // string is not equal to 'John'
            dataView.RowFilter = @"Name >= 'Jo'";         // string comparison

            dataView.RowFilter = @"Id IN (1, 2, 3)";                    // integer values
            dataView.RowFilter = @"Price IN (1.0, 9.9, 11.5)";          // float values
            dataView.RowFilter = @"Name IN ('John', 'Jim', 'Tom')";     // string values
            dataView.RowFilter = @"Date IN (#12/31/2008#, #1/1/2009#)"; // date time values

            dataView.RowFilter = @"Id NOT IN (1, 2, 3)";  // values not from the list
            dataView.RowFilter = @"Id IN (1, 2, 3)";                    // integer values
            dataView.RowFilter = @"Price IN (1.0, 9.9, 11.5)";          // float values
            dataView.RowFilter = @"Name IN ('John', 'Jim', 'Tom')";     // string values
            dataView.RowFilter = @"Date IN (#12/31/2008#, #1/1/2009#)"; // date time values

            dataView.RowFilter = @"Id NOT IN (1, 2, 3)";  // values not from the list

            dataView.RowFilter = @"Name LIKE 'j*'";       // values that start with 'j'
            dataView.RowFilter = @"Name LIKE '%jo%'";     // values that contain 'jo'

            dataView.RowFilter = @"Name NOT LIKE 'j*'";   // values that don't start with 'j'

            dataView.RowFilter = @"Name LIKE '[*]*'";     // values that starts with '*'
            dataView.RowFilter = @"Name LIKE '[[]*'";     // values that starts with '['
            // select all that starts with the value string (in this case with "*")
            string value = "*";
            //the dataView.RowFilter will be: "Name LIKE '[*]*'"
            dataView.RowFilter = String.Format("Name LIKE '{0}*'", EscapeLikeValue(value));

            #endregion

            #region Row filter Arithmetic and string operators
            dataView.RowFilter = "MotherAge - Age < 20";   // people with young mother
            dataView.RowFilter = "Age % 10 = 0";           // people with decennial birthday

            #endregion

            #region Parent-Child Relation Referencing and Aggregate Functions
            /*
             A parent table can be referenced in an expression using parent column name with Parent. prefix. 
             A column in a child table can be referenced using child column name with Child. prefix.
            The reference to the child column must be in an aggregate function because child relationships
            may return multiple rows. 
            If a table has more than one child relation, the prefix must contain relation name.
             */

            // select people with above-average salary
            dataView.RowFilter = "Salary > AVG(Salary)";

            // select orders which have more than 5 items
            dataView.RowFilter = "COUNT(Child.IdOrder) > 5";

            // select orders which total price (sum of items prices) is greater or equal $500
            dataView.RowFilter = "SUM(Child.Price) >= 500";
            #endregion
        }

        #region Row filter example
        //The following method escapes a text value for usage in a LIKE clause.

        public static string EscapeLikeValue(string valueWithoutWildcards)
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < valueWithoutWildcards.Length; i++)
                {
                    char c = valueWithoutWildcards[i];
                    if (c == '*' || c == '%' || c == '[' || c == ']')
                        sb.Append("[").Append(c).Append("]");
                    else if (c == '\'')
                        sb.Append("''");
                    else
                        sb.Append(c);
                }
                return sb.ToString();
            }

        
           
            #endregion
        }
}

