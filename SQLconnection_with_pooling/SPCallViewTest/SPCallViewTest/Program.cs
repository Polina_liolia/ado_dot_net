﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPCallViewTest
{
    class Program
    {
        static void Main(string[] args)
        {
            DataTable roleLogs = new DataTable();
            DataTable userLogs = new DataTable();
            string connectionString = @"Data Source=PC36-10-Z;Initial Catalog=SampleDB;Integrated Security=True; Pooling = true";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
               
                #region ADD_LOG
                SqlCommand cmdStoredProc_addLog = new SqlCommand()
                {
                    CommandText = "ADD_LOG",
                    CommandType = System.Data.CommandType.StoredProcedure,
                    Connection = conn
                };
                SqlParameter log_inParam1 = new SqlParameter("@ACTION", System.Data.SqlDbType.NChar, 50)
                {
                    Value = "Test from code2"
                };
                SqlParameter log_inParam2 = new SqlParameter("@TBL_NAME", System.Data.SqlDbType.VarChar, 50)
                {
                    Value = "NO TABLE"
                };
                SqlParameter log_inParam3 = new SqlParameter("@ID_ROW", System.Data.SqlDbType.Int)
                {
                    Value = "-1"
                };
                SqlParameter log_inParam4 = new SqlParameter("@LOG_INFO", System.Data.SqlDbType.NVarChar, 100)
                {
                    Value = "NO INFO"
                };
                SqlParameter log_outVal = new SqlParameter("@ID", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                cmdStoredProc_addLog.Parameters.AddRange(new SqlParameter[] { log_outVal, log_inParam1,
                log_inParam2, log_inParam3, log_inParam4});

                cmdStoredProc_addLog.ExecuteNonQuery();
                Console.WriteLine("New log entry's ID is {0}", log_outVal.Value);
                #endregion

                #region ADD_ROLE
                SqlCommand cmdStoredProc_addRole = new SqlCommand()
                {
                    CommandText = "ADD_ROLE",
                    CommandType = System.Data.CommandType.StoredProcedure,
                    Connection = conn
                };
                SqlParameter role_inParam1 = new SqlParameter("@ROLE_NAME", System.Data.SqlDbType.VarChar, 30)
                {
                    Value = "Test role from code"
                };
                SqlParameter role_inParam2 = new SqlParameter("@RULE", System.Data.SqlDbType.NChar, 30)
                {
                    Value = "rule test"
                };
                SqlParameter role_outVal = new SqlParameter("@ID", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                cmdStoredProc_addRole.Parameters.AddRange(new SqlParameter[] { role_outVal, role_inParam1, role_inParam2 });

                cmdStoredProc_addRole.ExecuteNonQuery();
                Console.WriteLine("New role entry's ID is {0}", role_outVal.Value);
                #endregion

                #region ADD_USER
                SqlCommand cmdStoredProc_addUser = new SqlCommand()
                {
                    CommandText = "ADD_USER",
                    CommandType = System.Data.CommandType.StoredProcedure,
                    Connection = conn
                };
                SqlParameter user_inParam1 = new SqlParameter("@USER_NAME", System.Data.SqlDbType.VarChar, 50)
                {
                    Value = "User comes from code"
                };
                SqlParameter user_outVal = new SqlParameter("@ID", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                cmdStoredProc_addUser.Parameters.AddRange(new SqlParameter[] { user_outVal, user_inParam1 });

                cmdStoredProc_addUser.ExecuteNonQuery();
                Console.WriteLine("New user entry's ID is {0}", user_outVal.Value);
                #endregion

                #region ADD_USERS_TO_ROLES
                SqlCommand cmdStoredProc_addUsersToRoles = new SqlCommand()
                {
                    CommandText = "ADD_USERS_TO_ROLES",
                    CommandType = System.Data.CommandType.StoredProcedure,
                    Connection = conn
                };
                SqlParameter usersToRoles_inParam1 = new SqlParameter("@USER_ID", System.Data.SqlDbType.Int)
                {
                    Value = "2"
                };
                SqlParameter usersToRoles_inParam2 = new SqlParameter("@ROLE_ID", System.Data.SqlDbType.Int)
                {
                    Value = "2"
                };
                SqlParameter usersToRoles_outVal = new SqlParameter("@ID", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                cmdStoredProc_addUsersToRoles.Parameters.AddRange(new SqlParameter[] { usersToRoles_outVal,
                usersToRoles_inParam1, usersToRoles_inParam2 });

                cmdStoredProc_addUsersToRoles.ExecuteNonQuery();
                Console.WriteLine("New users to roles entry's ID is {0}", usersToRoles_outVal.Value);
                #endregion
                

                #region ROLE_LOG_VIEW
                using (SqlCommand command = new SqlCommand("SELECT * FROM ROLE_LOG_VIEW ORDER BY[LOG_TIME] DESC", conn))
                {
                    roleLogs.Load(command.ExecuteReader());
                    Console.WriteLine("Select from ROLE_LOG_VIEW:");
                    PrintDataTable(roleLogs);
                }
                #endregion

                #region USER_LOG_VIEW
                using (SqlCommand command = new SqlCommand("SELECT * FROM USER_LOG_VIEW ORDER BY[LOG_TIME] DESC", conn))
                {
                    userLogs.Load(command.ExecuteReader());
                    Console.WriteLine("Select from USER_LOG_VIEW:");
                    PrintDataTable(userLogs);
                }
                #endregion
                

                #region udf_count_user_rule
                using (SqlCommand command = new SqlCommand("select [dbo].[udf_count_user_rule](@role)", conn))
                {
                    command.Parameters.AddWithValue("@role", "U");
                    int users_number = Convert.ToInt32(command.ExecuteScalar());
                    Console.WriteLine("Users, able to update data: {0}", users_number);
                }
                #endregion

                #region udf_show_user_rule
                using (SqlCommand command = new SqlCommand("select * FROM [dbo].[udf_show_user_rule](@id)", conn))
                {
                    command.Parameters.AddWithValue("@id", 1);
                    DataTable userRules = new DataTable();
                    userRules.Load(command.ExecuteReader());
                    Console.WriteLine("User's rules:");
                    PrintDataTable(userRules);
                }
                #endregion

                #region udf_show_user_for_rule
                using (SqlCommand command = new SqlCommand("select * FROM [dbo].[udf_show_user_for_rule] (@rule)", conn))
                {
                    command.Parameters.AddWithValue("@rule", 'U');
                    DataTable user_for_rule = new DataTable();
                    user_for_rule.Load(command.ExecuteReader());
                    Console.WriteLine("Users for rule:");
                    PrintDataTable(user_for_rule);
                }
                #endregion

                conn.Close();
            }
            
        }

        private static void PrintDataTable(DataTable dt)
        {
            foreach (DataRow row in dt.Rows)
            {
                foreach (object obj in row.ItemArray)
                {
                    if (obj is string )
                    {
                        string val = obj as string;
                        Console.WriteLine(val);
                    }
                    else if (obj is int)
                    {
                        int? id = obj as int?;
                        Console.WriteLine(id);
                    }
                        
                }
                Console.WriteLine(Environment.NewLine);
            }
        }
    }
}
