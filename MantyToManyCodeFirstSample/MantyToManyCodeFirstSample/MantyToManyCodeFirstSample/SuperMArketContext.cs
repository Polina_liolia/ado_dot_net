namespace MantyToManyCodeFirstSample
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public class SuperMArketContext : DbContext
    {
        // Your context has been configured to use a 'SuperMArketContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'MantyToManyCodeFirstSample.SuperMArketContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'SuperMArketContext' 
        // connection string in the application configuration file.
        public SuperMArketContext()
            : base("name=SuperMArketContext")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public DbSet<Product> Product { get; set; }
        public DbSet<Vendor> Vendor { get; set; }
        //
        public System.Data.Entity.DbSet<Member> Members { get; set; }
        public System.Data.Entity.DbSet<Comment> Comments { get; set; }
        public System.Data.Entity.DbSet<MemberComment> MemberComments { get; set; }
        //
        public System.Data.Entity.DbSet<User> User { get; set; }
        public System.Data.Entity.DbSet<Email> Email { get; set; }
        //
        public System.Data.Entity.DbSet<UserTest> UserTests { get; set; }
        public System.Data.Entity.DbSet<EmailTest> EmailTests { get; set; }
        //
        protected override void OnModelCreating(DbModelBuilder modelBuilder)   
        {  
            modelBuilder.Entity < Vendor > ()  
                .HasMany(v => v.VendorProducts)  
                .WithMany(p => p.ProductVendors)  
                .Map(  
            m =>  
            {  
                m.MapLeftKey("VendorId");  
                m.MapRightKey("ProductId");  
                m.ToTable("VendorProduct");  
            }); 
                    //
                    // Primary keys
                modelBuilder.Entity<User>().HasKey(q => q.UserID);
                modelBuilder.Entity<Email>().HasKey(q => q.EmailID);
                modelBuilder.Entity<UserEmail>().HasKey(q => 
                    new { 
                        q.UserID, q.EmailID
                    });

                // Relationships
                modelBuilder.Entity<UserEmail>()
                    .HasRequired(t => t.Email)
                    .WithMany(t => t.UserEmails)
                    .HasForeignKey(t => t.EmailID);

                modelBuilder.Entity<UserEmail>()
                    .HasRequired(t => t.User)
                    .WithMany(t => t.UserEmails)
                    .HasForeignKey(t => t.UserID);
                    //
                UserTestEmailTest.RelateFluent(modelBuilder);
        }  
    }
    #region Entity classes
    public class Vendor
    {
        public Vendor()
        {
            VendorProducts = new List<Product>();
        }

        public virtual int VendorId
        {
            get;
            set;
        }
        public virtual string VendorName
        {
            get;
            set;
        }
        public virtual ICollection<Product> VendorProducts
        {
            get;
            set;
        }
    }
    public class Product
    {
        public Product()
        {
            ProductVendors = new List<Vendor>();
        }
        public virtual int ProductId
        {
            get;
            set;
        }
        public virtual string ProductName
        {
            get;
            set;
        }
        public virtual ICollection<Vendor> ProductVendors
        {
            get;
            set;
        }
    }
    public class Member
    {
        public int MemberID { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual ICollection<MemberComment> MemberComments { get; set; }
    }
    public class Comment
    {
        public int CommentID { get; set; }
        public string Message { get; set; }

        public virtual ICollection<MemberComment> MemberComments { get; set; }
    }
    public class MemberComment
    {
        [Key, Column(Order = 0)]
        public int MemberID { get; set; }
        [Key, Column(Order = 1)]
        public int CommentID { get; set; }

        public virtual Member Member { get; set; }
        public virtual Comment Comment { get; set; }

        public int Something { get; set; }
        public string SomethingElse { get; set; }
    }
    public class User
    {
        public int UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public ICollection<UserEmail> UserEmails { get; set; }
    }
    public class Email
    {
        public int EmailID { get; set; }
        public string Address { get; set; }

        public ICollection<UserEmail> UserEmails { get; set; }
    }
    public class UserEmail
    {
        public int UserID { get; set; }
        public int EmailID { get; set; }
        public bool IsPrimary { get; set; }
        public User User { get; set; }
        public Email Email { get; set; }
    }
    #region example2
    public class UserTest
    {
        public int UserTestID { get; set; }
        public string UserTestname { get; set; }
        public string Password { get; set; }

        public ICollection<UserTestEmailTest> UserTestEmailTests { get; set; }

        public static void DoSomeTest(SuperMArketContext context)
        {

            for (int i = 0; i < 5; i++)
            {
                var user = context.UserTests.Add(new UserTest() { UserTestname = "Test" + i });
                var address = context.EmailTests.Add(new EmailTest() { Address = "address@" + i });
            }
            context.SaveChanges();

            foreach (var user in context.UserTests.Include(t => t.UserTestEmailTests))
            {
                foreach (var address in context.EmailTests)
                {
                    user.UserTestEmailTests.Add(new UserTestEmailTest() { UserTest = user, EmailTest = address, n1 = user.UserTestID, n2 = address.EmailTestID });
                }
            }
            context.SaveChanges();
        }
    }

    public class EmailTest
    {
        public int EmailTestID { get; set; }
        public string Address { get; set; }

        public ICollection<UserTestEmailTest> UserTestEmailTests { get; set; }
    }

    public class UserTestEmailTest
    {
        public int UserTestID { get; set; }
        public UserTest UserTest { get; set; }
        public int EmailTestID { get; set; }
        public EmailTest EmailTest { get; set; }
        public int n1 { get; set; }
        public int n2 { get; set; }


        //Call this code from ApplicationDbContext.ConfigureMapping
        //and add this lines as well:
        //public System.Data.Entity.DbSet<yournamespace.UserTest> UserTest { get; set; }
        //public System.Data.Entity.DbSet<yournamespace.EmailTest> EmailTest { get; set; }
        internal static void RelateFluent(System.Data.Entity.DbModelBuilder builder)
        {
            // Primary keys
            builder.Entity<UserTest>().HasKey(q => q.UserTestID);
            builder.Entity<EmailTest>().HasKey(q => q.EmailTestID);

            builder.Entity<UserTestEmailTest>().HasKey(q =>
                new
                {
                    q.UserTestID,
                    q.EmailTestID
                });

            // Relationships
            builder.Entity<UserTestEmailTest>()
                .HasRequired(t => t.EmailTest)
                .WithMany(t => t.UserTestEmailTests)
                .HasForeignKey(t => t.EmailTestID);

            builder.Entity<UserTestEmailTest>()
                .HasRequired(t => t.UserTest)
                .WithMany(t => t.UserTestEmailTests)
                .HasForeignKey(t => t.UserTestID);
        }
    }
    #endregion
    #endregion
}