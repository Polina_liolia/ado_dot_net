﻿//Создание подключения к БД MS SQL Server
using System;
using System.Data.SqlClient; //в этом пространстве имен находится поставщик данных для MS SQLServer 

namespace ADO_NET.ConnectionStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            string conStr = @"Data Source =PC36-10-Z;Initial Catalog=AdventureWorks2008;Integrated Security = True";
            // Следую инструкциям строки подключения следует найти на локальном компьютере экземпляр SQLServer с именем SQLEXPRESS,
            // поискать каталог AdventureWorks2014 и попытаться получить доступ к источнику данных через доверительной подключение,
            // используя для этого вашу учетную запись Microsoft Windows
            SqlConnection connection = new SqlConnection(conStr);

            try
            {
                connection.Open(); //открытие физического подключения к источнику данных
                Console.WriteLine(connection.State);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.Close(); //закрытие физического соединения к источнику данных
                Console.WriteLine(connection.State);
            }
        }
    }
}
