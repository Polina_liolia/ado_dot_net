﻿using System;
using System.Data.SqlClient;

//Получение данных при помощи DataReader

namespace _06_DataReader
{
    class Program
    {
        static void Main(string[] args)
        {
            string conStr = @"Data Source =PC36-10-Z;Initial Catalog=AdventureWorks2008;Integrated Security = True;";
            SqlConnection connection = new SqlConnection(conStr);
            connection.Open();

            SqlCommand cmd = new SqlCommand(
                  "SELECT PersonType, NameStyle, Title, FirstName, MiddleName, LastName FROM Person.Person"
                , connection); //создание команды 

            SqlDataReader reader = cmd.ExecuteReader();
            // С помощью объекта SqlDataReader можно просматривать результаты запроса строка за строкой
            // метод Read() возвращает true  или false в зависимости от того, достигнут ли конец пакета строк
            while (reader.Read())
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    Console.WriteLine(reader.GetName(i) + ": " + reader[i]);
                }
                Console.WriteLine(new string('_', 20));
            }
            reader.Close();
            connection.Close();
        }
    }
}
