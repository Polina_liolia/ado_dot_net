﻿//Создание подключения к БД MS SQL Server используя using
using System;
using System.Data.SqlClient; //в этом пространстве имен находится поставщик данных для MS SQLServer

namespace ConnectionUsing
{
    class Program
    {
        static void Main(string[] args)
        {
            string conStr = @"Data Source=PC36-10-Z;Initial Catalog=AdventureWorks2008;Integrated Security = True";

            using (SqlConnection connection = new SqlConnection(conStr))
            // при использовании объекта SqlConnection в блоке using можно не заботиться о закрытии физического соединения
            // даже если в блоке using генерируется исключительная ситуация
            {
                connection.StateChange += connection_StateChange; // событие, вызываемое при изменении состояния соединения

                try
                {
                    connection.Open();
                    // throw new Exception("error");
                    
                }
                catch(Exception ex)
                {
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
            }// connection.Dispose();
        }

        private static void connection_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            SqlConnection connection = sender as SqlConnection;

            Console.WriteLine();
            Console.WriteLine            //вывод информации о соединении и его состоянии
                (
                "Connection to " + Environment.NewLine +
                "Data Source: " + connection.DataSource + Environment.NewLine +
                "Database: " + connection.Database + Environment.NewLine +
                "State: " + connection.State
                );
        }
    }
}
