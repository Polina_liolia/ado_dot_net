﻿using System;
using System.Data.SqlClient;

//Выполнение команд на вставку и удаление записи в таблице

namespace _05_Inser_Delete_Command
{
    class Program
    {
        static void Main(string[] args)
        {
            string conStr = @"Data Source =PC36-10-Z;Initial Catalog=AdventureWorks2008;Integrated Security = True;";
            SqlConnection connection = new SqlConnection(conStr);
            connection.Open();

            SqlCommand insertCmd = connection.CreateCommand(); //создание команды на вставку записи
            insertCmd.CommandText = "INSERT Person.PhoneNumberType VALUES ('My Type', '01.01.2017')";

            int rowAffected = insertCmd.ExecuteNonQuery(); // выполнение команды на вставку
            Console.WriteLine("INSERT command rows affected: " + rowAffected);

            SqlCommand delCmd = connection.CreateCommand();
            delCmd.CommandText = "DELETE FROM Person.PhoneNumberType WHERE ModifiedDate='01.01.2017'";

            rowAffected = delCmd.ExecuteNonQuery(); // выполнение команды на удаление строки
            Console.WriteLine("DELETE command rows affected: " + rowAffected);

            connection.Close();
        }
    }
}
