﻿//Создание подключения к БД MS SQL Server используя pooling
using System;
using System.Data.SqlClient; //в этом пространстве имен находится поставщик данных для MS SQLServer

   // Технология connection pooling позволяет уменьшить затраты на открытие и закрытие соединения

namespace Pooling
{
    class Program
    {
        static void Main(string[] args)
        {
            string conStr = @"Data Source=PC36-10-Z;Initial Catalog=AdventureWorks2008;Integrated Security = True; Pooling = true";


            DateTime start = DateTime.Now;

            for (int i = 0; i < 1000; i++)
            {
                SqlConnection connection = new SqlConnection(conStr);
                connection.Open();// при включенном пуле физическое соединение не создается, а берется из пула соединений
                connection.Close();// при включенном пуле физическое соединение не разрывается, а помещается в пул

            }
            TimeSpan stop = DateTime.Now - start;
            Console.WriteLine(stop.TotalSeconds);
            Console.WriteLine("Press any key...");
            Console.ReadKey(true);
        }
    }
}
