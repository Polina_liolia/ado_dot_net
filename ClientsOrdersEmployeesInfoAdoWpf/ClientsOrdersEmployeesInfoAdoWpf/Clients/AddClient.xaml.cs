﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Clients
{
    /// <summary>
    /// Interaction logic for AddClient.xaml
    /// </summary>
    public partial class AddClient : Window
    {
        public AddClient()
        {
            InitializeComponent();
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            DataRow newRow = DBAccess.data.Tables["CLIENTS"].NewRow();
            newRow[0] = -1; //temporary id

            string name = txtName.Text;
            if (name != string.Empty)
            {
                newRow[1] = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string phone = txtPhone.Text;
            newRow[2] = phone;

            string mail = txtMail.Text;
            newRow[3] = txtMail.Text;

            //adding row to dataset (with temporary id = -1)
            DBAccess.data.Tables["CLIENTS"].Rows.Add(newRow);
            //updating database (id is generating with identity autoincrement)
            int db_id = DBAccess.insertClients();

            //changing id value in dataset
            DataRow[] row = DBAccess.data.Tables["CLIENTS"].Select(string.Format("CLIENTS_ID = -1"));
            row[0][0] = db_id;
            this.Close();

        }
    }
}
