﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Orders
{
    /// <summary>
    /// Interaction logic for AddOrder.xaml
    /// </summary>
    public partial class AddOrder : Window
    {
        public AddOrder()
        {
            InitializeComponent();
            cboxClient.ItemsSource = DBAccess.data.Tables["CLIENTS"].DefaultView;
            cboxClient.DisplayMemberPath = "CLIENTS_NAME";
            cboxClient.SelectedValuePath = "CLIENTS_ID";
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            DataRow newRow = DBAccess.data.Tables["ORDERS"].NewRow();
            newRow[0] = -1; //temporary id

            string description = txtDescription.Text;
            newRow[1] = description;

            int clientID;
            if (cboxClient.SelectedValue != null)
            {
                clientID = (int)cboxClient.SelectedValue;
                newRow[4] = clientID;
            }
            else
            {
                MessageBox.Show("Client id can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            decimal totalCosts = -1;
            Decimal.TryParse(txtTotalCosts.Text, out totalCosts);
            if (totalCosts != -1 && totalCosts > 0)
            {
                newRow[3] = totalCosts;
            }
            else
            {
                MessageBox.Show("Total costs can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DateTime date = Convert.ToDateTime(txtDate.Text);
            if (date != null)
            {
                newRow[2] = date;
            }
            else
            {
                MessageBox.Show("Date can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            //adding row to dataset (with temporary id = -1)
            DBAccess.data.Tables["ORDERS"].Rows.Add(newRow);
            //updating database (id is generating with identity autoincrement)
            int db_id = DBAccess.insertEmployee();

            ////getting id, generated with identity autoincrement
            // = DBAccess.getIdentity();

            //changing id value in dataset
            DataRow[] row = DBAccess.data.Tables["ORDERS"].Select(string.Format("ORDERS_ID = -1"));
            row[0][0] = db_id;
            this.Close();
        }
    }
}
