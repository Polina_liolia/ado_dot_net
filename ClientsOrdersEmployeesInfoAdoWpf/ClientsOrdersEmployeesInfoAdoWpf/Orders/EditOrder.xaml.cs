﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Orders
{
    /// <summary>
    /// Interaction logic for EditOrder.xaml
    /// </summary>
    public partial class EditOrder : Window
    {
        private DataRow row;
        private EditOrder()
        {
            InitializeComponent();
        }

        public EditOrder(DataRow dr)
        {
            InitializeComponent();
            row = dr;
            this.DataContext = row;
            cboxClient.ItemsSource = DBAccess.data.Tables["CLIENTS"].DefaultView;
            cboxClient.DisplayMemberPath = "CLIENTS_NAME";
            cboxClient.SelectedValuePath = "CLIENTS_ID";

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string description = txtDescription.Text;
            row[1] = description;

            int clientID;
            if (cboxClient.SelectedValue != null)
            {
                clientID = (int)cboxClient.SelectedValue;
                row[4] = clientID;
            }
            else
            {
                MessageBox.Show("Client id can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            decimal totalCosts = -1;
            Decimal.TryParse(txtTotalCosts.Text, out totalCosts);
            if (totalCosts != -1 && totalCosts > 0)
            {
                row[3] = totalCosts;
            }
            else
            {
                MessageBox.Show("Total costs can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DateTime date = Convert.ToDateTime(txtDate.Text);
            if (date != null)
            {
                row[2] = date;
            }
            else
            {
                MessageBox.Show("Date can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DBAccess.updateOrders();
            this.Close();
        }
    }
}
