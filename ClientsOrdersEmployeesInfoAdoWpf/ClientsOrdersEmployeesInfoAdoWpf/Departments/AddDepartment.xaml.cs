﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Departments
{
    /// <summary>
    /// Interaction logic for AddDepartment.xaml
    /// </summary>
    public partial class AddDepartment : Window
    {
        public AddDepartment()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DataRow newRow = DBAccess.data.Tables["DEPARTMENTS"].NewRow();
            newRow[0] = -1; //temporary id

            string name = txtName.Text;
            if (name != string.Empty)
            {
                newRow[1] = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string region_info = txtRegion.Text;
            newRow[2] = region_info;

            //adding row to dataset (with temporary id = -1)
            DBAccess.data.Tables["DEPARTMENTS"].Rows.Add(newRow);
            //updating database (id is generating with identity autoincrement)
            int db_id = DBAccess.insertDepartments();

            //changing id value in dataset
            DataRow[] row = DBAccess.data.Tables["DEPARTMENTS"].Select(string.Format("DEPARTMENTS_ID = -1"));
            row[0][0] = db_id;
            this.Close();
        }
    }
}
