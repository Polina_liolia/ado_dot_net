﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    public class DBAccess
    {
        public static DataSet data;

        static DBAccess()
        {
            data = new DataSet("DB");
            data.Locale = System.Globalization.CultureInfo.InvariantCulture;
            fillDataSet();
        }

        public static void fillDataSet()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                try
                {
                    con.Open();
                    SqlDataAdapter adapter;

                    adapter = new SqlDataAdapter(BanksHelper.selectAll(), con);
                    adapter.Fill(data, "BANKS");

                    adapter = new SqlDataAdapter(AccountsHelper.selectAll(), con);
                    adapter.Fill(data, "ACCOUNTS");

                    // Establish a relationship between the two tables.
                    DataRelation relationBanksAccounts = new DataRelation("BanksAccounts",
                        data.Tables["BANKS"].Columns["BANKS_ID"],
                        data.Tables["ACCOUNTS"].Columns["BANKS_ID"]);
                    data.Relations.Add(relationBanksAccounts);

                    adapter = new SqlDataAdapter(ClientsHelper.selectAll(), con);
                    adapter.Fill(data, "CLIENTS");

                    adapter = new SqlDataAdapter(AccountsToClientsHelper.selectAll(), con);
                    adapter.Fill(data, "ACCOUNTS_TO_CLIENTS");
                    
                    // Establish many-to-many relationship between tables.
                    DataRelation relationAccountsToClients = new DataRelation("Accounts_AccountsToClients",
                        data.Tables["ACCOUNTS"].Columns["ACCOUNTS_ID"],
                        data.Tables["ACCOUNTS_TO_CLIENTS"].Columns["ACCOUNTS_ID"]);
                    data.Relations.Add(relationAccountsToClients);

                    relationAccountsToClients = new DataRelation("Clients_AccountsToClients",
                        data.Tables["CLIENTS"].Columns["CLIENTS_ID"],
                        data.Tables["ACCOUNTS_TO_CLIENTS"].Columns["CLIENTS_ID"]);
                    data.Relations.Add(relationAccountsToClients);


                    adapter = new SqlDataAdapter(OrdersHelper.selectAll(), con);
                    adapter.Fill(data, "ORDERS");

                    // Establish a relationship between the two tables.
                    DataRelation relationOrdersClients = new DataRelation("OrdersClients",
                        data.Tables["CLIENTS"].Columns["CLIENTS_ID"],
                        data.Tables["ORDERS"].Columns["CLIENTS_ID"]);
                    data.Relations.Add(relationOrdersClients);

                    adapter = new SqlDataAdapter(DepartmentsHelper.selectAll(), con);
                    adapter.Fill(data, "DEPARTMENTS");

                    adapter = new SqlDataAdapter(EmployeesHelper.selectAll(), con);
                    adapter.Fill(data, "EMPLOYEES");

                    // Establish a relationship between the two tables.
                    DataRelation relationEmployeesDepatrments = new DataRelation("EmployeesDepatrments",
                        data.Tables["DEPARTMENTS"].Columns["DEPARTMENTS_ID"],
                        data.Tables["EMPLOYEES"].Columns["DEPARTMENTS_ID"]);
                    data.Relations.Add(relationEmployeesDepatrments);

                    adapter = new SqlDataAdapter(OrdersHelper.selectAll(), con);
                    adapter.Fill(data, "PRODUCTS");

                    adapter = new SqlDataAdapter(OrdersPositionsHelper.selectAll(), con);
                    adapter.Fill(data, "ORDERS_POSITIONS");

                    // Establish many-to-many relationship between tables.
                    DataRelation relationOrdersToPositions = new DataRelation("Products_OrdersToPositions",
                        data.Tables["PRODUCTS"].Columns["PRODUCTS_ID"],
                        data.Tables["ORDERS_POSITIONS"].Columns["PRODUCTS_ID"]);
                    data.Relations.Add(relationOrdersToPositions);

                    relationOrdersToPositions = new DataRelation("Orders_OrdersToPositions",
                        data.Tables["ORDERS"].Columns["ORDERS_ID"],
                        data.Tables["ORDERS_POSITIONS"].Columns["ORDERS_ID"]);
                    data.Relations.Add(relationOrdersToPositions);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        

        #region Employees
        public static DataView getEmployees()
        {
            return data.Tables["EMPLOYEES"].AsDataView();
        }

        public static SqlDataAdapter getEmployeesAdapter(SqlConnection conn)
        {
            // Create data adapter
            SqlDataAdapter da = new SqlDataAdapter();

            // insert command
            SqlCommand insertCmd = new SqlCommand(EmployeesHelper.insertEmployee(), conn);
            // map parameters
            //parm = insertCmd.Parameters.Add("@ID", SqlDbType.Int, 0, "EMPLOYEES_ID");
            //parm.SourceVersion = DataRowVersion.Current;
            SqlParameter parm = insertCmd.Parameters.Add("@Name", SqlDbType.Text, 100, "EMPLOYEES_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Phone", SqlDbType.Text, 100, "PHONE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Mail", SqlDbType.Text, 100, "MAIL");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Salary", SqlDbType.Int, 0, "SALARY");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@DeptID", SqlDbType.Int, 0, "DEPARTMENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //delete command
            SqlCommand deleteCmd = new SqlCommand(EmployeesHelper.deleteEmployee(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 0, "EMPLOYEES_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SqlCommand updateCmd = new SqlCommand(EmployeesHelper.updateEmployees(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Name", SqlDbType.Text, 100, "EMPLOYEES_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Phone", SqlDbType.Text, 100, "PHONE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Mail", SqlDbType.Text, 100, "MAIL");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Salary", SqlDbType.Int, 0, "SALARY");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@DeptID", SqlDbType.Int, 0, "DEPARTMENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ID", SqlDbType.Int, 10, "EMPLOYEES_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SqlCommand getIDCmd = new SqlCommand(@"SELECT @@IDENTITY", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateEmployees()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SqlDataAdapter da = DBAccess.getEmployeesAdapter(conn);

                    // Update database
                    da.Update(data, "EMPLOYEES");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertEmployee()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlDataAdapter da = DBAccess.getEmployeesAdapter(conn);
              
                    da.Update(data, "EMPLOYEES");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }  
            }
            return id;
        }
        #endregion

        #region Departments
        internal static DataView getDepartments()
        {
            return data.Tables["DEPARTMENTS"].AsDataView();
        }

        
        public static SqlDataAdapter getDepartmentsAdapter(SqlConnection conn)
        {
            // Create data adapter
            SqlDataAdapter da = new SqlDataAdapter();

            // insert command
            SqlCommand insertCmd = new SqlCommand(DepartmentsHelper.insertDepartment(), conn);
            // map parameters
            SqlParameter parm = insertCmd.Parameters.Add("@Name", SqlDbType.Text, 100, "DEPARTMENTS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Region", SqlDbType.Text, 100, "REGION_INFO");
            parm.SourceVersion = DataRowVersion.Current;

            //delete command
            SqlCommand deleteCmd = new SqlCommand(DepartmentsHelper.deleteDepartment(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 0, "DEPARTMENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SqlCommand updateCmd = new SqlCommand(DepartmentsHelper.updateDepartments(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Name", SqlDbType.Text, 100, "DEPARTMENTS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Region", SqlDbType.Text, 100, "REGION_INFO");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ID", SqlDbType.Int, 10, "DEPARTMENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SqlCommand getIDCmd = new SqlCommand(@"SELECT @@IDENTITY", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateDepartments()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SqlDataAdapter da = DBAccess.getDepartmentsAdapter(conn);

                    // Update database
                    da.Update(data, "DEPARTMENTS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertDepartments()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlDataAdapter da = DBAccess.getDepartmentsAdapter(conn);

                    da.Update(data, "DEPARTMENTS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }

        #endregion

        #region Clients
        internal static DataView getClients()
        {
            return data.Tables["CLIENTS"].AsDataView();
        }

        public static SqlDataAdapter getClientsAdapter(SqlConnection conn)
        {
            // Create data adapter
            SqlDataAdapter da = new SqlDataAdapter();

            // insert command
            SqlCommand insertCmd = new SqlCommand(ClientsHelper.insertClient(), conn);
            // map parameters
            SqlParameter parm = insertCmd.Parameters.Add("@Name", SqlDbType.Text, 100, "CLIENTS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Phone", SqlDbType.Text, 100, "CLIENTS_PHONE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Mail", SqlDbType.Text, 100, "CLIENTS_MAIL");
            parm.SourceVersion = DataRowVersion.Current;

            //delete command
            SqlCommand deleteCmd = new SqlCommand(ClientsHelper.deleteClient(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "CLIENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SqlCommand updateCmd = new SqlCommand(ClientsHelper.updateClients(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Name", SqlDbType.Text, 100, "CLIENTS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Phone", SqlDbType.Text, 100, "CLIENTS_PHONE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Mail", SqlDbType.Text, 100, "CLIENTS_MAIL");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Id", SqlDbType.Int, 10, "CLIENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SqlCommand getIDCmd = new SqlCommand(@"SELECT @@IDENTITY", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateClients()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SqlDataAdapter da = DBAccess.getClientsAdapter(conn);

                    // Update database
                    da.Update(data, "CLIENTS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertClients()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlDataAdapter da = DBAccess.getClientsAdapter(conn);

                    da.Update(data, "CLIENTS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }
        #endregion

        #region Accounts
        public static DataView getAccounts()
        {
            return data.Tables["ACCOUNTS"].AsDataView();
        }

        public static SqlDataAdapter getAccountsAdapter(SqlConnection conn)
        {
            // Create data adapter
            SqlDataAdapter da = new SqlDataAdapter();

            // insert command
            SqlCommand insertCmd = new SqlCommand(AccountsHelper.insertAccount(), conn);
            // map parameters
            SqlParameter parm = insertCmd.Parameters.Add("@Description", SqlDbType.Text, 500, "DESCRIPTION");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Banks_id", SqlDbType.Int, 0, "BANKS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Accounts_sum", SqlDbType.Decimal, 0, "ACCOUNTS_SUM");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Account", SqlDbType.Text, 50, "ACCOUNT");
            parm.SourceVersion = DataRowVersion.Current;

            //delete command
            SqlCommand deleteCmd = new SqlCommand(AccountsHelper.deleteAccount(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 0, "ACCOUNTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SqlCommand updateCmd = new SqlCommand(ClientsHelper.updateClients(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Description", SqlDbType.Text, 500, "DESCRIPTION");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Banks_id", SqlDbType.Int, 0, "BANKS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Accounts_sum", SqlDbType.Decimal, 0, "ACCOUNTS_SUM");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Account", SqlDbType.Text, 50, "ACCOUNT");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ID", SqlDbType.Int, 0, "ACCOUNTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SqlCommand getIDCmd = new SqlCommand(@"SELECT @@IDENTITY", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateAccounts()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SqlDataAdapter da = DBAccess.getAccountsAdapter(conn);

                    // Update database
                    da.Update(data, "ACCOUNTS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertAccounts()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlDataAdapter da = DBAccess.getAccountsAdapter(conn);

                    da.Update(data, "ACCOUNTS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }
        #endregion

        #region Banks
        public static DataView getBanks()
        {
            return data.Tables["BANKS"].AsDataView();
        }

        public static SqlDataAdapter getBanksAdapter(SqlConnection conn)
        {
            // Create data adapter
            SqlDataAdapter da = new SqlDataAdapter();

            // insert command
            SqlCommand insertCmd = new SqlCommand(BanksHelper.insertBank(), conn);
            // map parameters
            SqlParameter parm = insertCmd.Parameters.Add("@Name", SqlDbType.Text, 100, "BANKS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Region", SqlDbType.Text, 100, "REGION_INFO");
            parm.SourceVersion = DataRowVersion.Current;

            //delete command
            SqlCommand deleteCmd = new SqlCommand(BanksHelper.deleteBank(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 0, "BANKS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SqlCommand updateCmd = new SqlCommand(BanksHelper.updateBanks(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Name", SqlDbType.Text, 100, "BANKS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Region", SqlDbType.Text, 100, "REGION_INFO");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ID", SqlDbType.Int, 0, "BANKS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SqlCommand getIDCmd = new SqlCommand(@"SELECT @@IDENTITY", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateBanks()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SqlDataAdapter da = DBAccess.getBanksAdapter(conn);

                    // Update database
                    da.Update(data, "BANKS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertBanks()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlDataAdapter da = DBAccess.getBanksAdapter(conn);

                    da.Update(data, "BANKS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }
        #endregion

        #region Products
        internal static DataView getProducts()
        {
            return data.Tables["PRODUCTS"].AsDataView();
        }


        public static SqlDataAdapter getProductsAdapter(SqlConnection conn)
        {
            // Create data adapter
            SqlDataAdapter da = new SqlDataAdapter();

            // insert command
            SqlCommand insertCmd = new SqlCommand(ProductsHelper.insertProduct(), conn);
            // map parameters
            SqlParameter parm = insertCmd.Parameters.Add("@Name", SqlDbType.Text, 100, "PRODUCTS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Price", SqlDbType.Decimal, 0, "PRICE");
            parm.SourceVersion = DataRowVersion.Current;

            
            //delete command
            SqlCommand deleteCmd = new SqlCommand(ProductsHelper.deleteProduct(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 0, "PRODUCTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SqlCommand updateCmd = new SqlCommand(ProductsHelper.updateProducts(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Name", SqlDbType.Text, 100, "PRODUCTS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Price", SqlDbType.Decimal, 0, "PRICE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ID", SqlDbType.Int, 0, "PRODUCTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SqlCommand getIDCmd = new SqlCommand(@"SELECT @@IDENTITY", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateProducts()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SqlDataAdapter da = DBAccess.getProductsAdapter(conn);

                    // Update database
                    da.Update(data, "PRODUCTS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertProducts()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlDataAdapter da = DBAccess.getProductsAdapter(conn);

                    da.Update(data, "PRODUCTS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }
        #endregion

        #region Orders
        internal static DataView getOrders()
        {
            return data.Tables["ORDERS"].AsDataView();
        }

        //todo refactor
        public static SqlDataAdapter getOrdersAdapter(SqlConnection conn)
        {
            // Create data adapter
            SqlDataAdapter da = new SqlDataAdapter();

            // insert command
            SqlCommand insertCmd = new SqlCommand(OrdersHelper.insertOrder(), conn);
            // map parameters
            SqlParameter parm = insertCmd.Parameters.Add("@Description", SqlDbType.Text, 500, "DESCRIPTION");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@TotalCosts", SqlDbType.Decimal, 0, "TOTAL_COSTS");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@ClientsId", SqlDbType.Int, 0, "CLIENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Date", SqlDbType.DateTime, 0, "ORDERS_DATE");
            parm.SourceVersion = DataRowVersion.Current;


            //delete command
            SqlCommand deleteCmd = new SqlCommand(OrdersHelper.deleteOrder(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "ORDERS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SqlCommand updateCmd = new SqlCommand(OrdersHelper.updateOrders(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Description", SqlDbType.Text, 500, "DESCRIPTION");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@TotalCosts", SqlDbType.Decimal, 0, "TOTAL_COSTS");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ClientsId", SqlDbType.Int, 0, "CLIENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Date", SqlDbType.DateTime, 0, "ORDERS_DATE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "ORDERS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SqlCommand getIDCmd = new SqlCommand(@"SELECT @@IDENTITY", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateOrders()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SqlDataAdapter da = DBAccess.getOrdersAdapter(conn);

                    // Update database
                    da.Update(data, "ORDERS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertOrders()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["MSSQL"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlDataAdapter da = DBAccess.getOrdersAdapter(conn);

                    da.Update(data, "ORDERS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }
        #endregion



    }
}
