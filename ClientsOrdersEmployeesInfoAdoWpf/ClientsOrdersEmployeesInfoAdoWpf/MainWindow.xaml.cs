﻿using ClientsOrdersEmployeesInfoAdoWpf.Clients;
using ClientsOrdersEmployeesInfoAdoWpf.Departments;
using ClientsOrdersEmployeesInfoAdoWpf.Employees;
using ClientsOrdersEmployeesInfoAdoWpf.Orders;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            dg_Employees.ItemsSource = DBAccess.getEmployees();
            dg_Departments.ItemsSource = DBAccess.getDepartments();
            dg_Clients.ItemsSource = DBAccess.getClients();
            dg_Orders.ItemsSource = DBAccess.getOrders();
            dg_Products.ItemsSource = DBAccess.getProducts();
            dg_Banks.ItemsSource = DBAccess.getBanks();
            dg_Accounts.ItemsSource = DBAccess.getAccounts();
        }

        #region Employees actions
        private void btn_viewEmployee_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Employees.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg_Employees.SelectedItem;
                DataRow dr = curRow.Row;
                ViewEmployee viewEmployee = new ViewEmployee(dr);
                viewEmployee.ShowDialog();
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to view", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_editEmployee_Click(object sender, RoutedEventArgs e)
        {
            showEditEmployeeWindow(dg_Employees);
        }

        private void dg_Employees_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditEmployeeWindow(dg_Employees);
        }

        private void showEditEmployeeWindow(DataGrid dg)
        {
            int index = dg.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg.SelectedItem;
                DataRow dr = curRow.Row;
                EditEmployee editEmployee = new EditEmployee(dr);
                editEmployee.ShowDialog();
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }


        private void btn_addEmployee_Click(object sender, RoutedEventArgs e)
        {
            AddEmployee addWindow = new AddEmployee();
            addWindow.ShowDialog();
        }

        private void btn_deleteEmployee_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Employees.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg_Employees.SelectedItem;
                DataRow dr = curRow.Row;
                MessageBoxResult result = MessageBox.Show(String.Format("Do you want to delete {0}?", dr[1]),
                    "Confirmation",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    dr.Delete();
                    DBAccess.updateEmployees();
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
        #endregion

        #region Departments actions
        private void btn_viewDepartment_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Departments.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg_Departments.SelectedItem;
                DataRow dr = curRow.Row;
                ViewDepartment viewDepartment = new ViewDepartment(dr);
                viewDepartment.ShowDialog();
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to view", 
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_editDepartment_Click(object sender, RoutedEventArgs e)
        {
            showEditDepartmentWindow(dg_Departments);
        }

        private void dg_Departments_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditDepartmentWindow(dg_Departments);
        }

        private void showEditDepartmentWindow(DataGrid dg)
        {
            int index = dg.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg.SelectedItem;
                DataRow dr = curRow.Row;
                EditDepartment editEmployee = new EditDepartment(dr);
                editEmployee.ShowDialog();
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", 
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_addDepartment_Click(object sender, RoutedEventArgs e)
        {
            AddDepartment addWindow = new AddDepartment();
            addWindow.ShowDialog();
        }

        private void btn_deleteDepartment_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Departments.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg_Departments.SelectedItem;
                DataRow dr = curRow.Row;
                MessageBoxResult result = MessageBox.Show(String.Format("Do you want to delete {0}?", dr[1]),
                    "Confirmation",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    dr.Delete();
                    DBAccess.updateEmployees();
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", 
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void dg_EmplDepts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditEmployeeWindow(dg_EmplDepts);
        }


        #endregion

        #region Clients actions
        private void dg_Clients_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditClientWindow(dg_Clients);
        }

        private void dg_ClientsOrders_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditOrderWindow(dg_Orders);
        }

        private void btn_viewClient_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Clients.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg_Clients.SelectedItem;
                DataRow dr = curRow.Row;
                ViewClient viewClient = new ViewClient(dr);
                viewClient.ShowDialog();
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to view",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
            
        }

        private void btn_editClient_Click(object sender, RoutedEventArgs e)
        {
            showEditClientWindow(dg_Clients);
        }

        private void showEditClientWindow(DataGrid dg)
        {
            int index = dg.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg.SelectedItem;
                DataRow dr = curRow.Row;
                EditClient editClient = new EditClient(dr);
                editClient.ShowDialog();
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_addClient_Click(object sender, RoutedEventArgs e)
        {
            AddClient addClient = new AddClient();
            addClient.ShowDialog();
        }

        private void btn_deleteClient_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Clients.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg_Clients.SelectedItem;
                DataRow dr = curRow.Row;
                MessageBoxResult result = MessageBox.Show(String.Format("Do you want to delete {0}?", dr[1]),
                    "Confirmation",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    dr.Delete();
                    DBAccess.updateClients();
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
        #endregion

        #region Orders actions
        private void btn_viewOrders_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Orders.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg_Orders.SelectedItem;
                DataRow dr = curRow.Row;
                ViewOrder viewOrder = new ViewOrder(dr);
                viewOrder.ShowDialog();
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to view",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_editOrders_Click(object sender, RoutedEventArgs e)
        {
            showEditOrderWindow(dg_Orders);
        }

        private void showEditOrderWindow(DataGrid dg)
        {
            int index = dg.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg.SelectedItem;
                DataRow dr = curRow.Row;
                EditOrder editOrder = new EditOrder(dr);
                editOrder.ShowDialog();
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_addOrders_Click(object sender, RoutedEventArgs e)
        {
            AddOrder addOrder = new AddOrder();
            addOrder.ShowDialog();
        }

        private void btn_deleteOrders_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Orders.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg_Orders.SelectedItem;
                DataRow dr = curRow.Row;
                MessageBoxResult result = MessageBox.Show(String.Format("Do you want to delete {0}?", dr[1]),
                    "Confirmation",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    dr.Delete();
                    DBAccess.updateOrders();
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
        #endregion

        #region Master detail SelectionChanged helders
        private void dg_Departments_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView curRow = (DataRowView)e.AddedItems[0];
                dg_EmplDepts.ItemsSource = curRow.CreateChildView(DBAccess.data.Relations["EmployeesDepatrments"]);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void dg_Clients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView curRow = (DataRowView)e.AddedItems[0];
                dg_ClientsOrders.ItemsSource = curRow.CreateChildView(DBAccess.data.Relations["OrdersClients"]);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void dg_Banks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DataRowView curRow = (DataRowView)e.AddedItems[0];
                dg_BanksAccounts.ItemsSource = curRow.CreateChildView(DBAccess.data.Relations["BanksAccounts"]);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }



        #endregion

        
    }
}
