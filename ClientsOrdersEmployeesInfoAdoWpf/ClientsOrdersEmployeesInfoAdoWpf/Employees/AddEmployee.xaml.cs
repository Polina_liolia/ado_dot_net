﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Employees
{
    /// <summary>
    /// Interaction logic for AddEmployee.xaml
    /// </summary>
    public partial class AddEmployee : Window
    {
        public AddEmployee()
        {
            InitializeComponent();
            this.DataContext = DBAccess.data.Tables["EMPLOYEES"];
            cboxDept.ItemsSource = DBAccess.data.Tables["DEPARTMENTS"].DefaultView;
            cboxDept.DisplayMemberPath = "DEPARTMENTS_NAME";
            cboxDept.SelectedValuePath = "DEPARTMENTS_ID";
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            DataRow newRow = DBAccess.data.Tables["EMPLOYEES"].NewRow();
            newRow[0] = -1; //temporary id

            string name = txtName.Text;
            if (name != string.Empty)
            {
                newRow[1] = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int deptID;
            if (cboxDept.SelectedValue != null)
            {
                deptID = (int)cboxDept.SelectedValue;
                newRow[5] = deptID;
            }
       
            int salary = -1;
            Int32.TryParse(txtSalary.Text, out salary);
            if (salary != -1 && salary > 0)
            {
                newRow[4] = salary;
            }
           
            string phone = txtPhone.Text;
            newRow[2] = phone;
            string mail = txtMail.Text;
            newRow[3] = mail;

            //adding row to dataset (with temporary id = -1)
            DBAccess.data.Tables["EMPLOYEES"].Rows.Add(newRow);
            //updating database (id is generating with identity autoincrement)
            int db_id = DBAccess.insertEmployee();

            ////getting id, generated with identity autoincrement
            // = DBAccess.getIdentity();

            //changing id value in dataset
            DataRow[] row = DBAccess.data.Tables["EMPLOYEES"].Select(string.Format("EMPLOYEES_ID = -1"));
            row[0][0] = db_id;
            this.Close();
        }
    }
}
