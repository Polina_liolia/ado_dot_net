﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Employees
{
    /// <summary>
    /// Interaction logic for EditEmployee.xaml
    /// </summary>
    public partial class EditEmployee : Window
    {
        private DataRow row;
        private EditEmployee()
        {
            InitializeComponent();
        }

        public EditEmployee(DataRow dr)
        {
            InitializeComponent();
            this.row = dr;
            this.DataContext = dr;
            cboxDept.ItemsSource = DBAccess.data.Tables["DEPARTMENTS"].DefaultView;
            cboxDept.DisplayMemberPath = "DEPARTMENTS_NAME";
            cboxDept.SelectedValuePath = "DEPARTMENTS_ID";

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            if (name != string.Empty)
            {
                row[1] = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int deptID;
            if (cboxDept.SelectedValue != null)
            {
                deptID = (int)cboxDept.SelectedValue;
                row[5] = deptID;
            }

            int salary = -1;
            Int32.TryParse(txtSalary.Text, out salary);
            if (salary != -1 && salary > 0)
            {
                row[4] = salary;
            }

            string phone = txtPhone.Text;
            row[2] = phone;
            string mail = txtMail.Text;
            row[3] = mail;

            DBAccess.updateEmployees();

            this.Close();
        }
    }
}
