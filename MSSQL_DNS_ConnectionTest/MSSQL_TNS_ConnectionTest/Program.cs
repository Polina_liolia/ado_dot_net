﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSSQL_DNS_ConnectionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            TestDSNOdbcConnection();
        }

        private static void TestDSNOdbcConnection()
        {
            //create DNS: Администрирование - Источники данных ODBC - Добавить
            OdbcConnection DbConnection = new OdbcConnection("DSN=MSSQL_DNS"); //Open Database Connectivity
            DbConnection.Open();
            OdbcCommand DbCommand = DbConnection.CreateCommand();
            DbCommand.CommandText = "select top 10 [BusinessEntityID], [FirstName] from[Person].[Person]";
            OdbcDataReader DbReader = DbCommand.ExecuteReader();
            int fCount = DbReader.FieldCount;
            Console.Write(":");
            for (int i = 0; i < fCount; i++)
            {
                String fName = DbReader.GetName(i);
                Console.Write(fName + ":");
            }
            Console.WriteLine();
            while (DbReader.Read())
            {
                Console.Write(":");
                for (int i = 0; i < fCount; i++)
                {
                    String col = DbReader.GetString(i);
                    Console.Write(col + ":");
                }
                Console.WriteLine();
            }

            DbReader.Close();
            DbCommand.Dispose();
            DbConnection.Close();
            Console.WriteLine("Press any...");
            Console.ReadKey();
        }
    }
}
