namespace CodeFirstExam
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ORDER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ORDER()
        {
            ORDERS_POSITIONS = new HashSet<ORDER_POSITION>();
        }

        
        public int ORDERS_ID { get; set; }

        public string DESCRIPTION { get; set; }

        public DateTime ORDERS_DATE { get; set; }

        public float? TOTAL_COSTS { get; set; }

        public int? CLIENTS_ID { get; set; }

        public virtual CLIENT CLIENTS { get; set; }

        public BindingList<ProductsListItem> get_products(int order_id)
        {
            var query = from product in DBAccess.model.PRODUCTS
                        orderby product.PRODUCTS_NAME
                        join order_position in DBAccess.model.ORDERS_POSITIONS
                        on product.PRODUCTS_ID equals order_position.PRODUCTS_ID
                        join order in DBAccess.model.ORDERS
                        on order_position.ORDERS_ID equals order.ORDERS_ID
                        where order.ORDERS_ID == order_id
                        select new ProductsListItem()
                        {
                            ID = product.PRODUCTS_ID,
                            Name = product.PRODUCTS_NAME,
                            Price = product.PRICE,
                            Count = order_position.ITEM_COUNT
                        };
            List<ProductsListItem> products = query.ToList<ProductsListItem>();
            BindingList<ProductsListItem> bindingList = new BindingList<ProductsListItem>(products);
            return bindingList;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ORDER_POSITION> ORDERS_POSITIONS { get; set; }
    }
}
