namespace CodeFirstExam
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ACCOUNT_TO_CLIENT
    {
        public int ACCOUNTS_TO_CLIENTS_ID { get; set; }

        public int? ACCOUNTS_ID { get; set; }

        public int? CLIENTS_ID { get; set; }

        public int? INDICATION { get; set; }

        public virtual ACCOUNT ACCOUNTS { get; set; }

        public virtual CLIENT CLIENTS { get; set; }
    }
}
