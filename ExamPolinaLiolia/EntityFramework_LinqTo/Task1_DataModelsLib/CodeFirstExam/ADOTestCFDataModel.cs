namespace CodeFirstExam
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Configurations;

    public partial class ADOTestCFDataModel : DbContext
    {
        public ADOTestCFDataModel()
            : base("name=ADOTestCFDataModel")
        {
        }

        public virtual DbSet<ACCOUNT> ACCOUNTS { get; set; }
        public virtual DbSet<ACCOUNT_TO_CLIENT> ACCOUNTS_TO_CLIENTS { get; set; }
        public virtual DbSet<BANK> BANKS { get; set; }
        public virtual DbSet<CLIENT> CLIENTS { get; set; }
        public virtual DbSet<DEPARTMENT> DEPARTMENTS { get; set; }
        public virtual DbSet<EMPLOYEE> EMPLOYEES { get; set; }
        public virtual DbSet<ORDER> ORDERS { get; set; }
        public virtual DbSet<ORDER_POSITION> ORDERS_POSITIONS { get; set; }
        public virtual DbSet<PRODUCT> PRODUCTS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AccountsConfig());
            modelBuilder.Configurations.Add(new AccountsToClientsConfig());
            modelBuilder.Configurations.Add(new BanksConfig());
            modelBuilder.Configurations.Add(new ClientsConfig());
            modelBuilder.Configurations.Add(new DepartmentsConfig());
            modelBuilder.Configurations.Add(new EmployeesConfig());
            modelBuilder.Configurations.Add(new OrdersConfig());
            modelBuilder.Configurations.Add(new OrdersPositionsConfig());
            modelBuilder.Configurations.Add(new ProductsConfig());
        }
    }
}
