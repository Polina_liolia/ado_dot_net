﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstExam.Configurations
{
    public class EmployeesConfig : EntityTypeConfiguration<EMPLOYEE>
    {
        public EmployeesConfig()
        {
            HasKey(e => e.EMPLOYEES_ID);

            Property(e => e.EMPLOYEES_NAME)
                .IsRequired()
                .IsUnicode(false);

            Property(e => e.PHONE)
                .IsUnicode(false);

            Property(e => e.MAIL)
                .IsUnicode(false);
        }
    }
}
