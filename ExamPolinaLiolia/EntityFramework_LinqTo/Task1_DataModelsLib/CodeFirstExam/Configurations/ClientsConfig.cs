﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstExam.Configurations
{
    public class ClientsConfig : EntityTypeConfiguration<CLIENT>
    {
        public ClientsConfig()
        {
            HasKey(e => e.CLIENTS_ID);

            Property(e => e.CLIENTS_NAME)
                   .IsRequired()
                   .IsUnicode(false);


            Property(e => e.CLIENTS_PHONE)
                .IsRequired()
                .IsUnicode(false);


            Property(e => e.CLIENTS_MAIL)
            .IsUnicode(false);


            HasMany(e => e.ACCOUNTS_TO_CLIENTS)
            .WithOptional(e => e.CLIENTS)
            .WillCascadeOnDelete();


            HasMany(e => e.ORDERS)
            .WithOptional(e => e.CLIENTS)
            .WillCascadeOnDelete();
        }
        
               
    }
}
