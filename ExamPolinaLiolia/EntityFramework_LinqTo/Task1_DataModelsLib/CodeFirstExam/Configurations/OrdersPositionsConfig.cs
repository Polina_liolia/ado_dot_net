﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstExam.Configurations
{
    public class OrdersPositionsConfig : EntityTypeConfiguration<ORDER_POSITION>
    {
        public OrdersPositionsConfig()
        {
            HasKey(e => e.ORDERS_POSITIONS_ID)
                .ToTable("ORDERS_POSITIONS");
        }
    }
}
