﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstExam.Configurations
{
    public class DepartmentsConfig : EntityTypeConfiguration<DEPARTMENT>
    {
        public DepartmentsConfig()
        {
            HasKey(e => e.DEPARTMENTS_ID);

            Property(e => e.DEPARTMENTS_NAME)
                .IsRequired()
                .IsUnicode(false);
            
            Property(e => e.REGION_INFO)
                .IsUnicode(false);
            
            HasMany(e => e.EMPLOYEES)
                .WithOptional(e => e.DEPARTMENTS)
                .WillCascadeOnDelete();
        }
    }
}
