﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstExam.Configurations
{
    class AccountsToClientsConfig : EntityTypeConfiguration<ACCOUNT_TO_CLIENT>
    {
        public AccountsToClientsConfig()
        {
            HasKey(e => e.ACCOUNTS_TO_CLIENTS_ID)
                .ToTable("ACCOUNTS_TO_CLIENTS");
        }
    }
}
