﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstExam.Configurations
{
    public class OrdersConfig : EntityTypeConfiguration<ORDER>
    {
        public OrdersConfig()
        {
            HasKey(e => e.ORDERS_ID);

            Property(e => e.DESCRIPTION)
               .IsUnicode(false);

            HasMany(e => e.ORDERS_POSITIONS)
                .WithOptional(e => e.ORDERS)
                .WillCascadeOnDelete();
        }
    }
}
