﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstExam.Configurations
{
    public class ProductsConfig : EntityTypeConfiguration<PRODUCT>
    {
        public ProductsConfig()
        {
            HasKey(e => e.PRODUCTS_ID);

            Property(e => e.PRODUCTS_NAME)
                .IsRequired()
                .IsUnicode(false);

            HasMany(e => e.ORDERS_POSITIONS)
                .WithOptional(e => e.PRODUCTS)
                .WillCascadeOnDelete();
        }
    }
}
