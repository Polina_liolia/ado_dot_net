﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstExam.Configurations
{
    public class AccountsConfig : EntityTypeConfiguration<ACCOUNT>
    {
        public AccountsConfig()
        {
            HasKey(a => a.ACCOUNTS_ID);
            Property(a => a.DESCRIPTION).IsUnicode(false);
            Property(a => a.ACCOUNT_NUMBER).HasColumnName("ACCOUNT");

            HasMany(e => e.ACCOUNTS_TO_CLIENTS)
                .WithOptional(e => e.ACCOUNTS)
                .WillCascadeOnDelete();  
        }
    }
}
