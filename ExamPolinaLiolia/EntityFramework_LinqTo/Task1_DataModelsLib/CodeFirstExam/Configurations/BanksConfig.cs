﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstExam.Configurations
{
    public class BanksConfig : EntityTypeConfiguration<BANK>
    {
        public BanksConfig()
        {
            HasKey(e => e.BANKS_ID);

            Property(e => e.BANKS_NAME)
               .IsRequired()
               .IsUnicode(false);

            Property(e => e.REGION_INFO)
                .IsRequired()
                .IsUnicode(false);

            HasMany(e => e.ACCOUNTS)
                .WithOptional(e => e.BANKS)
                .WillCascadeOnDelete();
        }
    }
}
