namespace CodeFirstExam
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ACCOUNT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ACCOUNT()
        {
            ACCOUNTS_TO_CLIENTS = new HashSet<ACCOUNT_TO_CLIENT>();
        }

        
        public int ACCOUNTS_ID { get; set; }

        public string DESCRIPTION { get; set; }

        public int? BANKS_ID { get; set; }

        public float ACCOUNTS_SUM { get; set; }

        public int ACCOUNT_NUMBER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACCOUNT_TO_CLIENT> ACCOUNTS_TO_CLIENTS { get; set; }

        public virtual BANK BANKS { get; set; }
    }
}
