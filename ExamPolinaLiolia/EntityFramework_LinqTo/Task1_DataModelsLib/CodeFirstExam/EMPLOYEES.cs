namespace CodeFirstExam
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EMPLOYEE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EMPLOYEE()
        {
            
        }

        public int EMPLOYEES_ID { get; set; }

        public string EMPLOYEES_NAME { get; set; }

        public string PHONE { get; set; }

        public string MAIL { get; set; }

        public int? SALARY { get; set; }

        public int? DEPARTMENTS_ID { get; set; }

        public virtual DEPARTMENT DEPARTMENTS { get; set; }

    }
}
