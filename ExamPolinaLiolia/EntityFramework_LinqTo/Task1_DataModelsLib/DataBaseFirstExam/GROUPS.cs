//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataBaseFirstExam
{
    using System;
    using System.Collections.Generic;
    
    public partial class GROUPS
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GROUPS()
        {
            this.EMPLOYEES_TO_GROUPS = new HashSet<EMPLOYEES_TO_GROUPS>();
            this.EMPLOYEES_TO_GROUPS_HISTORY = new HashSet<EMPLOYEES_TO_GROUPS_HISTORY>();
        }
    
        public int GROUPS_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string GROUPS_NAME { get; set; }
        public Nullable<int> GROUPS_ROLE { get; set; }
        public Nullable<int> MAIN_GROUPS_ID { get; set; }
        public string GROUPS_TITLE { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EMPLOYEES_TO_GROUPS> EMPLOYEES_TO_GROUPS { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EMPLOYEES_TO_GROUPS_HISTORY> EMPLOYEES_TO_GROUPS_HISTORY { get; set; }
    }
}
