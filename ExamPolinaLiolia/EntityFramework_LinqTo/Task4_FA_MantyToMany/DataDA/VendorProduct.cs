﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDA
{
    public class VendorProduct
    {
        [Key]
        [Column(Order = 1)]
        [ForeignKey("Vendor")]
        public int VendorID { get; set; }

        [Key]
        [Column(Order = 2)]
        [ForeignKey("Product")]
        public int ProductID { get; set; }

        public virtual Vendor Vendor { get; set; }
        public virtual Product Product { get; set; }
    }
}
