﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDA
{
    public class Product
    {
        public Product()
        {
            VendorProduct = new HashSet<VendorProduct>();
        }
        [Key]
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public virtual ICollection<VendorProduct> VendorProduct { get; set; }
    }
}
