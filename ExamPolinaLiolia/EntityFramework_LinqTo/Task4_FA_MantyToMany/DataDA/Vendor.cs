﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDA
{
    public class Vendor
    {
        public Vendor()
        {
            VendorProduct = new HashSet<VendorProduct>();
        }
        [Key]
        public int VendorID { get; set; }
        public string VendorName { get; set; }
        public virtual ICollection<VendorProduct> VendorProduct { get; set; }
    }
}
