﻿using DataFA;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccessFA
{
    public class DropCreateDB : DropCreateDatabaseAlways<Model_ManyToManyFA>
    {
        protected override void Seed(Model_ManyToManyFA context)
        {
            base.Seed(context);
            Vendor vendor1 = new Vendor() { VendorName = "vendor1" };
            Vendor vendor2 = new Vendor() { VendorName = "vendor2" };
            Vendor vendor3 = new Vendor() { VendorName = "vendor3" };

            Product product1 = new Product() { ProductName = "product1" };
            Product product2 = new Product() { ProductName = "product2" };
            Product product3 = new Product() { ProductName = "product3" };
            Product product4 = new Product() { ProductName = "product4" };
            Product product5 = new Product() { ProductName = "product5" };

            vendor1.Products.Add(product1);
            vendor1.Products.Add(product3);
            vendor1.Products.Add(product4);
            vendor1.Products.Add(product5);

            vendor2.Products.Add(product2);
            vendor2.Products.Add(product4);

            vendor3.Products.Add(product2);
            vendor3.Products.Add(product3);
            vendor3.Products.Add(product5);

            context.Vendors.AddRange(new Vendor[] { vendor1, vendor2, vendor3 });
            context.Products.AddRange(new Product[] { product1, product2, product3, product4, product5 });
            

            context.SaveChanges();
        }
    }
}
