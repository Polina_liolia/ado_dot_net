﻿using DataFA;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccessFA.EntitiesConfigurations
{
    public class VendorsConfigs : EntityTypeConfiguration<Vendor>
    {
        public VendorsConfigs()
        {
            HasMany(v => v.Products)
                .WithMany(p => p.Vendors)
                .Map(
            m =>
            {
                m.MapLeftKey("VendorId");
                m.MapRightKey("ProductId");
                m.ToTable("VendorProduct");
            });
        }
         
    }
}
