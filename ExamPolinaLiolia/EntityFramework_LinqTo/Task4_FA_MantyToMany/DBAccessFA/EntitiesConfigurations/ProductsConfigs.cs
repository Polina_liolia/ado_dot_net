﻿using DataFA;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccessFA.EntitiesConfigurations
{
    public class ProductsConfigs : EntityTypeConfiguration<Product>
    {
        public ProductsConfigs()
        {
            HasMany(p => p.Vendors)
                .WithMany(v => v.Products)
                .Map(
                      m =>
                      {
                          m.MapLeftKey("ProductId");
                          m.MapRightKey("VendorId");
                          m.ToTable("VendorProduct");
                      });
        }
        
    }
}
