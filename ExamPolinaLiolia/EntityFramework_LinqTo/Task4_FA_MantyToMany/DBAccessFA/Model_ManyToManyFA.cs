﻿using DataFA;
using DBAccessFA.EntitiesConfigurations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccessFA
{
    public class Model_ManyToManyFA : DbContext
    {
        public Model_ManyToManyFA() :base("MSSQL_FA")
        {
            Database.SetInitializer(new DropCreateDB());
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Vendor> Vendors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new ProductsConfigs());
            modelBuilder.Configurations.Add(new VendorsConfigs());
        }
    }
}
