﻿using DataDA;
using System.Data.Entity;

namespace DBAccessDA
{
    internal class DropCreateDB : DropCreateDatabaseAlways<Model_ManyToManyDA>
    {
        protected override void Seed(Model_ManyToManyDA context)
        {
            base.Seed(context);
            Vendor vendor1 = new Vendor() { VendorName = "vendor1" };
            Vendor vendor2 = new Vendor() { VendorName = "vendor2" };
            Vendor vendor3 = new Vendor() { VendorName = "vendor3" };

            Product product1 = new Product() { ProductName = "product1" };
            Product product2 = new Product() { ProductName = "product2" };
            Product product3 = new Product() { ProductName = "product3" };
            Product product4 = new Product() { ProductName = "product4" };
            Product product5 = new Product() { ProductName = "product5" };

            VendorProduct vp1 = new VendorProduct() { Vendor = vendor1, Product = product2 };
            VendorProduct vp2 = new VendorProduct() { Vendor = vendor1, Product = product3 };
            VendorProduct vp3 = new VendorProduct() { Vendor = vendor2, Product = product2 };
            VendorProduct vp4 = new VendorProduct() { Vendor = vendor2, Product = product1 };
            VendorProduct vp5 = new VendorProduct() { Vendor = vendor2, Product = product5 };
            VendorProduct vp6 = new VendorProduct() { Vendor = vendor1, Product = product4 };
            VendorProduct vp7 = new VendorProduct() { Vendor = vendor3, Product = product1 };
            VendorProduct vp8 = new VendorProduct() { Vendor = vendor3, Product = product5 };
            VendorProduct vp9 = new VendorProduct() { Vendor = vendor3, Product = product3 };

            context.Vendors.AddRange(new Vendor[] { vendor1, vendor2, vendor3 });
            context.Products.AddRange(new Product[] { product1, product2, product3, product4, product5 });
            context.VendorProduct.AddRange(new VendorProduct[] { vp1, vp2, vp3, vp4, vp5, vp6, vp7, vp8, vp9 });

            context.SaveChanges();
        }
    }
}