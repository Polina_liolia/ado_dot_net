﻿using DataDA;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccessDA
{
    public class Model_ManyToManyDA : DbContext
    {
        public Model_ManyToManyDA() : base ("MSSQL_DA")
        {
            Database.SetInitializer(new DropCreateDB());
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<VendorProduct> VendorProduct { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
