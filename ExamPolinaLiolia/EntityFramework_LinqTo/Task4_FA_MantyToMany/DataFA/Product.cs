﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFA
{
    public class Product
    {
        public Product()
        {
            Vendors = new List<Vendor>();
        }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public virtual ICollection<Vendor> Vendors { get; set; }
    }
}
