﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFA
{
    public class Vendor
    {
        public Vendor()
        {
            Products = new List<Product>();
        }
        public int VendorID { get; set; }
        public string VendorName { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
