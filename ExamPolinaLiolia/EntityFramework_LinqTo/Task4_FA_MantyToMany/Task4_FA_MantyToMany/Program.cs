﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBAccessDA;
using DBAccessFA;

namespace Task4_FA_MantyToMany
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Data annotations model test:");
            using (Model_ManyToManyDA model = new Model_ManyToManyDA())
            {
                var grouped_query = from vendor in model.Vendors
                                    orderby vendor.VendorName
                                    join vp in model.VendorProduct
                                    on vendor.VendorID equals vp.VendorID
                                    join product in model.Products
                                    on vp.ProductID equals product.ProductId
                                    group vp by vp.Vendor.VendorName;
                foreach (var group in grouped_query)
                {
                    Console.WriteLine($"Products of {group.Key}:");
                    foreach (var vp in group)
                    {
                        Console.WriteLine(vp.Product.ProductName);
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine("Fluent API model test:");
            using (Model_ManyToManyFA model = new Model_ManyToManyFA())
            {
                foreach (var vendor in model.Vendors)
                {
                    Console.WriteLine($"Products of {vendor.VendorName}:");
                    foreach (var product in vendor.Products)
                    {
                        Console.WriteLine(product.ProductName);
                    }
                }
            }
        }
    }
}
