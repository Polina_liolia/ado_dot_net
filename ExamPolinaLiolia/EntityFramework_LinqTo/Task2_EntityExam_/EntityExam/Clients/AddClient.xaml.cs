﻿using CodeFirstExam;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EntityExam.Clients
{
    /// <summary>
    /// Interaction logic for AddClient.xaml
    /// </summary>
    public partial class AddClient : Window
    {
        public AddClient()
        {
            InitializeComponent();
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            CLIENT client = new CLIENT();
            string name = txtName.Text;
            if (name != string.Empty)
            {
                client.CLIENTS_NAME = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string phone = txtPhone.Text;
            client.CLIENTS_PHONE = phone;

            string mail = txtMail.Text;
            client.CLIENTS_MAIL = txtMail.Text;

            //adding row to dbset
            DBAccess.model.CLIENTS.Add(client);
            //updating database 
            DBAccess.model.SaveChanges();
            this.Close();

        }
    }
}
