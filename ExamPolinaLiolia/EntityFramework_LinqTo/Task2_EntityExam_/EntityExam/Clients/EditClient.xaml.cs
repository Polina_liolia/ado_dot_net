﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CodeFirstExam;

namespace EntityExam.Clients
{
    /// <summary>
    /// Interaction logic for EditClient.xaml
    /// </summary>
    public partial class EditClient : Window
    {
        private CLIENT client;

        private EditClient()
        {
            InitializeComponent();
        }

        public EditClient(CLIENT client)
        {
            InitializeComponent();
            this.client = client;
            this.DataContext = this.client;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            if (name != string.Empty)
            {
                client.CLIENTS_NAME = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string phone = txtPhone.Text;
            client.CLIENTS_PHONE = phone;
            string mail = txtMail.Text;
            client.CLIENTS_MAIL = mail;

            DBAccess.model.SaveChanges();

            this.Close();
        }
    }
}
