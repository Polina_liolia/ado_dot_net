﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CodeFirstExam;

namespace EntityExam.Clients
{
    /// <summary>
    /// Interaction logic for ViewClient.xaml
    /// </summary>
    public partial class ViewClient : Window
    {
        private CLIENT client;

        private ViewClient()
        {
            InitializeComponent();
        }

        public ViewClient(CLIENT client)
        {
            InitializeComponent();
            this.client = client;
            this.DataContext = this.client;
        }
    }
}
