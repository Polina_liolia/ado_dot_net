﻿using CodeFirstExam;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace EntityExam.Converters
{
    public class EmployeesParentRelationsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int deptID = System.Convert.ToInt32(value);
            string dept_name = (from dept in DBAccess.model.DEPARTMENTS
                               where dept.DEPARTMENTS_ID == deptID
                               select dept.DEPARTMENTS_NAME).FirstOrDefault();
            return dept_name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class OrdersParentRelationsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int clientID = System.Convert.ToInt32(value);
            string clients_name = (from client in DBAccess.model.CLIENTS
                                   where client.CLIENTS_ID == clientID
                                   select client.CLIENTS_NAME).FirstOrDefault();
            return clients_name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class AccountsParentRelationsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int bankID = System.Convert.ToInt32(value);
            string bank_name = (from bank in DBAccess.model.BANKS
                                where bank.BANKS_ID == bankID
                                select bank.BANKS_NAME).FirstOrDefault();
            return bank_name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
