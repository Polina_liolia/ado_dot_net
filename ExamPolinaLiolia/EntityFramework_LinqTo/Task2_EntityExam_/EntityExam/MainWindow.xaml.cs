﻿using EntityExam.Clients;
using EntityExam.Departments;
using EntityExam.Employees;
using EntityExam.Orders;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Globalization;
using EntityExam.Products;
using EntityExam.Banks;
using EntityExam.Accounts;
using CodeFirstExam;
using System.Data.Entity;

namespace EntityExam
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
            dg_Employees.ItemsSource = DBAccess.model.EMPLOYEES.Local;
            dg_Departments.ItemsSource = DBAccess.model.DEPARTMENTS.Local;
            dg_Clients.ItemsSource = DBAccess.model.CLIENTS.Local;
            dg_Orders.ItemsSource = DBAccess.model.ORDERS.Local;
            dg_Products.ItemsSource = DBAccess.model.PRODUCTS.Local;
            dg_Banks.ItemsSource = DBAccess.model.BANKS.Local;
            dg_Accounts.ItemsSource = DBAccess.model.ACCOUNTS.Local;
        }

        #region Employees actions
        private void btn_viewEmployee_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Employees.SelectedIndex;
            if (index >= 0)
            {
                if (dg_Employees.SelectedItem is EMPLOYEE)
                {
                    EMPLOYEE current_employee = dg_Employees.SelectedItem as EMPLOYEE;
                    if (current_employee != null)
                    {
                        ViewEmployee viewEmployee = new ViewEmployee(current_employee);
                        viewEmployee.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to view", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_editEmployee_Click(object sender, RoutedEventArgs e)
        {
            showEditEmployeeWindow(dg_Employees);
        }

        private void dg_Employees_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditEmployeeWindow(dg_Employees);
        }

        private void showEditEmployeeWindow(DataGrid dg)
        {
            if (dg.SelectedIndex >= 0)
            {
                if (dg.SelectedItem is EMPLOYEE)
                {
                    EMPLOYEE current_employee = dg.SelectedItem as EMPLOYEE;
                    if (current_employee != null)
                    {
                        EditEmployee editEmployee = new EditEmployee(current_employee);
                        editEmployee.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }


        private void btn_addEmployee_Click(object sender, RoutedEventArgs e)
        {
            AddEmployee addWindow = new AddEmployee();
            addWindow.ShowDialog();
        }

        private void btn_deleteEmployee_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Employees.SelectedIndex >= 0)
            {
                if (dg_Employees.SelectedItem is EMPLOYEE)
                {
                    EMPLOYEE current_employee = dg_Employees.SelectedItem as EMPLOYEE;
                    if (current_employee != null)
                    {
                        MessageBoxResult result = MessageBox.Show($"Do you want to delete {current_employee.EMPLOYEES_NAME}?",
                                                                   "Confirmation",
                                                                   MessageBoxButton.YesNo,
                                                                   MessageBoxImage.Question);
                        if (result == MessageBoxResult.Yes)
                        {
                            DBAccess.model.EMPLOYEES.Remove(current_employee);
                            DBAccess.model.SaveChanges();
                        }
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to delete", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
        #endregion

        #region Departments actions
        private void btn_viewDepartment_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Departments.SelectedIndex >= 0)
            {
                if (dg_Departments.SelectedItem is DEPARTMENT)
                {
                    DEPARTMENT dept = dg_Departments.SelectedItem as DEPARTMENT;
                    if (dept != null)
                    {
                        ViewDepartment viewDepartment = new ViewDepartment(dept);
                        viewDepartment.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to view", 
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_editDepartment_Click(object sender, RoutedEventArgs e)
        {
            showEditDepartmentWindow(dg_Departments);
        }

        private void dg_Departments_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditDepartmentWindow(dg_Departments);
        }

        private void showEditDepartmentWindow(DataGrid dg)
        {
            if (dg.SelectedIndex >= 0)
            {
                if (dg.SelectedItem is DEPARTMENT)
                {
                    DEPARTMENT dept = dg.SelectedItem as DEPARTMENT;
                    if (dept != null)
                    {
                        EditDepartment editEmployee = new EditDepartment(dept);
                        editEmployee.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", 
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_addDepartment_Click(object sender, RoutedEventArgs e)
        {
            AddDepartment addWindow = new AddDepartment();
            addWindow.ShowDialog();
        }

        private void btn_deleteDepartment_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Departments.SelectedIndex >= 0)
            {
                if (dg_Departments.SelectedItem is DEPARTMENT)
                {
                    DEPARTMENT dept = dg_Departments.SelectedItem as DEPARTMENT;
                    if (dept != null)
                    {
                        MessageBoxResult result = MessageBox.Show($"Do you want to delete {dept.DEPARTMENTS_NAME}?",
                                                                    "Confirmation",
                                                                    MessageBoxButton.YesNo,
                                                                    MessageBoxImage.Question);
                        if (result == MessageBoxResult.Yes)
                        {
                            DBAccess.model.DEPARTMENTS.Remove(dept);
                            DBAccess.model.SaveChanges();
                        }
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to delete", 
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void dg_EmplDepts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditEmployeeWindow(dg_EmplDepts);
        }
        #endregion

        #region Clients actions
        private void btn_viewClient_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Clients.SelectedIndex >= 0)
            {
                if (dg_Clients.SelectedItem is CLIENT)
                {
                    CLIENT client = dg_Clients.SelectedItem as CLIENT;
                    if (client != null)
                    {
                        ViewClient viewClient = new ViewClient(client);
                        viewClient.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to view",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_editClient_Click(object sender, RoutedEventArgs e)
        {
            showEditClientWindow(dg_Clients);
        }

        private void dg_Clients_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditClientWindow(dg_Clients);
        }

        private void dg_ClientsOrders_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditOrderWindow(dg_Orders);
        }

        private void showEditClientWindow(DataGrid dg)
        {
            if (dg.SelectedIndex >= 0)
            {
                if (dg.SelectedItem is CLIENT)
                {
                    CLIENT client = dg.SelectedItem as CLIENT;
                    if (client != null)
                    {
                        EditClient editClient = new EditClient(client);
                        editClient.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_addClient_Click(object sender, RoutedEventArgs e)
        {
            AddClient addClient = new AddClient();
            addClient.ShowDialog();
        }

        private void btn_deleteClient_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Clients.SelectedIndex >= 0)
            {
                if (dg_Clients.SelectedItem is CLIENT)
                {
                    CLIENT client = dg_Clients.SelectedItem as CLIENT;
                    if (client != null)
                    {
                        MessageBoxResult result = MessageBox.Show($"Do you want to delete {client.CLIENTS_NAME}?",
                                                "Confirmation",
                                                MessageBoxButton.YesNo,
                                                MessageBoxImage.Question);
                        if (result == MessageBoxResult.Yes)
                        {
                            DBAccess.model.CLIENTS.Remove(client);
                            DBAccess.model.SaveChanges();
                        }
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to delete",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void dg_ClientsOrdersProducts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditProductWindow(dg_ClientsOrdersProducts);
        }

        private void btn_getVIPClients_Click(object sender, RoutedEventArgs e)
        {
            VIPClients VIPClientsWindow = new VIPClients();
            VIPClientsWindow.ShowDialog();
        }
        #endregion

        #region Orders actions
        private void btn_viewOrders_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Orders.SelectedIndex >= 0)
            {
                if (dg_Orders.SelectedItem is ORDER)
                {
                    ORDER order = dg_Orders.SelectedItem as ORDER;
                    if (order != null)
                    {
                        ViewOrder viewOrder = new ViewOrder(order);
                        viewOrder.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to view",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_editOrders_Click(object sender, RoutedEventArgs e)
        {
            showEditOrderWindow(dg_Orders);
        }

        private void dg_Orders_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditOrderWindow(dg_Orders);
        }

        private void showEditOrderWindow(DataGrid dg)
        {
            if (dg.SelectedIndex >= 0)
            {
                if (dg.SelectedItem is ORDER)
                {
                    ORDER order = dg.SelectedItem as ORDER;
                    if (order != null)
                    {
                        EditOrder editOrder = new EditOrder(order);
                        editOrder.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_addOrders_Click(object sender, RoutedEventArgs e)
        {
            AddOrder addOrder = new AddOrder();
            addOrder.ShowDialog();
        }

        private void btn_deleteOrders_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Orders.SelectedIndex >= 0)
            {
                if (dg_Orders.SelectedItem is ORDER)
                {
                    ORDER order = dg_Orders.SelectedItem as ORDER;
                    if (order != null)
                    {
                        MessageBoxResult result = MessageBox.Show("Do you want to delete selected order?",
                                                                     "Confirmation",
                                                                     MessageBoxButton.YesNo,
                                                                     MessageBoxImage.Question);
                        if (result == MessageBoxResult.Yes)
                        {
                            DBAccess.model.ORDERS.Remove(order);
                            DBAccess.model.SaveChanges();
                        }
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to delete",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void dg_OrdersProducts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditProductWindow(dg_OrdersProducts);
        }

        private void btn_avgOrderSum_Click(object sender, RoutedEventArgs e)
        {
            float? avg_order_sum = DBAccess.getAvgOrderSum();
            if (avg_order_sum != null)
            {
                MessageBox.Show($"Average orders sum is {avg_order_sum}.",
                    "Analitics", 
                    MessageBoxButton.OK, 
                    MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Average orders sum can not be calculated. Are there any orders in database?",
                    "Analitics",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
        }

        #endregion

        #region Products actions
        private void btn_viewProducts_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Products.SelectedIndex >= 0)
            {
                if (dg_Products.SelectedItem is PRODUCT)
                {
                    PRODUCT product = dg_Products.SelectedItem as PRODUCT;
                    if (product != null)
                    {
                        ViewProduct viewProduct = new ViewProduct(product);
                        viewProduct.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to view", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_addProducts_Click(object sender, RoutedEventArgs e)
        {
            AddProduct addProductWindow = new AddProduct();
            addProductWindow.ShowDialog();
        }

        private void btn_editProducts_Click(object sender, RoutedEventArgs e)
        {
            showEditProductWindow(dg_Products);
        }

        private void dg_Products_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditProductWindow(dg_Products);
        }

        private void showEditProductWindow(DataGrid dg)
        {
            if (dg.SelectedIndex >= 0)
            {
                if (dg.SelectedItem is PRODUCT)
                {
                    PRODUCT product = dg.SelectedItem as PRODUCT;
                    if (product != null)
                    {
                        EditProduct editProduct = new EditProduct(product);
                        editProduct.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_deleteProducts_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Products.SelectedIndex >= 0)
            {
                if (dg_Products.SelectedItem is PRODUCT)
                {
                    PRODUCT product = dg_Products.SelectedItem as PRODUCT;
                    if (product != null)
                    {
                        MessageBoxResult result = MessageBox.Show($"Do you want to delete {product.PRODUCTS_NAME}?",
                                                                    "Confirmation",
                                                                    MessageBoxButton.YesNo,
                                                                    MessageBoxImage.Question);
                        if (result == MessageBoxResult.Yes)
                        {
                            DBAccess.model.PRODUCTS.Remove(product);
                            DBAccess.model.SaveChanges();
                        }
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to delete", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
        #endregion

        #region Banks actions
        private void btn_viewBank_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Banks.SelectedIndex >= 0)
            {
                if (dg_Banks.SelectedItem is BANK)
                {
                    BANK bank = dg_Banks.SelectedItem as BANK;
                    if (bank != null)
                    {
                        ViewBank viewBank = new ViewBank(bank);
                        viewBank.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to view", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_editBank_Click(object sender, RoutedEventArgs e)
        {
            showEditBankWindow(dg_Banks);
        }

        private void dg_Banks_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditBankWindow(dg_Banks);
        }
        

        private void showEditBankWindow(DataGrid dg)
        {
            if (dg.SelectedIndex >= 0)
            {
                if (dg.SelectedItem is BANK)
                {
                    BANK bank = dg.SelectedItem as BANK;
                    if (bank != null)
                    {
                        EditBank editBank = new EditBank(bank);
                        editBank.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_addBank_Click(object sender, RoutedEventArgs e)
        {
            AddBank addBankWindow = new AddBank();
            addBankWindow.ShowDialog();
        }

        private void btn_deleteBank_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Banks.SelectedIndex >= 0)
            {
                if (dg_Banks.SelectedItem is BANK)
                {
                    BANK bank = dg_Banks.SelectedItem as BANK;
                    if (bank != null)
                    {
                        MessageBoxResult result = MessageBox.Show($"Do you want to delete {bank.BANKS_NAME}?",
                                                                     "Confirmation",
                                                                     MessageBoxButton.YesNo,
                                                                     MessageBoxImage.Question);
                                                                        if (result == MessageBoxResult.Yes)
                        {
                            DBAccess.model.BANKS.Remove(bank);
                            DBAccess.model.SaveChanges();
                        }
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to delete", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void dg_BanksAccounts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditAccountWindow(dg_BanksAccounts);
        }

        #endregion

        #region Accounts actions
        private void btn_viewAccount_Click(object sender, RoutedEventArgs e)
        {
            
            if (dg_Accounts.SelectedIndex >= 0)
            {
                if (dg_Accounts.SelectedItem is ACCOUNT)
                {
                    ACCOUNT account = dg_Accounts.SelectedItem as ACCOUNT;
                    if (account != null)
                    {
                        ViewAccount viewAccountWindow = new ViewAccount(account);
                        viewAccountWindow.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to view", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_editAccount_Click(object sender, RoutedEventArgs e)
        {
            showEditAccountWindow(dg_Accounts);
        }

        private void dg_Accounts_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            showEditAccountWindow(dg_Accounts);
        }

        private void showEditAccountWindow(DataGrid dg)
        {
            if (dg.SelectedIndex >= 0)
            {
                if (dg.SelectedItem is ACCOUNT)
                {
                    ACCOUNT account = dg.SelectedItem as ACCOUNT;
                    if (account != null)
                    {
                        EditAccount editAccountWindow = new EditAccount(account);
                        editAccountWindow.ShowDialog();
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void btn_addAccount_Click(object sender, RoutedEventArgs e)
        {
            AddAccount addAccountWindow = new AddAccount();
            addAccountWindow.ShowDialog();
        }

        private void btn_deleteAccount_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Accounts.SelectedIndex >= 0)
            {
                if (dg_Accounts.SelectedItem is ACCOUNT)
                {
                    ACCOUNT account = dg_Accounts.SelectedItem as ACCOUNT;
                    if (account != null)
                    {
                        MessageBoxResult result = MessageBox.Show($"Do you want to delete account number {account.ACCOUNT_NUMBER}?",
                                                                    "Confirmation",
                                                                    MessageBoxButton.YesNo,
                                                                    MessageBoxImage.Question);
                        if (result == MessageBoxResult.Yes)
                        {
                            DBAccess.model.ACCOUNTS.Remove(account);
                            DBAccess.model.SaveChanges();
                        }
                    }
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to delete", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        #endregion

        #region Master detail SelectionChanged holders
        private void dg_Departments_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (e.AddedItems[0] is DEPARTMENT)
                {
                    DEPARTMENT dept = e.AddedItems[0] as DEPARTMENT;
                    if (dept != null)
                    {
                        dg_EmplDepts.ItemsSource = dept.EMPLOYEES;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void dg_Clients_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (e.AddedItems[0] is CLIENT)
                {
                    CLIENT client = e.AddedItems[0] as CLIENT;
                    if (client != null)
                    {
                        dg_ClientsOrders.ItemsSource = client.ORDERS;
                        dg_ClientsAccounts.ItemsSource = DBAccess.getClientsAccountsList(client.CLIENTS_ID);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void dg_ClientsOrders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (e.AddedItems[0] is ORDER)
                {
                    ORDER order = e.AddedItems[0] as ORDER;
                    if (order != null)
                    {
                        dg_ClientsOrdersProducts.ItemsSource = DBAccess.getProductsListFromOrder(order.ORDERS_ID);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void dg_Orders_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {

                if (e.AddedItems[0] is ORDER)
                {
                    ORDER order = e.AddedItems[0] as ORDER;
                    if (order != null)
                    {
                        dg_OrdersProducts.ItemsSource = DBAccess.getProductsListFromOrder(order.ORDERS_ID);
                    }
                }
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

       

        private void dg_Banks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if(e.AddedItems[0] is BANK)
                {
                    BANK bank = e.AddedItems[0] as BANK;
                    if (bank != null)
                    {
                        dg_BanksAccounts.ItemsSource = bank.ACCOUNTS;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        #endregion

        
    }
}
