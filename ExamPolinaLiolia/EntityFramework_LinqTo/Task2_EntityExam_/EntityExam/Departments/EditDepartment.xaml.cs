﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CodeFirstExam;

namespace EntityExam.Departments
{
    /// <summary>
    /// Interaction logic for EditDepartment.xaml
    /// </summary>
    public partial class EditDepartment : Window
    {
        private DEPARTMENT dept;

        private EditDepartment()
        {
            InitializeComponent();
        }

        public EditDepartment(DEPARTMENT dept)
        {
            InitializeComponent();
            this.dept = dept;
            this.DataContext = this.dept;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            if (name != string.Empty)
            {
                dept.DEPARTMENTS_NAME = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string region_info = txtRegion.Text;
            dept.REGION_INFO = region_info;

            DBAccess.model.SaveChanges();

            this.Close();
        }
    }
}
