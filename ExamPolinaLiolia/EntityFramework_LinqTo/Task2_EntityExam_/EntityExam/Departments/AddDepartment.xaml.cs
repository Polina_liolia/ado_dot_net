﻿using CodeFirstExam;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EntityExam.Departments
{
    /// <summary>
    /// Interaction logic for AddDepartment.xaml
    /// </summary>
    public partial class AddDepartment : Window
    {
        public AddDepartment()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DEPARTMENT dept = new DEPARTMENT();
            string name = txtName.Text;
            if (name != string.Empty)
            {
                dept.DEPARTMENTS_NAME = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string region_info = txtRegion.Text;
            dept.REGION_INFO = region_info;

            //adding row to model
            DBAccess.model.DEPARTMENTS.Add(dept);
            //updating database 
            DBAccess.model.SaveChanges();
            this.Close();
        }
    }
}
