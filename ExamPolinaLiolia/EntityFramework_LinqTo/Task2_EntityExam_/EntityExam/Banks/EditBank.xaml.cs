﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CodeFirstExam;

namespace EntityExam.Banks
{
    /// <summary>
    /// Interaction logic for EditBank.xaml
    /// </summary>
    public partial class EditBank : Window
    {
        private BANK bank;

        private EditBank()
        {
            InitializeComponent();
        }

        public EditBank(BANK bank)
        {
            InitializeComponent();
            this.bank = bank;
            this.DataContext = this.bank;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            if (name != string.Empty)
            {
                bank.BANKS_NAME = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            bank.REGION_INFO = txtInfo.Text;

            DBAccess.model.SaveChanges();
            
            this.Close();
        }
    }
}
