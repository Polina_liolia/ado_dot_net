﻿using CodeFirstExam;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EntityExam.Banks
{
    /// <summary>
    /// Interaction logic for AddBank.xaml
    /// </summary>
    public partial class AddBank : Window
    {
        public AddBank()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BANK bank = new BANK();
            
            string name = txtName.Text;
            if (name != string.Empty)
            {
                bank.BANKS_NAME = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            bank.REGION_INFO = txtInfo.Text;

            //adding row to dbset
            DBAccess.model.BANKS.Add(bank);
            //updating database 
            DBAccess.model.SaveChanges();
            this.Close();
        }
    }
}
