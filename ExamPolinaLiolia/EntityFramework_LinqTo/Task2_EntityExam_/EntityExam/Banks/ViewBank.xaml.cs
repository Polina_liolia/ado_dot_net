﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CodeFirstExam;

namespace EntityExam.Banks
{
    /// <summary>
    /// Interaction logic for ViewBank.xaml
    /// </summary>
    public partial class ViewBank : Window
    {
        private BANK bank;

        private ViewBank()
        {
            InitializeComponent();
        }

        public ViewBank(BANK bank)
        {
            InitializeComponent();
            this.bank = bank;
            this.DataContext = this.bank;
        }
    }
}
