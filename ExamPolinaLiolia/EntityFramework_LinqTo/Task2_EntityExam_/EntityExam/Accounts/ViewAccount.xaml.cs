﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CodeFirstExam;

namespace EntityExam.Accounts
{
    /// <summary>
    /// Interaction logic for ViewAccount.xaml
    /// </summary>
    public partial class ViewAccount : Window
    {
        private ACCOUNT account;

        private ViewAccount()
        {
            InitializeComponent();
        }

        public ViewAccount(ACCOUNT account)
        {
            InitializeComponent();
            this.account = account;
            this.DataContext = this.account;
        }
    }
}
