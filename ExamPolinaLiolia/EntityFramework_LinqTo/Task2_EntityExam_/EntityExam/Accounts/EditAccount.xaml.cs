﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CodeFirstExam;

namespace EntityExam.Accounts
{
    /// <summary>
    /// Interaction logic for EditAccount.xaml
    /// </summary>
    public partial class EditAccount : Window
    {
        private ACCOUNT account;

        private EditAccount()
        {
            InitializeComponent();
        }

        public EditAccount(ACCOUNT account)
        {
            InitializeComponent();
            this.account = account;
            this.DataContext = this.account;
            cboxBank.ItemsSource = DBAccess.model.BANKS.Local;
            cboxBank.DisplayMemberPath = "BANKS_NAME";
            cboxBank.SelectedValuePath = "BANKS_ID";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            account.DESCRIPTION = txtDescr.Text;

            int bankID;
            if (cboxBank.SelectedValue != null)
            {
                bankID = Convert.ToInt32(cboxBank.SelectedValue);
                account.BANKS_ID = bankID;
            }
            else
            {
                MessageBox.Show("Bank can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            float sum = -1;
            bool result = float.TryParse(txtSum.Text, out sum);
            if (result && sum > 0)
            {
                account.ACCOUNTS_SUM = sum;
            }
            else
            {
                MessageBox.Show("Sum can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int account_number = -1;
            if (txtAccount.Text != string.Empty)
            {
                Int32.TryParse(txtAccount.Text, out account_number);
                if (account_number >= 0)
                    account.ACCOUNT_NUMBER = account_number;
                else
                {
                    MessageBox.Show("Wrong account!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Account can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DBAccess.model.SaveChanges();
            
            this.Close();
        }
    }
}
