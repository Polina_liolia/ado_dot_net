﻿using CodeFirstExam;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EntityExam.Accounts
{
    /// <summary>
    /// Interaction logic for AddAccount.xaml
    /// </summary>
    public partial class AddAccount : Window
    {
        public AddAccount()
        {
            InitializeComponent();
            cboxBank.ItemsSource = DBAccess.model.BANKS.Local;
            cboxBank.DisplayMemberPath = "BANKS_NAME";
            cboxBank.SelectedValuePath = "BANKS_ID";
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            ACCOUNT new_account = new ACCOUNT();
            new_account.DESCRIPTION = txtDescr.Text;

            int bankID;
            if (cboxBank.SelectedValue != null)
            {
                bankID = Convert.ToInt32(cboxBank.SelectedValue);
                new_account.BANKS_ID = bankID;
            }
            else
            {
                MessageBox.Show("Bank can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            float sum = -1;
            bool result = float.TryParse(txtSum.Text, out sum);
            if (result && sum >= 0)
            {
                new_account.ACCOUNTS_SUM = sum;
            }
            else
            {
                MessageBox.Show("Sum can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int account = -1;
            if (txtAccount.Text != string.Empty)
            {
                Int32.TryParse(txtAccount.Text, out account);
                if (account >= 0)
                    new_account.ACCOUNT_NUMBER = account;
                else
                {
                    MessageBox.Show("Wrong account!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Account can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            //adding row to dbset
            DBAccess.model.ACCOUNTS.Add(new_account);
            //updating database
            DBAccess.model.SaveChanges();
            this.Close();
        }
    }
}
