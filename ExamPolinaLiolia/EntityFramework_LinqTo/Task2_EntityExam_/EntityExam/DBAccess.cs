﻿using CodeFirstExam;
using EntityExam.Orders;
using EntityExam.TmpClasses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityExam
{
    public class DBAccess
    {
        public static ADOTestCFDataModel model;

        static DBAccess()
        {
            model = new ADOTestCFDataModel();
            model.EMPLOYEES.Load();
            model.ACCOUNTS.Load();
            model.ACCOUNTS_TO_CLIENTS.Load();
            model.BANKS.Load();
            model.CLIENTS.Load();
            model.DEPARTMENTS.Load();
            model.ORDERS.Load();
            model.ORDERS_POSITIONS.Load();
            model.PRODUCTS.Load();
        }

        #region Methods to get detail binding lists from many-to-many relations
        public static BindingList<ProductsListItem> getProductsListFromOrder(int order_id)
        {
            List<ProductsListItem> products = (from product in DBAccess.model.PRODUCTS
                                               orderby product.PRODUCTS_NAME
                                               join order_position in DBAccess.model.ORDERS_POSITIONS
                                               on product.PRODUCTS_ID equals order_position.PRODUCTS_ID
                                               join order in DBAccess.model.ORDERS
                                               on order_position.ORDERS_ID equals order.ORDERS_ID
                                               where order.ORDERS_ID == order_id
                                               select new ProductsListItem()
                                               {
                                                   ID = product.PRODUCTS_ID,
                                                   Name = product.PRODUCTS_NAME,
                                                   Price = product.PRICE,
                                                   Count = order_position.ITEM_COUNT
                                               }).ToList();
            return new BindingList<ProductsListItem>(products);
        }

        public static BindingList<ACCOUNT> getClientsAccountsList(int client_id)
        {
            List<ACCOUNT> accounts = (from account in DBAccess.model.ACCOUNTS
                           join account_to_client in DBAccess.model.ACCOUNTS_TO_CLIENTS
                           on account.ACCOUNTS_ID equals account_to_client.ACCOUNTS_ID
                           join client in DBAccess.model.CLIENTS
                           on account_to_client.CLIENTS_ID equals client.CLIENTS_ID
                           where client.CLIENTS_ID == client_id
                           select account).ToList();
            return new BindingList<ACCOUNT>(accounts);
        }
        #endregion

        #region LinqToEntities methods for analitics
        /// <summary>
        /// Caluclates sum of every order, then gets average value
        /// </summary>
        /// <returns>average sum of all orders</returns>
        public static float? getAvgOrderSum()
        {
            return DBAccess.model.ORDERS.Average(o => o.TOTAL_COSTS);         
        }

        /// <summary>
        /// Creates the list of clients, whose order sum is more than average
        /// </summary>
        /// <returns>VIP clients IEnumerable collection</returns>
        public static IEnumerable<CLIENT> getClientsVIPList()
        {
            float? avg_order_sum = DBAccess.getAvgOrderSum();
            return (from order in DBAccess.model.ORDERS
                                where order.TOTAL_COSTS > avg_order_sum
                                select order.CLIENTS).ToList().Distinct();
        }
        #endregion
    }
}
