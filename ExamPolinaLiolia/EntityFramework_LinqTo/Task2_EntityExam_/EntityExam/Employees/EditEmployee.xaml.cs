﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CodeFirstExam;

namespace EntityExam.Employees
{
    /// <summary>
    /// Interaction logic for EditEmployee.xaml
    /// </summary>
    public partial class EditEmployee : Window
    {
        private EMPLOYEE current_employee;

        private EditEmployee()
        {
            InitializeComponent();
        }

        public EditEmployee(EMPLOYEE current_employee)
        {
            InitializeComponent();
            this.current_employee = current_employee;
            this.DataContext = current_employee;
            cboxDept.ItemsSource = DBAccess.model.DEPARTMENTS.Local;
            cboxDept.DisplayMemberPath = "DEPARTMENTS_NAME";
            cboxDept.SelectedValuePath = "DEPARTMENTS_ID";
            cboxDept.SelectedValue = current_employee.DEPARTMENTS_ID;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            if (name != string.Empty)
            {
                current_employee.EMPLOYEES_NAME = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int deptID;
            if (cboxDept.SelectedValue != null)
            {
                deptID = Convert.ToInt32(cboxDept.SelectedValue);
                current_employee.DEPARTMENTS_ID = deptID;
            }

            int salary = -1;
            Int32.TryParse(txtSalary.Text, out salary);
            if (salary != -1 && salary > 0)
            {
                current_employee.SALARY = salary;
            }

            string phone = txtPhone.Text;
            current_employee.PHONE = phone;
            string mail = txtMail.Text;
            current_employee.MAIL = mail;

            DBAccess.model.SaveChanges();

            this.Close();
        }
    }
}
