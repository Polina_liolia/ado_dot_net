﻿using CodeFirstExam;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EntityExam.Employees
{
    /// <summary>
    /// Interaction logic for AddEmployee.xaml
    /// </summary>
    public partial class AddEmployee : Window
    {
        public AddEmployee()
        {
            InitializeComponent();
            this.DataContext = DBAccess.model.EMPLOYEES.Local;
            cboxDept.ItemsSource = DBAccess.model.DEPARTMENTS.Local;
            cboxDept.DisplayMemberPath = "DEPARTMENTS_NAME";
            cboxDept.SelectedValuePath = "DEPARTMENTS_ID";
            cboxDept.SelectedIndex = 0;
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            EMPLOYEE employee = new EMPLOYEE();
            string name = txtName.Text;
            if (name != string.Empty)
            {
                employee.EMPLOYEES_NAME = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int deptID;
            if (cboxDept.SelectedValue != null)
            {
                deptID = Convert.ToInt32(cboxDept.SelectedValue);
                employee.DEPARTMENTS_ID = deptID;
            }
       
            int salary = -1;
            Int32.TryParse(txtSalary.Text, out salary);
            if (salary != -1 && salary > 0)
            {
                employee.SALARY = salary;
            }
           
            string phone = txtPhone.Text;
            employee.PHONE = phone;
            string mail = txtMail.Text;
            employee.MAIL = mail;

            //adding row to model 
            DBAccess.model.EMPLOYEES.Add(employee);
            //updating database
            DBAccess.model.SaveChanges();
            this.Close();
        }
    }
}
