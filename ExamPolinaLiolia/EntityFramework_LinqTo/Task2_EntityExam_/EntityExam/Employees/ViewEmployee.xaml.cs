﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CodeFirstExam;

namespace EntityExam.Employees
{
    /// <summary>
    /// Interaction logic for ViewEmployee.xaml
    /// </summary>
    public partial class ViewEmployee : Window
    {
        private EMPLOYEE current_employee;

        private ViewEmployee()
        {
            InitializeComponent();
        }

        public ViewEmployee(EMPLOYEE current_employee)
        {
            InitializeComponent();
            this.current_employee = current_employee;
            this.DataContext = current_employee;
        }
    }
}
