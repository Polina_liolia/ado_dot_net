﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CodeFirstExam;
using System.Data.Entity;
using System.Collections;
using EntityExam.TmpClasses;

namespace EntityExam.Orders
{
    /// <summary>
    /// Interaction logic for EditOrder.xaml
    /// </summary>
    public partial class EditOrder : Window, INotifyPropertyChanged
    {
        BindingList<ProductsListItem> products;
        private float? totalPrice;
        private ORDER order;

        public float? TotalPrice
        {
            get { return totalPrice; }
            set
            {
                totalPrice = value;
                NotifyPropertyChanged("TotalPrice");
            }
        }
        private EditOrder()
        {
            InitializeComponent();
        }

        public EditOrder(ORDER order)
        {
            InitializeComponent();
            this.order = order;
            this.DataContext = this.order;
            products = DBAccess.getProductsListFromOrder(order.ORDERS_ID);
            dg_Products.ItemsSource = products;
            txtTotalCosts.DataContext = this;
            TotalPrice = getTotalCosts();
            cboxClient.ItemsSource = DBAccess.model.CLIENTS.Local;
            cboxClient.DisplayMemberPath = "CLIENTS_NAME";
            cboxClient.SelectedValuePath = "CLIENTS_ID";
        }

        private float? getTotalCosts()
        {
            return products.Sum(x => (x.Price * x.Count));
        }

        private void btn_edit_Click(object sender, RoutedEventArgs e)
        {
            string description = txtDescription.Text;
            order.DESCRIPTION = description;

            int clientID;
            if (cboxClient.SelectedValue != null)
            {
                clientID = Convert.ToInt32(cboxClient.SelectedValue);
                order.CLIENTS_ID = clientID;
            }
            else
            {
                MessageBox.Show("Client id can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            order.TOTAL_COSTS = TotalPrice;

            DateTime date = txtDate.DisplayDate;
            if (date != null)
            {
                order.ORDERS_DATE = date;
            }
            else
            {
                MessageBox.Show("Date can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DBAccess.model.SaveChanges();

            //editing products list of current order:
            attachProductsToOrder(products);

            this.Close();
        }

        #region Add/remove product actions
        private void btn_AddProduct_Click(object sender, RoutedEventArgs e)
        {
            AddProductToOrder addProductWindow = new AddProductToOrder();
            addProductWindow.ShowDialog();
            int productId = addProductWindow.ProductID;
            int count = addProductWindow.Count;
            if (productId != -1 && count > 0) //product was selected
            {
                PRODUCT selected_product = (from product in DBAccess.model.PRODUCTS
                                           where product.PRODUCTS_ID == productId
                                           select product).FirstOrDefault();
                if (selected_product != null)
                {
                    ProductsListItem item = new ProductsListItem(selected_product.PRODUCTS_ID,
                        selected_product.PRODUCTS_NAME, selected_product.PRICE, count);
                    products.Add(item);
                    TotalPrice += selected_product.PRICE * count;
                }
            }
        }

        private void btn_RemoveProduct_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Products.SelectedIndex >= 0)
            {
                if (dg_Products.SelectedItem is ProductsListItem)
                {
                    ProductsListItem item = dg_Products.SelectedItem as ProductsListItem;
                    TotalPrice -= item.Price * item.Count;
                    products.Remove(item);
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private void attachProductsToOrder(BindingList<ProductsListItem> products)
        {
            order.ORDERS_POSITIONS.Clear();
            foreach (ProductsListItem item in products)
            {
                ORDER_POSITION op_new = new ORDER_POSITION()
                {
                    ITEM_COUNT = item.Count,
                    PRICE = item.Price,
                    PRODUCTS_ID = item.ID,
                };
                order.ORDERS_POSITIONS.Add(op_new);
            }
            DBAccess.model.SaveChanges();
        }
        #endregion


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        #endregion

       
    }

   

}
