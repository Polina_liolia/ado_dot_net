﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CodeFirstExam;

namespace EntityExam.Orders
{
    /// <summary>
    /// Interaction logic for ViewOrder.xaml
    /// </summary>
    public partial class ViewOrder : Window
    {
        private ORDER order;

        private ViewOrder()
        {
            InitializeComponent();
        }

        public ViewOrder(ORDER order)
        {
            InitializeComponent();
            this.order = order;
            this.DataContext = this.order;
        }
    }
}
