﻿using CodeFirstExam;
using EntityExam.TmpClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EntityExam.Orders
{
    /// <summary>
    /// Interaction logic for AddOrder.xaml
    /// </summary>
    public partial class AddOrder : Window, INotifyPropertyChanged
    {
        BindingList<ProductsListItem> products = new BindingList<ProductsListItem>();
        private float? totalPrice;
        public float? TotalPrice
        {
            get { return totalPrice; }
            set
            {
                totalPrice = value;
                NotifyPropertyChanged("TotalPrice");
            }
        }
        public AddOrder()
        {
            InitializeComponent();
            txtTotalCosts.DataContext = this;
            TotalPrice = 0;
            dg_Products.ItemsSource = this.products;
            cboxClient.ItemsSource = DBAccess.model.CLIENTS.Local;
            cboxClient.DisplayMemberPath = "CLIENTS_NAME";
            cboxClient.SelectedValuePath = "CLIENTS_ID";
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            ORDER order = new ORDER();
            string description = txtDescription.Text;
            order.DESCRIPTION = description;

            int clientID;
            if (cboxClient.SelectedValue != null)
            {
                clientID = Convert.ToInt32(cboxClient.SelectedValue);
                order.CLIENTS_ID = clientID;
            }
            else
            {
                MessageBox.Show("Client id can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

                order.TOTAL_COSTS = TotalPrice;
            
            DateTime date = txtDate.DisplayDate;
            if (date != null)
            {
                order.ORDERS_DATE = date;
            }
            else
            {
                MessageBox.Show("Date can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DBAccess.model.ORDERS.Add(order);
            
            //adding products to order
            if(!attachProductsToOrder(order, products))
                MessageBox.Show("Attention: your order was created, but it is empty yet! Don't forget to add products.",
                    "Attention!", MessageBoxButton.OK, MessageBoxImage.Information);
            this.Close();
        }

       

        #region Add/remove product actions
        private void btn_AddProduct_Click(object sender, RoutedEventArgs e)
        {
            AddProductToOrder addProductWindow = new AddProductToOrder();
            addProductWindow.ShowDialog();
            int productId = addProductWindow.ProductID;
            int count = addProductWindow.Count;
            if (productId != -1 && count > 0) //product was selected
            {
                PRODUCT selected_product = (from product in DBAccess.model.PRODUCTS
                                            where product.PRODUCTS_ID == productId
                                            select product).FirstOrDefault();
                if (selected_product != null)
                {
                    ProductsListItem item = new ProductsListItem(selected_product.PRODUCTS_ID,
                        selected_product.PRODUCTS_NAME, selected_product.PRICE, count);
                    products.Add(item);
                    TotalPrice += selected_product.PRICE * count;
                }
            }
        }

        private void btn_RemoveProduct_Click(object sender, RoutedEventArgs e)
        {
            if (dg_Products.SelectedIndex >= 0)
            {
                if (dg_Products.SelectedItem is ProductsListItem)
                {
                    ProductsListItem item = dg_Products.SelectedItem as ProductsListItem;
                    TotalPrice -= item.Price * item.Count;
                    products.Remove(item);
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        private bool attachProductsToOrder(ORDER order, BindingList<ProductsListItem> products)
        {
            bool result = false;
            if(products.Count > 0)
            {
                order.ORDERS_POSITIONS.Clear();
                foreach (ProductsListItem item in products)
                {
                    ORDER_POSITION op_new = new ORDER_POSITION()
                    {
                        ITEM_COUNT = item.Count,
                        PRICE = item.Price,
                        PRODUCTS_ID = item.ID,
                    };
                    order.ORDERS_POSITIONS.Add(op_new);
                }
                DBAccess.model.SaveChanges();
                result = true;
            }
            return result;
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        #endregion
    }
}
