﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityExam.TmpClasses
{
    public class ProductsListItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
        public int? Count { get; set; }
        public ProductsListItem() { }
        public ProductsListItem(int id, string name, float price, int? count)
        {
            ID = id;
            Name = name;
            Price = price;
            Count = count;
        }
    }
}
