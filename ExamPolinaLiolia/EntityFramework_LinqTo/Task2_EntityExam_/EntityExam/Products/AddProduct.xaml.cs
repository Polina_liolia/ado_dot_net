﻿using CodeFirstExam;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EntityExam.Products
{
    /// <summary>
    /// Interaction logic for AddProduct.xaml
    /// </summary>
    public partial class AddProduct : Window
    {
        public AddProduct()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PRODUCT product = new PRODUCT();
            
            string name = txtName.Text;
            if (name != string.Empty)
            {
                product.PRODUCTS_NAME = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            float price = 0;
            bool result = float.TryParse(txtPrice.Text, out price);
            if (result)
                product.PRICE = price;
            else
            {
                MessageBox.Show("Price can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            //adding row to dbset:
            DBAccess.model.PRODUCTS.Add(product);
            //updating database 
            DBAccess.model.SaveChanges();
            this.Close();
        }
    }
}
