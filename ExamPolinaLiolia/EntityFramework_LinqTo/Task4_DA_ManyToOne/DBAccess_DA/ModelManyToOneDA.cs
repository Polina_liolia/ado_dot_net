﻿using DataDA;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccess_DA
{
    public class ModelManyToOneDA : DbContext
    {
        public ModelManyToOneDA() : base("MSSQL_DA")
        {
            Database.SetInitializer(new DropCreateDB());
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Grade> Grades { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
