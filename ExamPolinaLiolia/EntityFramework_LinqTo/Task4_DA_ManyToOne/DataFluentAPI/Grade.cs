﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataFluentAPI
{
    public class Grade
    {
        public Grade()
        {
            Students = new HashSet<Student>();
        }
        public int GradeID { get; set; }
        public string GradeName { get; set; }
        public string Section { get; set; }
        public virtual ICollection<Student> Students { get; set; }
    }
}
