﻿using DBAccess_DA;
using DBAccess_FluentAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4_DA_ManyToOne
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Data annotations:");
            using (ModelManyToOneDA model = new ModelManyToOneDA())
            {
                foreach (var student in model.Students)
                {
                    Console.WriteLine($"{student.Name} - {student.Grade.GradeName}, {student.Grade.Section}");
                }
            }
            Console.WriteLine();
            Console.WriteLine("Fluent API:");
            using (ModelManyToOneFluentAPI model = new ModelManyToOneFluentAPI())
            {
                foreach (var student in model.Students)
                {
                    Console.WriteLine($"{student.Name} - {student.Grade.GradeName}, {student.Grade.Section}");
                }
            }

        }
    }
}
