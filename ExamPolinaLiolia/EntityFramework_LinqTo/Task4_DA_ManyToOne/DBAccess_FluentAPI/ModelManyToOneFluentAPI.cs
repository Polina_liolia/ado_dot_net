﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataFluentAPI;
using DBAccess_FluentAPI.EntitiesConfigs;

namespace DBAccess_FluentAPI
{
     public class ModelManyToOneFluentAPI : DbContext
     {
         public ModelManyToOneFluentAPI() : base("MSSQL_FluentAPI")
         {
             Database.SetInitializer(new DropCreateDB());
         }

         public DbSet<Student> Students { get; set; }
         public DbSet<Grade> Grades { get; set; }

         protected override void OnModelCreating(DbModelBuilder modelBuilder)
         {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new StudentConfig());
            modelBuilder.Configurations.Add(new GradeConfig());
         }
     }
}
