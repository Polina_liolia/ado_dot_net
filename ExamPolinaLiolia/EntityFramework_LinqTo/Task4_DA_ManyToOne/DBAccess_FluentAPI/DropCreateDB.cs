﻿using DataFluentAPI;
using System.Collections.Generic;
using System.Data.Entity;

namespace DBAccess_FluentAPI
{
    internal class DropCreateDB : DropCreateDatabaseAlways<ModelManyToOneFluentAPI>
    {
        protected override void Seed(ModelManyToOneFluentAPI context)
        {
            base.Seed(context);
            Grade grade1 = new Grade() { GradeName = "A", Section = "Mathematics" };
            Grade grade2 = new Grade() { GradeName = "B", Section = "Mathematics" };
            Grade grade3 = new Grade() { GradeName = "C", Section = "Mathematics" };


            Student s1 = new Student() { Name = "Petr", Grade = grade1 };
            Student s2 = new Student() { Name = "Anna", Grade = grade1 };
            Student s3 = new Student() { Name = "Kristina", Grade = grade2 };
            Student s4 = new Student() { Name = "Anton", Grade = grade3 };
            Student s5 = new Student() { Name = "Alisa", Grade = grade2 };
            Student s6 = new Student() { Name = "Eugenii", Grade = grade3 };
            Student s7 = new Student() { Name = "Boris", Grade = grade3 };
            Student s8 = new Student() { Name = "Elena", Grade = grade1 };

            context.Grades.AddRange(new Grade[]{ grade1, grade2, grade3 });
            context.Students.AddRange(new Student[] { s1, s2, s3, s4, s5, s6, s7, s8 });
            context.SaveChanges();
        }
    }
}