﻿using DataFluentAPI;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccess_FluentAPI.EntitiesConfigs
{
    public class GradeConfig : EntityTypeConfiguration<Grade>
    {
        public GradeConfig()
        {
            HasMany(g => g.Students)
                .WithRequired(s => s.Grade)
                .HasForeignKey(s => s.Grade_GradeId);
        }
    }
}
