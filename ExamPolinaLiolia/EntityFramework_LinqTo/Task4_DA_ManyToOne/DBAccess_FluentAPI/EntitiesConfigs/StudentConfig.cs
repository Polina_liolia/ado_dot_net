﻿using DataFluentAPI;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccess_FluentAPI.EntitiesConfigs
{
    public class StudentConfig : EntityTypeConfiguration<Student>
    {
        public StudentConfig()
        {
            HasRequired(s => s.Grade)
                .WithMany(g => g.Students)
                .HasForeignKey(s => s.Grade_GradeId);
        }
    }
}
