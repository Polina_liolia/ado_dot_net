﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDA
{
    public class Grade
    {
        public Grade()
        {
            Students = new HashSet<Student>();
        }
        [Key]
        public int GradeID { get; set; }
        public string GradeName { get; set; }
        public string Section { get; set; }
        [ForeignKey ("Grade_GradeId")]
        public virtual ICollection<Student> Students { get; set; }
    }
}
