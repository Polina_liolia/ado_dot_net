﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDA
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [ForeignKey("Grade")]
        public int Grade_GradeId { get; set; }
        public virtual Grade Grade { get; set; }
    }
}
