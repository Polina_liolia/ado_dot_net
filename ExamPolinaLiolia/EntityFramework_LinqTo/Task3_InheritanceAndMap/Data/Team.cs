﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class Team : IEntity
    {
        [Key]
        public int Id { get; set; }
        //exception:  System.Data.SqlClient.SqlException: Column 'Name' in table 'dbo.Teams' is of a type that is invalid for use as a key column in an index.
        // [Index("IX_Team_TeamsName")] // Test for named index. 
        [Required]
        public string Name { get; set; }
        public virtual Coach Coach { get; set; }
        public virtual ICollection<Player> Players { get; set; }
        public virtual Stadion Stadion { get; set; }
    }
}
