﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    [Table("TeamPlayer")]
    public class Player : Person
    {
        [Index] // Automatically named 'IX_TeamPlayer_Number' 
        public int Number { get; set; }
        public virtual Team Team { get; set; }
    }
}
