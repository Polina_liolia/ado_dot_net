﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using DataAccess;

namespace _03_InheritanceAndMap
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var codeContext = new CodeContext())
            {
                foreach (var player in codeContext.Players)
                {
                    Console.WriteLine($"{player.FirstName} {player.LastName} Team: {player.Team.Name}, # {player.Number}, Coach: {player.Team.Coach.FirstName} {player.Team.Coach.LastName}");
                }
            }
        }
    }
}
                