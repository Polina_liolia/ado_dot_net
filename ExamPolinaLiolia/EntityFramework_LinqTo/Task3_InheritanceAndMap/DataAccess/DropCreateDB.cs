﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Data;

namespace DataAccess
{
    public class DropCreateDB : DropCreateDatabaseAlways<CodeContext>
    {
        protected override void Seed(CodeContext context)
        {
            base.Seed(context);
            Stadion stadion1 = new Stadion() { Name = "Green Stadion", City = "Green City", Street = "Green Str." };
            Stadion stadion2 = new Stadion() { Name = "Orange Stadion", City = "Orange City", Street = "Orange Str." };

            Coach coach1 = new Coach() { FirstName = "Kirill", LastName = "Karnauchov", City = "Kharkiv", Street = "Dinamovskaya" };
            Coach coach2 = new Coach() { FirstName = "Roman", LastName = "Soroka", City = "Dnepr", Street = "Rechnaya" };

            Team team1 = new Team() { Name = "MegaTeam", Coach = coach1, Stadion = stadion1 };
            Team team2 = new Team() { Name = "BestTaemEver", Coach = coach2, Stadion = stadion2 };

            coach1.Team = team1;
            coach2.Team = team2;

            Player player1 = new Player() { FirstName = "Anton", LastName = "Anisimov", City = "Kharkiv", Street = "Klochkovskaya", Number = 1, Team = team1 };
            Player player2 = new Player() { FirstName = "Boris", LastName = "Ivanov", City = "Kharkiv", Street = "Sumskaya", Number = 2, Team = team1 };
            Player player3 = new Player() { FirstName = "Ivan", LastName = "Kuzmenko", City = "Kharkiv", Street = "Veselaya", Number = 3, Team = team1 };
            Player player4 = new Player() { FirstName = "Egor", LastName = "Kavets", City = "Kharkiv", Street = "Mirnaya", Number = 4, Team = team1 };
            Player player5 = new Player() { FirstName = "Eugeniy", LastName = "Zhidkov", City = "Kharkiv", Street = "Landau", Number = 5, Team = team1 };
            Player player6 = new Player() { FirstName = "Konstantin", LastName = "Lebedev", City = "Kharkiv", Street = "Kamenskaya", Number = 6, Team = team1 };
            Player player7 = new Player() { FirstName = "Semen", LastName = "Petrenko", City = "Kharkiv", Street = "Druzby Narodov", Number = 1, Team = team2 };
            Player player8 = new Player() { FirstName = "Gennadiy", LastName = "Kozlov", City = "Kharkiv", Street = "Kotlova", Number = 2, Team = team2 };
            Player player9 = new Player() { FirstName = "Petr", LastName = "Rozhkov", City = "Kharkiv", Street = "Svetlaya", Number = 3, Team = team2 };
            Player player10 = new Player() { FirstName = "Sergey", LastName = "Perepelov", City = "Kharkiv", Street = "Dneproperovskaya", Number = 4, Team = team2 };
            Player player11 = new Player() { FirstName = "Maksim", LastName = "Kotlyarov", City = "Kharkiv", Street = "Lopanskaya", Number = 5, Team = team2 };
            Player player12 = new Player() { FirstName = "Mark", LastName = "Golinsky", City = "Kharkiv", Street = "Krasnoposelkovaya", Number = 6, Team = team2 };

            team1.Players = new List<Player>
            {
                player1, player2, player3, player4, player5, player6
            };

            team2.Players = new List<Player>
            {
                player7, player8, player9, player10, player11, player12
            };

            context.Stadions.AddRange(new Stadion[] { stadion1, stadion2 });
            context.Coaches.AddRange(new Coach[] { coach1, coach2 });
            context.Teams.AddRange(new Team[] { team1, team2 });
            context.Players.AddRange(new List<Player>
            {
               player1, player2, player3, player4, player5, player6, player7, player8, player9, player10, player11, player12
            });

            context.SaveChanges();
        }
    }
}
