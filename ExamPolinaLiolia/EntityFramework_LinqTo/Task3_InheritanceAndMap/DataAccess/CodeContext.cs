﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Data;

namespace DataAccess
{
    public class CodeContext : DbContext
    {
        public CodeContext(): base ("MS_SQL")
        {
            Database.SetInitializer(new DropCreateDB());
        }
        //public DbSet<Person> Persons { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Stadion> Stadions { get; set; }
        public DbSet<CustomHistory> CustomHistories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Player>()
                .HasRequired(p => p.Team)
                .WithMany(t => t.Players);

            modelBuilder.Entity<Coach>()
                .HasRequired(c => c.Team)
                .WithRequiredDependent(t => t.Coach);

            modelBuilder.Entity<Team>()
                .HasRequired(t => t.Stadion);

            modelBuilder.Entity<Team>()
                .HasRequired(t => t.Coach)
                .WithRequiredPrincipal(c => c.Team);

            modelBuilder.Entity<Team>()
                .HasMany(t => t.Players)
                .WithRequired(p => p.Team);
        }
    }
}
