namespace DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ROLE_LOG_VIEW
    {
        [Key]
        [Column(Order = 0)]
        public int MYLOGS_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string ACTION { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string TBL_NAME { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_ROW { get; set; }

        [StringLength(100)]
        public string LOG_INFO { get; set; }

        [Key]
        [Column(Order = 4)]
        public DateTime LOG_TIME { get; set; }
    }
}
