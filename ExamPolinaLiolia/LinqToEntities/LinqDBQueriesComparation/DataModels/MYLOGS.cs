namespace DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MYLOGS
    {
        [Key]
        public int MYLOGS_ID { get; set; }

        [Required]
        [StringLength(50)]
        public string ACTION { get; set; }

        [Required]
        [StringLength(50)]
        public string TBL_NAME { get; set; }

        public int ID_ROW { get; set; }

        [StringLength(100)]
        public string LOG_INFO { get; set; }

        public DateTime LOG_TIME { get; set; }
    }
}
