﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModels;
using NWDataModel;
using System.Data.Entity.Core.Objects;
using System.Text.RegularExpressions;

namespace LinqDBQueriesComparation
{
    class Program
    {
        static void Main(string[] args)
        {
            #region AdwentureWorks queries
            using (AdventureWorksModel advModel = new AdventureWorksModel())
            {
                /*
                1. Список должностей, на которых заработную плату получают раз в месяц и список должностей, на которых – дважды

                SELECT
                Employee.JobTitle,
                PayHistory.PayFrequency
                FROM[HumanResources].[Employee] AS Employee
                LEFT JOIN[HumanResources].[EmployeePayHistory] AS PayHistory
                ON Employee.BusinessEntityID = PayHistory.BusinessEntityID
                GROUP BY Employee.JobTitle, PayHistory.PayFrequency
                ORDER BY PayHistory.PayFrequency
                */
                var query1 = from employee in advModel.Employee
                             join emplPayHist in advModel.EmployeePayHistory
                             on employee.BusinessEntityID equals emplPayHist.BusinessEntityID into emplPayHistJoined
                             from eph in emplPayHistJoined.DefaultIfEmpty()

                             group employee by new { employee.JobTitle, eph.PayFrequency } into emplGrouped
                             orderby emplGrouped.Key.PayFrequency
                             select new
                             {
                                 emplGrouped.Key.JobTitle,
                                 emplGrouped.Key.PayFrequency
                             };

                Console.WriteLine(query1);
                Console.WriteLine();

                foreach (var item in query1)
                {
                    Console.WriteLine(item);
                }
                Console.WriteLine();


                /*
                 --2. Список отделов, в которых работают сотрудники на должностях, оплачиваемых раз в месяц
                SELECT 
                Department.Name AS 'Department name',
                PayHistory.PayFrequency
                FROM [HumanResources].[Department] AS Department
                LEFT JOIN [HumanResources].[EmployeeDepartmentHistory] AS DepartmentHistory
                ON Department.DepartmentID = DepartmentHistory.DepartmentID
                LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
                ON DepartmentHistory.BusinessEntityID = PayHistory.BusinessEntityID
                WHERE PayHistory.PayFrequency = 1
                GROUP BY Department.Name, PayHistory.PayFrequency
                ORDER BY Department.Name;    
             */

                var query2 = from department in advModel.Department
                                let edh = department.EmployeeDepartmentHistory.FirstOrDefault()
                                join emplPayHist in advModel.EmployeePayHistory
                                on edh.BusinessEntityID equals emplPayHist.BusinessEntityID into emplPayHistJoined
                                from eph in emplPayHistJoined.DefaultIfEmpty()
                                where eph.PayFrequency == 1
                                orderby department.Name
                                group new { department, eph } by new { department.Name, eph.PayFrequency };

                   Console.WriteLine(query2);

                   foreach (var group in query2)
                   {
                       Console.WriteLine($"{group.Key.Name} - {group.Key.PayFrequency} times per month");
                   }
                    
                /*
                3.Выведите список сотрудников с указанием их имени, должности, заработной платы и режима
                ее выплаты
                */
                /*
                SELECT 
                Person.LastName,
                Person.MiddleName,
                Person.FirstName,
                Employee.JobTitle,
                PayHistory.Rate,
                PayHistory.PayFrequency
                FROM [Person].[Person] AS Person
                LEFT JOIN [HumanResources].[Employee] AS Employee
                ON Person.BusinessEntityID = Employee.BusinessEntityID
                LEFT JOIN [HumanResources].[EmployeePayHistory] AS PayHistory
                ON Person.BusinessEntityID = PayHistory.BusinessEntityID
                ORDER BY Person.LastName
                */
               
                var query3 = from person in advModel.Person
                             orderby person.LastName
                             join employee in advModel.Employee
                             on person.BusinessEntityID equals employee.BusinessEntityID into emplJoined
                             from empl in emplJoined.DefaultIfEmpty()
                             join emplPayHistory in advModel.EmployeePayHistory
                             on person.BusinessEntityID equals emplPayHistory.BusinessEntityID into ephJoined
                             from eph in ephJoined.DefaultIfEmpty()
                             select new
                             {
                                 person.LastName,
                                 person.MiddleName,
                                 person.FirstName,
                                 JobTitle = empl != null ? empl.JobTitle : "not defined",
                                 Rate = eph != null ? eph.Rate : 0,
                                 PayFrequency = eph != null ? eph.PayFrequency : 0
                             };
                Console.WriteLine(query3);
                foreach (var item in query3)
                {
                    Console.WriteLine(item);
                }
                
            }
            #endregion

            #region Northwind queries
            using (NorthWindModel context = new NorthWindModel())
            {
                /*1. выбрать клиентов, у которых есть заказы
                 SELECT 
                Customers.CustomerID,
                Customers.ContactName,
                Orders.OrderID
                FROM [dbo].[Customers] AS Customers
                LEFT JOIN [dbo].[Orders] AS Orders
                ON Orders.CustomerID = Customers.CustomerID
                WHERE Orders.OrderID IS NOT NULL
                             */
                var query201 = from customer in context.Customers
                               join order in context.Orders
                               on customer.CustomerID equals order.CustomerID
                               select new
                               {
                                   customer.CustomerID,
                                   customer.ContactName,
                                   order.OrderID
                               };
                Console.WriteLine(query201);
                foreach (var item in query201)
                {
                    Console.WriteLine(item);
                }
                Console.WriteLine($"Rows selected: {query201.Count()}");

                //2.выбрать всех клиентов, у которых нет ни одного заказа
                // SELECT
                //Customers.CustomerID,
                //Customers.ContactName
                //FROM[dbo].[Customers]
                //        AS Customers
                //LEFT JOIN[dbo].[Orders]
                //        AS Orders
                //ON Orders.CustomerID = Customers.CustomerID
                //WHERE Orders.OrderID IS NULL

                var query202 = from customer in context.Customers
                               join order in context.Orders
                               on customer.CustomerID equals order.CustomerID into customersJoined
                               from customerJoined in customersJoined.DefaultIfEmpty()
                               where customerJoined == null
                               select new
                               {
                                   customer.CustomerID,
                                   customer.ContactName
                               };
                Console.WriteLine(query202);
                Console.WriteLine($"Rows selected: {query202.Count()}");

                // --3.выбрать клиентов, у которых только один заказ
                //   SELECT
                //Customers.CustomerID,
                //Customers.ContactName,
                //COUNT(Orders.OrderID) AS NUMBER_OF_ORDERS
                //FROM[dbo].[Customers] AS Customers
                //JOIN[dbo].[Orders] AS Orders
                //ON Orders.CustomerID = Customers.CustomerID
                //GROUP BY Customers.CustomerID,Customers.ContactName
                //HAVING COUNT(Orders.OrderID) = 1

                var query203 = from customer in context.Customers
                               join order in context.Orders
                               on customer.CustomerID equals order.CustomerID into ordersJoined
                               let number_of_orders = ordersJoined.Count()
                               where number_of_orders == 1
                               group customer by new { customer.CustomerID, customer.ContactName }
                               into customersGrouped
                               select new
                               {
                                   customersGrouped.Key.CustomerID,
                                   customersGrouped.Key.ContactName
                               };
                Console.WriteLine(query203);
                Console.WriteLine(query203.Count());
                foreach (var item in query203)
                {
                    Console.WriteLine(item);
                }

                /*
                 --4. выбрать клиентов, у которых более двух заказов
                SELECT 
                Customers.CustomerID,
                Customers.ContactName,
                COUNT (Orders.OrderID) AS NUMBER_OF_ORDERS 
                FROM [dbo].[Customers] AS Customers
                JOIN [dbo].[Orders] AS Orders
                ON Orders.CustomerID = Customers.CustomerID
                GROUP BY Customers.CustomerID,Customers.ContactName
                HAVING COUNT (Orders.OrderID) > 2
                 */
                var query204 = from customer in context.Customers
                               join order in context.Orders
                               on customer.CustomerID equals order.CustomerID into ordersJoined
                               let number_of_orders = ordersJoined.Count()
                               where number_of_orders > 2
                               group customer by new { customer.CustomerID, customer.ContactName }
                               into customersGrouped
                               select new
                               {
                                   customersGrouped.Key.CustomerID,
                                   customersGrouped.Key.ContactName
                               };
                Console.WriteLine(query204);
                Console.WriteLine(query204.Count());
                foreach (var item in query204)
                {
                    Console.WriteLine(item);
                }

                /*
                 --5. выбрать всех клиентов, у которых не задан Fax
                SELECT 
                Customers.CustomerID,
                Customers.ContactName,
                Customers.Fax
                FROM [dbo].[Customers] AS Customers
                WHERE Customers.Fax IS NULL
                 */
                var query205 = from customer in context.Customers
                               where customer.Fax == null
                               select new
                               {
                                   customer.CustomerID,
                                   customer.ContactName,
                                   customer.Fax
                               };
                Console.WriteLine(query205);
                Console.WriteLine(query205.Count());
                foreach (var item in query205)
                {
                    Console.WriteLine(item);
                }

                /*
             6. выбрать всех клиентов, у которых задан Регион
                SELECT 
                Customers.CustomerID,
                Customers.ContactName,
                Customers.Region
                FROM [dbo].[Customers] AS Customers
                WHERE Customers.Region IS NOT NULL    
             */
                var query206 = from customer in context.Customers
                               where customer.Region != null
                               select new
                               {
                                   customer.CustomerID,
                                   customer.ContactName,
                                   customer.Region
                               };
                Console.WriteLine(query206);
                Console.WriteLine(query206.Count());
                foreach (var item in query206)
                {
                    Console.WriteLine(item);
                }

                /*
             --7. выбрать всех клиентов, у которых PostalCode начинается с нуля
                SELECT 
                Customers.CustomerID,
                Customers.ContactName,
                Customers.PostalCode
                FROM [dbo].[Customers] AS Customers
                WHERE Customers.PostalCode LIKE '0%'    
             */
                var query207 = from customer in context.Customers
                               where customer.PostalCode.StartsWith("0")
                               select new
                               {
                                   customer.CustomerID,
                                   customer.ContactName,
                                   customer.PostalCode
                               };
                Console.WriteLine(query207);
                Console.WriteLine(query207.Count());
                foreach (var item in query207)
                {
                    Console.WriteLine(item);
                }

                /*
             --8. выбрать всех клиентов, у которых PostalCode длиннее 5 символов
                SELECT 
                Customers.CustomerID,
                Customers.ContactName,
                Customers.PostalCode
                FROM [dbo].[Customers] AS Customers
                where (LEN(Customers.PostalCode) > 5)  
             */

                var query208 = from customer in context.Customers
                               where customer.PostalCode.Length > 5
                               select new
                               {
                                   customer.CustomerID,
                                   customer.ContactName,
                                   customer.PostalCode
                               };
                Console.WriteLine(query208);
                Console.WriteLine(query208.Count());
                foreach (var item in query208)
                {
                    Console.WriteLine(item);
                }

                /*
                9. выбрать всех клиентов, у которых PostalCode начинается с нуля и длиннее 5 символов
                SELECT 
                Customers.CustomerID,
                Customers.ContactName,
                Customers.PostalCode
                FROM [dbo].[Customers] AS Customers
                WHERE (LEN(Customers.PostalCode) > 5) AND Customers.PostalCode LIKE '0%'  
             */

                var query209 = from customer in context.Customers
                               where customer.PostalCode.Length > 5 && customer.PostalCode.StartsWith("0")
                               select new
                               {
                                   customer.CustomerID,
                                   customer.ContactName,
                                   customer.PostalCode
                               };
                Console.WriteLine(query209);
                Console.WriteLine(query209.Count());
                foreach (var item in query209)
                {
                    Console.WriteLine(item);
                }

                /*
                10. выбрать всех клиентов, у которых Phone заканчивается на 4
                SELECT 
                Customers.CustomerID,
                Customers.ContactName,
                Customers.Phone
                FROM [dbo].[Customers] AS Customers
                WHERE Customers.Phone LIKE '%4'    
             */

                var query210 = from customer in context.Customers
                               where customer.Phone.EndsWith("4")
                               select new
                               {
                                   customer.CustomerID,
                                   customer.ContactName,
                                   customer.PostalCode
                               };
                Console.WriteLine(query210);
                Console.WriteLine(query210.Count());
                foreach (var item in query210)
                {
                    Console.WriteLine(item);
                }

                /*
                 11. выбрать всех клиентов, у которых Phone содержит скобки
                SELECT 
                Customers.CustomerID,
                Customers.ContactName,
                Customers.Phone
                FROM [dbo].[Customers] AS Customers
                WHERE Customers.Phone LIKE '%(%)%'
             */
                var query211 = from customer in context.Customers
                               where customer.Phone.Contains("(") && customer.Phone.Contains(")")
                               select new
                               {
                                   customer.CustomerID,
                                   customer.ContactName,
                                   customer.PostalCode
                               };
                Console.WriteLine(query211);
                Console.WriteLine(query211.Count());
                foreach (var item in query211)
                {
                    Console.WriteLine(item);
                }


                /*
                12. выбрать всех клиентов, у которых Phone код записан в скобках и длина его 1 символ
                SELECT 
                Customers.CustomerID,
                Customers.ContactName,
                Customers.Phone
                FROM [dbo].[Customers] AS Customers
                WHERE Customers.Phone LIKE '%(_)%'    
             */
                //EXCEPTION: LINQ to Entities does not recognize the method 'Boolean IsMatch(System.String)' method, 
                //and this method cannot be translated into a store expression.
                //Regex regex = new Regex("^[0-9]+\\([0-9]\\)[0-9]+$");
                //var query212 = from customer in context.Customers
                //               where regex.IsMatch(customer.Phone) 
                //               select new
                //               {
                //                   customer.CustomerID,
                //                   customer.ContactName,
                //                   customer.PostalCode
                //               };

                /*
                16. Вычислить количество клиентов к каждом регионе
                SELECT 
                Customers.Region,
                COUNT(Customers.CustomerID) AS CUSTOMERS_NUMBER
                FROM [dbo].[Customers] AS Customers
                GROUP BY Customers.Region
                ORDER BY CUSTOMERS_NUMBER DESC    
             */

                var query216 = from customer in context.Customers
                               group customer by customer.Region into customersOfRegion
                               let customersNumber = customersOfRegion.Count()
                               orderby customersNumber descending
                               select new
                               {
                                   Region = customersOfRegion.Key,
                                   CustomersNumber = customersNumber
                               };



                Console.WriteLine(query216);
                Console.WriteLine(query216.Count());
                foreach (var item in query216)
                {
                    Console.WriteLine(item);
                }

                /*
             --17. Вывести список клиентов из того региона, в котором их больше всего
                SELECT  
                Customers.CustomerID,
                Customers.ContactName,
                Customers.Region
                FROM [dbo].[Customers] AS Customers,(
                SELECT TOP 1
                Customers.Region,
                COUNT(Customers.CustomerID) as 'Customers amount'
                FROM [dbo].[Customers] AS Customers
                GROUP BY Customers.Region
                ORDER BY 2 desc
                ) x
                WHERE Customers.Region = x.Region or (Customers.Region is NULL and x.Region is NULL)    
             */

                var query217 = from customer in context.Customers
                               group customer by customer.Region into customersOfRegion
                               let customersNumber = customersOfRegion.Count()
                               orderby customersNumber descending
                               let regionWithMaxCustomers = customersOfRegion.FirstOrDefault().Region
                               from customer in context.Customers
                               where customer.Region == null && regionWithMaxCustomers == null ||
                               customer.Region.Equals(regionWithMaxCustomers)
                               select new
                               {
                                   customer.CustomerID,
                                   customer.ContactName,
                                   customer.Region
                               };

                Console.WriteLine(query217);
                Console.WriteLine(query217.Count());
                foreach (var item in query217)
                {
                    Console.WriteLine(item);
                }


                /*
                19. Вывести список Регионов и Территорий, которые обслуживают менеджеры, с указанием имени
                --закрепленного за территорией менеджера и города, в котором находится менеджер
                SELECT
                Region.RegionID,
                Region.RegionDescription,
                Territories.TerritoryID,
                Territories.TerritoryDescription,
                Employees.LastName,
                Employees.FirstName,
                Employees.City
                FROM [dbo].[Employees] AS Employees
                LEFT JOIN [dbo].[EmployeeTerritories] AS ETerritories
                ON ETerritories.EmployeeID = Employees.EmployeeID
                LEFT JOIN [dbo].[Territories] AS Territories
                ON Territories.TerritoryID = ETerritories.TerritoryID
                LEFT JOIN [dbo].[Region] AS Region
                ON Region.RegionID = Territories.RegionID    
             */

                var query219 = from ter in context.Territories
                               group ter by ter.Region into terGrouped
                               from terInGroup in terGrouped
                               let empl = terInGroup.Employees.FirstOrDefault()
                               select new
                               {
                                   terGrouped.Key.RegionID,
                                   terGrouped.Key.RegionDescription,
                                   terInGroup.TerritoryID,
                                   terInGroup.TerritoryDescription,
                                   empl.LastName,
                                   empl.FirstName,
                                   empl.City
                               };

                Console.WriteLine(query219);
                Console.WriteLine(query219.Count());
                foreach (var item in query219)
                {
                    Console.WriteLine(item);
                }
            }
            #endregion

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}
