﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOTestConnectionLib
{
    public class EmployeesHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[EMPLOYEES]";
        }

        public static string updateEmployees()
        {
            return @"
                UPDATE [dbo].[EMPLOYEES]   
                SET
                   EMPLOYEES_NAME = @Name,
                   PHONE = @Phone,
                   MAIL = @Mail,
                   SALARY = @Salary,                
                   DEPARTMENTS_ID = @DeptID
                WHERE
                    EMPLOYEES_ID = @ID
             ";
        }

        public static string insertEmployee()
        {
            return @"
                    INSERT INTO [dbo].[EMPLOYEES]
                    (
                        [EMPLOYEES_NAME],
                        [PHONE],
                        [MAIL],
                        [SALARY],
                        [DEPARTMENTS_ID]
                    )
                    VALUES
                    (
                        @Name,
                        @Phone,
                        @Mail,
                        @Salary,
                        @DeptID
                    );";
        }

        public static string deleteEmployee()
        {
            return @"
                    DELETE FROM  [dbo].[EMPLOYEES] 
					WHERE [EMPLOYEES_ID] = @ID;";
        }

    }

    public class DepartmentsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[DEPARTMENTS]";
        }

        public static string updateDepartments()
        {
            return @"
                    UPDATE[dbo].[DEPARTMENTS]
                    SET
                        [DEPARTMENTS_NAME] = @Name,
                        [REGION_INFO] = @Region
                    WHERE 
                        [DEPARTMENTS_ID] = @ID;";
        } 

        public static string insertDepartment()
        {
            return @"
                    INSERT INTO [dbo].[DEPARTMENTS]
                    (
	                    [DEPARTMENTS_NAME],
	                    [REGION_INFO]
                    )
                    VALUES
                    (
	                    @Name,
	                    @Region
                    );";
        }

        public static string deleteDepartment()
        {
            return @"
                    DELETE FROM [dbo].[DEPARTMENTS]
                    WHERE [DEPARTMENTS_ID] = @ID;";
        }
    }

    public class ClientsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[CLIENTS]";
        }

        public static string updateClients()
        {
            return @"
                    UPDATE [dbo].[CLIENTS]
                    SET 
	                    [CLIENTS_NAME] = @Name,
	                    [CLIENTS_PHONE] = @Phone,
	                    [CLIENTS_MAIL] = @Mail
                    WHERE
	                    [CLIENTS_ID] = @Id;";
        }

        public static string insertClient()
        {
            return @"
                    INSERT INTO [dbo].[CLIENTS]
                    (
	                    [CLIENTS_NAME],
	                    [CLIENTS_PHONE],
	                    [CLIENTS_MAIL]
                    )
                    VALUES
                    (
	                    @Name,
	                    @Phone,
	                    @Mail
                    );";
        }

        public static string deleteClient()
        {
            return @"
                    DELETE FROM [dbo].[CLIENTS]
                    WHERE
	                    [CLIENTS_ID] = @Id;";
        }
    }

    public class OrdersHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[ORDERS]";
        }

        public static string updateOrders()
        {
            return @"
                    UPDATE [dbo].[ORDERS]
                    SET 
	                    [DESCRIPTION] = @Description,
	                    [ORDERS_DATE] = @Date,
	                    [TOTAL_COSTS] = @TotalCosts,
	                    [CLIENTS_ID] = @ClientsId
                    WHERE [ORDERS_ID] = @Id;";
        }

        public static string insertOrder()
        {
            return @"
                    INSERT INTO [dbo].[ORDERS]
                    (
	                    [DESCRIPTION],
	                    [ORDERS_DATE],
	                    [TOTAL_COSTS],
	                    [CLIENTS_ID]
                    )
                    VALUES
                    (
	                    @Description,
	                    @Date,
	                    @TotalCosts,
	                    @ClientsId
                    );";
        }

        public static string deleteOrder()
        {
            return @"
                    DELETE FROM [dbo].[Orders]
                    WHERE [ORDERS_ID] = @Id;";
        }
    }

    public class ProductsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[PRODUCTS]";
        }

        public static string updateProducts()
        {
            return @"
                    UPDATE [dbo].[PRODUCTS]
                    SET
	                    [PRODUCTS_NAME] = @Name,
	                    [PRICE] = @Price
                    WHERE 
	                    [PRODUCTS_ID] = @ID;";
        }

        public static string insertProduct()
        {
            return @"
                    INSERT INTO [dbo].[PRODUCTS]
                    (
	                    [PRODUCTS_NAME],
	                    [PRICE]
                    )
                    VALUES
                    (
	                    @Name,
	                    @Price
                    );";
        }
        public static string deleteProduct()
        {
            return @"
                    DELETE FROM [dbo].[PRODUCTS]
                    WHERE 
	                    [PRODUCTS_ID] = @ID;";
        }
    }

    public class BanksHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[BANKS]";
        }

        public static string updateBanks()
        {
            return @"
                    UPDATE [dbo].[BANKS]
                    SET
	                    [BANKS_NAME] = @Name,
	                    [REGION_INFO] = @Region
                    WHERE 
	                    [BANKS_ID] = @ID;";
        }

        public static string insertBank()
        {
            return @"
                    INSERT INTO [dbo].[BANKS]
                    (
	                    [BANKS_NAME],
	                    [REGION_INFO]
                    )
                    VALUES
                    (
	                    @Name,
	                    @Region
                    );";
        }

        public static string deleteBank()
        {
            return @"
                    DELETE FROM [dbo].[BANKS]
                    WHERE 
	                    [BANKS_ID] = @ID;";
        }
    }

    public class AccountsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[ACCOUNTS]";
        }

        public static string updateAccounts()
        {
            return @"
                    UPDATE [dbo].[ACCOUNTS]
                    SET 
	                    [DESCRIPTION] = @Description,
	                    [BANKS_ID] = @Banks_id,
	                    [ACCOUNTS_SUM] = @Accounts_sum,
	                    [ACCOUNT] = @Account
                    WHERE
	                    [ACCOUNTS_ID] = @ID;";
        }

        public static string insertAccount()
        {
            return @"
                    INSERT INTO [dbo].[ACCOUNTS]
                    (
	                    [DESCRIPTION],
	                    [BANKS_ID],
	                    [ACCOUNTS_SUM],
	                    [ACCOUNT]
                    )
                    VALUES
                    (
	                    @Description,
	                    @Banks_id,
	                    @Accounts_sum,
	                    @Account
                    );";
        }

        public static string deleteAccount()
        {
            return @"
                    DELETE FROM [dbo].[ACCOUNTS]
                    WHERE
	                    [ACCOUNTS_ID] = @ID;";
        }
    }

    public class OrdersPositionsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[ORDERS_POSITIONS]";
        }

        public static string updateOrdersPositions()
        {
            return @"
                    UPDATE [dbo].[ORDERS_POSITIONS]
                    SET 
	                    [ORDERS_ID] = @Orders_id,
	                    [PRODUCTS_ID] = @Products_id,
	                    [PRICE] = @Price,
	                    [ITEM_COUNT] = @Item_count
                    WHERE 
	                    [ORDERS_POSITIONS_ID] = @ID;";
        }

        public static string insertOrderPosition()
        {
            return @"
                    INSERT INTO [dbo].[ORDERS_POSITIONS]
                    (
	                    [ORDERS_ID],
	                    [PRODUCTS_ID],
	                    [PRICE],
	                    [ITEM_COUNT]
                    )
                    VALUES
                    (
	                    @Orders_id,
	                    @Products_id,
	                    @Price,
	                    @Item_count
                    );";
        }

        public static string deleteOrderPosition()
        {
            return @"
                    DELETE [dbo].[ORDERS_POSITIONS]
                    WHERE 
	                    [ORDERS_POSITIONS_ID] = @ID;";
        }
    }

    public class AccountsToClientsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM [dbo].[ACCOUNTS_TO_CLIENTS]";
        }

        public static string updateAccountsToClients()
        {
            return @"
                    UPDATE [dbo].[ACCOUNTS_TO_CLIENTS]
                    SET 
	                    [ACCOUNTS_ID] = @Accounts_id,
	                    [CLIENTS_ID] = @Clients_id,
	                    [INDICATION] = @Indication
                    WHERE 
	                    [ACCOUNTS_TO_CLIENTS_ID] = @ID;";
        }

        public static string insertAccountsToClients()
        {
            return @"
                    INSERT INTO [dbo].[ACCOUNTS_TO_CLIENTS]
                    (
	                    [ACCOUNTS_ID],
	                    [CLIENTS_ID],
	                    [INDICATION]
                    )
                    VALUES
                    (
	                    @Accounts_id,
	                    @Clients_id,
	                    @Indication
                    );";
        }

        public static string deleteAccountsToClients()
        {
            return @"DELETE FROM [dbo].[ACCOUNTS_TO_CLIENTS]
                    WHERE 
	                    [ACCOUNTS_TO_CLIENTS_ID] = @ID;";
        }
    }
}
