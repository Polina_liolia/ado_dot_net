﻿using ADOTestConnectionLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientsOrdersEmployeesInfoAdoWinForms.Clients
{
    public partial class ClientsView : Form
    {
        private BindingSource masterBindingSource = new BindingSource();
        private BindingSource orderDetailsBindingSource = new BindingSource();

        public ClientsView()
        {
            InitializeComponent();
           
        }

        private void ClientsView_Load(object sender, EventArgs e)
        {
            // Bind the master data connector to the CLIENTS table.
            masterBindingSource.DataSource = DBAccess.data;
            masterBindingSource.DataMember = "CLIENTS";

            // Bind the details data connector to the master data connector,
            // using the DataRelation name to filter the information in the 
            // details table based on the current row in the master table.
            orderDetailsBindingSource.DataSource = masterBindingSource;
            orderDetailsBindingSource.DataMember = "OrdersClients";

            dgv_Clients.DataSource = masterBindingSource;
            dgv_orders.DataSource = orderDetailsBindingSource;
        }

        private void dgv_orders_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv_orders.SelectedRows.Count > 0)
            {
                int selectedOrderID = Convert.ToInt32(dgv_orders.SelectedRows[0].Cells["ORDERS_ID"].Value);
                if (selectedOrderID >= 1)
                    dgv_products.DataSource = DBAccess.createOrderProductsTmpDataTable(selectedOrderID).AsDataView();
            }
        }

        private void btn_AddClient_Click(object sender, EventArgs e)
        {
            AddClient addClientForm = new AddClient();
            addClientForm.ShowDialog();
        }

        private void btn_EditClient_Click(object sender, EventArgs e)
        {
            showEditClientWindow(dgv_Clients);
        }

        private void showEditClientWindow(DataGridView dg)
        {
            if (dg.SelectedRows.Count > 0 && dg.SelectedRows[0].Index >= 0)
            {
                int currentClientID = Convert.ToInt32(dg.SelectedRows[0].Cells["CLIENTS_ID"].Value);
                DataView dv = DBAccess.data.Tables["CLIENTS"].DefaultView;
                dv.RowFilter = $"CLIENTS_ID={currentClientID}";
                EditClient editClient = new EditClient(dv);
                editClient.ShowDialog();
               
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        private void btn_DeleteClient_Click(object sender, EventArgs e)
        {
            if (dgv_Clients.SelectedRows.Count > 0 && dgv_Clients.SelectedRows[0].Index >= 0)
            {
               DialogResult result = MessageBox.Show(String.Format("Do you want to delete {0}?", dgv_Clients.CurrentRow.Cells[1].Value),
                   "Confirmation",
                   MessageBoxButtons.YesNo,
                   MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    dgv_Clients.Rows.RemoveAt(dgv_Clients.CurrentRow.Index);
                    DBAccess.updateClients();
                    //updating child tables (because of cascade deleting):
                    DBAccess.updateAccountsToClients();
                    DBAccess.updateOrders();
                    DBAccess.updateOrdersPositions();
                    dgv_Clients.EndEdit();
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to delete",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void ClientsView_FormClosing(object sender, FormClosingEventArgs e)
        {
            DBAccess.data.AcceptChanges();
        }
    }
}
