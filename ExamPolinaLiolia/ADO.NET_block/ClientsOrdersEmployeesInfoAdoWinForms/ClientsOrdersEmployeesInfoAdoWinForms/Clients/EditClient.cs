﻿using ADOTestConnectionLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientsOrdersEmployeesInfoAdoWinForms.Clients
{
    public partial class EditClient : Form
    {
        private DataView dv;
        private EditClient()
        {
            InitializeComponent();
        }

        public EditClient(DataView dv)
        {
            InitializeComponent();
            this.dv = dv;
          
            txt_Name.DataBindings.Add(new Binding("Text", this.dv, "CLIENTS_NAME", true));
            txt_Phone.DataBindings.Add(new Binding("Text", this.dv, "CLIENTS_PHONE", true));
            txt_Mail.DataBindings.Add(new Binding("Text", this.dv, "CLIENTS_MAIL", true));
            
           
        }

        private void btn_Edit_Click(object sender, EventArgs e)
        {
            string name = txt_Name.Text;
            if (name != string.Empty)
            {
                dv[0][1] = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string phone = txt_Phone.Text;
            dv[0][2] = phone;
            string mail = txt_Mail.Text;
            dv[0][3] = mail;

            dv[0].EndEdit();//changing DataRowView status from Unchanged to Modified

            DBAccess.updateClients();//sending modified data to DB

            this.Close();
        }



    }
}
