﻿namespace ClientsOrdersEmployeesInfoAdoWinForms.Clients
{
    partial class ClientsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgv_Clients = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgv_orders = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgv_products = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_DeleteClient = new System.Windows.Forms.Button();
            this.btn_EditClient = new System.Windows.Forms.Button();
            this.btn_AddClient = new System.Windows.Forms.Button();
            this.lbl_Orders = new System.Windows.Forms.Label();
            this.lbl_Products = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Clients)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_orders)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_products)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgv_Clients);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(707, 148);
            this.panel1.TabIndex = 0;
            // 
            // dgv_Clients
            // 
            this.dgv_Clients.AllowUserToAddRows = false;
            this.dgv_Clients.AllowUserToDeleteRows = false;
            this.dgv_Clients.AllowUserToOrderColumns = true;
            this.dgv_Clients.AllowUserToResizeColumns = false;
            this.dgv_Clients.AllowUserToResizeRows = false;
            this.dgv_Clients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Clients.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_Clients.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgv_Clients.Location = new System.Drawing.Point(0, 0);
            this.dgv_Clients.MultiSelect = false;
            this.dgv_Clients.Name = "dgv_Clients";
            this.dgv_Clients.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_Clients.Size = new System.Drawing.Size(707, 148);
            this.dgv_Clients.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv_orders);
            this.panel2.Location = new System.Drawing.Point(12, 231);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(707, 141);
            this.panel2.TabIndex = 1;
            // 
            // dgv_orders
            // 
            this.dgv_orders.AllowUserToAddRows = false;
            this.dgv_orders.AllowUserToDeleteRows = false;
            this.dgv_orders.AllowUserToOrderColumns = true;
            this.dgv_orders.AllowUserToResizeColumns = false;
            this.dgv_orders.AllowUserToResizeRows = false;
            this.dgv_orders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_orders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_orders.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgv_orders.Location = new System.Drawing.Point(0, 0);
            this.dgv_orders.MultiSelect = false;
            this.dgv_orders.Name = "dgv_orders";
            this.dgv_orders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_orders.Size = new System.Drawing.Size(707, 141);
            this.dgv_orders.TabIndex = 0;
            this.dgv_orders.SelectionChanged += new System.EventHandler(this.dgv_orders_SelectionChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgv_products);
            this.panel3.Location = new System.Drawing.Point(12, 399);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(707, 100);
            this.panel3.TabIndex = 2;
            // 
            // dgv_products
            // 
            this.dgv_products.AllowUserToAddRows = false;
            this.dgv_products.AllowUserToDeleteRows = false;
            this.dgv_products.AllowUserToOrderColumns = true;
            this.dgv_products.AllowUserToResizeColumns = false;
            this.dgv_products.AllowUserToResizeRows = false;
            this.dgv_products.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_products.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_products.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgv_products.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dgv_products.Location = new System.Drawing.Point(0, 0);
            this.dgv_products.Name = "dgv_products";
            this.dgv_products.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_products.Size = new System.Drawing.Size(707, 100);
            this.dgv_products.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btn_DeleteClient);
            this.panel4.Controls.Add(this.btn_EditClient);
            this.panel4.Controls.Add(this.btn_AddClient);
            this.panel4.Location = new System.Drawing.Point(12, 166);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(707, 38);
            this.panel4.TabIndex = 3;
            // 
            // btn_DeleteClient
            // 
            this.btn_DeleteClient.Location = new System.Drawing.Point(166, 4);
            this.btn_DeleteClient.Name = "btn_DeleteClient";
            this.btn_DeleteClient.Size = new System.Drawing.Size(75, 23);
            this.btn_DeleteClient.TabIndex = 2;
            this.btn_DeleteClient.Text = "Delete";
            this.btn_DeleteClient.UseVisualStyleBackColor = true;
            this.btn_DeleteClient.Click += new System.EventHandler(this.btn_DeleteClient_Click);
            // 
            // btn_EditClient
            // 
            this.btn_EditClient.Location = new System.Drawing.Point(85, 4);
            this.btn_EditClient.Name = "btn_EditClient";
            this.btn_EditClient.Size = new System.Drawing.Size(75, 23);
            this.btn_EditClient.TabIndex = 1;
            this.btn_EditClient.Text = "Edit";
            this.btn_EditClient.UseVisualStyleBackColor = true;
            this.btn_EditClient.Click += new System.EventHandler(this.btn_EditClient_Click);
            // 
            // btn_AddClient
            // 
            this.btn_AddClient.Location = new System.Drawing.Point(4, 4);
            this.btn_AddClient.Name = "btn_AddClient";
            this.btn_AddClient.Size = new System.Drawing.Size(75, 23);
            this.btn_AddClient.TabIndex = 0;
            this.btn_AddClient.Text = "Add";
            this.btn_AddClient.UseVisualStyleBackColor = true;
            this.btn_AddClient.Click += new System.EventHandler(this.btn_AddClient_Click);
            // 
            // lbl_Orders
            // 
            this.lbl_Orders.AutoSize = true;
            this.lbl_Orders.Location = new System.Drawing.Point(12, 212);
            this.lbl_Orders.Name = "lbl_Orders";
            this.lbl_Orders.Size = new System.Drawing.Size(41, 13);
            this.lbl_Orders.TabIndex = 4;
            this.lbl_Orders.Text = "Orders:";
            // 
            // lbl_Products
            // 
            this.lbl_Products.AutoSize = true;
            this.lbl_Products.Location = new System.Drawing.Point(12, 380);
            this.lbl_Products.Name = "lbl_Products";
            this.lbl_Products.Size = new System.Drawing.Size(90, 13);
            this.lbl_Products.TabIndex = 5;
            this.lbl_Products.Text = "Products in order:";
            // 
            // ClientsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 511);
            this.Controls.Add(this.lbl_Products);
            this.Controls.Add(this.lbl_Orders);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ClientsView";
            this.Text = "ClientsView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClientsView_FormClosing);
            this.Load += new System.EventHandler(this.ClientsView_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Clients)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_orders)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_products)).EndInit();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgv_Clients;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgv_orders;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dgv_products;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btn_DeleteClient;
        private System.Windows.Forms.Button btn_EditClient;
        private System.Windows.Forms.Button btn_AddClient;
        private System.Windows.Forms.Label lbl_Orders;
        private System.Windows.Forms.Label lbl_Products;
    }
}