﻿using ADOTestConnectionLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientsOrdersEmployeesInfoAdoWinForms.Clients
{
    public partial class AddClient : Form
    {
        public AddClient()
        {
            InitializeComponent();
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            DataRow newRow = DBAccess.data.Tables["CLIENTS"].NewRow();
            newRow[0] = -1; //temporary id

            string name = txt_Name.Text;
            if (name != string.Empty)
            {
                newRow[1] = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string phone = txt_Phone.Text;
            newRow[2] = phone;

            string mail = txt_Mail.Text;
            newRow[3] = txt_Mail.Text;

            //adding row to dataset (with temporary id = -1)
            DBAccess.data.Tables["CLIENTS"].Rows.Add(newRow);
            //updating database (id is generating with identity autoincrement)
            int db_id = DBAccess.insertClients();
            //changing id value in dataset
            newRow[0] = db_id;
            this.Close();
        }
    }
}
