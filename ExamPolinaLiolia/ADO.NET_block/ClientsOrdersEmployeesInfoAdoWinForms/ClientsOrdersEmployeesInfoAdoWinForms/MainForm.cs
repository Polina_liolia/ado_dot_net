﻿using ClientsOrdersEmployeesInfoAdoWinForms.Clients;
using ClientsOrdersEmployeesInfoAdoWinForms.Orders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientsOrdersEmployeesInfoAdoWinForms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btn_Clients_Click(object sender, EventArgs e)
        {
            ClientsView clientsViewForm = new ClientsView();
            clientsViewForm.Show();
        }

        private void btn_Orders_Click(object sender, EventArgs e)
        {
            OrdersView ordersViewForm = new OrdersView();
            ordersViewForm.Show();
        }
    }
}
