﻿namespace ClientsOrdersEmployeesInfoAdoWinForms.Orders
{
    partial class AddOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_AddProduct = new System.Windows.Forms.Button();
            this.btn_RemoveProduct = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dg_Products = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTotalCosts = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboxClient = new System.Windows.Forms.ComboBox();
            this.btn_add = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_Products)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Description";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(102, 13);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(307, 51);
            this.txtDescription.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Date";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(102, 81);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(307, 20);
            this.txtDate.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Products list";
            // 
            // btn_AddProduct
            // 
            this.btn_AddProduct.Location = new System.Drawing.Point(22, 138);
            this.btn_AddProduct.Name = "btn_AddProduct";
            this.btn_AddProduct.Size = new System.Drawing.Size(61, 23);
            this.btn_AddProduct.TabIndex = 5;
            this.btn_AddProduct.Text = "Add";
            this.btn_AddProduct.UseVisualStyleBackColor = true;
            this.btn_AddProduct.Click += new System.EventHandler(this.btn_AddProduct_Click);
            // 
            // btn_RemoveProduct
            // 
            this.btn_RemoveProduct.Location = new System.Drawing.Point(22, 158);
            this.btn_RemoveProduct.Name = "btn_RemoveProduct";
            this.btn_RemoveProduct.Size = new System.Drawing.Size(61, 23);
            this.btn_RemoveProduct.TabIndex = 6;
            this.btn_RemoveProduct.Text = "Remove";
            this.btn_RemoveProduct.UseVisualStyleBackColor = true;
            this.btn_RemoveProduct.Click += new System.EventHandler(this.btn_RemoveProduct_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dg_Products);
            this.panel1.Location = new System.Drawing.Point(102, 121);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(307, 123);
            this.panel1.TabIndex = 7;
            // 
            // dg_Products
            // 
            this.dg_Products.AllowUserToAddRows = false;
            this.dg_Products.AllowUserToDeleteRows = false;
            this.dg_Products.AllowUserToResizeRows = false;
            this.dg_Products.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_Products.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_Products.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dg_Products.Location = new System.Drawing.Point(0, 0);
            this.dg_Products.Name = "dg_Products";
            this.dg_Products.ReadOnly = true;
            this.dg_Products.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_Products.Size = new System.Drawing.Size(307, 123);
            this.dg_Products.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 255);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Total costs";
            // 
            // txtTotalCosts
            // 
            this.txtTotalCosts.AutoSize = true;
            this.txtTotalCosts.Location = new System.Drawing.Point(102, 255);
            this.txtTotalCosts.Name = "txtTotalCosts";
            this.txtTotalCosts.Size = new System.Drawing.Size(0, 13);
            this.txtTotalCosts.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 286);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Client";
            // 
            // cboxClient
            // 
            this.cboxClient.FormattingEnabled = true;
            this.cboxClient.Location = new System.Drawing.Point(102, 286);
            this.cboxClient.Name = "cboxClient";
            this.cboxClient.Size = new System.Drawing.Size(307, 21);
            this.cboxClient.TabIndex = 11;
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(183, 319);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(75, 23);
            this.btn_add.TabIndex = 12;
            this.btn_add.Text = "Add";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // AddOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 354);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.cboxClient);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtTotalCosts);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_RemoveProduct);
            this.Controls.Add(this.btn_AddProduct);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.label1);
            this.Name = "AddOrder";
            this.Text = "AddOrder";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_Products)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker txtDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_AddProduct;
        private System.Windows.Forms.Button btn_RemoveProduct;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dg_Products;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label txtTotalCosts;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboxClient;
        private System.Windows.Forms.Button btn_add;
    }
}