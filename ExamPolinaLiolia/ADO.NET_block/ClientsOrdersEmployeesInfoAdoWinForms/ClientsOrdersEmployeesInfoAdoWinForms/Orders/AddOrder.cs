﻿using ADOTestConnectionLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientsOrdersEmployeesInfoAdoWinForms.Orders
{
    public partial class AddOrder : Form, INotifyPropertyChanged
    {
        private DataTable products;
        private Decimal totalPrice;
        public Decimal TotalPrice
        {
            get { return totalPrice; }
            set
            {
                totalPrice = value;
                NotifyPropertyChanged("TotalPrice");
            }
        }
        public AddOrder()
        {
            InitializeComponent();
            TotalPrice = 0;
            //binding txtTotalCosts to form's property:
            txtTotalCosts.DataBindings.Add(new Binding("Text", this, "TotalPrice"));
            //setting form field DataTable as data source for dataGridView products:
            dgProductsSettings();
            //combo box data source and settings:
            cboxClientsSettings();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            DataRow newRow = DBAccess.data.Tables["ORDERS"].NewRow();
            newRow[0] = -1; //temporary id

            string description = txtDescription.Text;
            newRow[1] = description;

            int clientID;
            if (cboxClient.SelectedValue != null)
            {
                clientID = Convert.ToInt32(cboxClient.SelectedValue);
                newRow[4] = clientID;
            }
            else
            {
                MessageBox.Show("Client id can not be empty!", "Error", MessageBoxButtons.OK,  MessageBoxIcon.Error);
                return;
            }

            decimal totalCosts = -1;
            Decimal.TryParse(txtTotalCosts.Text, out totalCosts);
            if (totalCosts != -1 && totalCosts > 0)
            {
                newRow[3] = totalCosts;
            }
            else
            {
                MessageBox.Show("Total costs can not be empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DateTime date = txtDate.Value;
            if (date != null)
            {
                newRow[2] = date;
            }
            else
            {
                MessageBox.Show("Date can not be empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DBAccess.data.Tables["ORDERS"].Rows.Add(newRow);
            int db_id = DBAccess.insertOrders();
            newRow[0] = db_id;


            //adding products to order
            if (!DBAccess.attachProductsToOrder(db_id, products))
                MessageBox.Show("Attention: your order was created, but it is empty yet! Don't forget to add products.",
                    "Attention!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        #region Components and data sources settings
        private void cboxClientsSettings()
        {
            cboxClient.DataSource = DBAccess.data.Tables["CLIENTS"].DefaultView;
            cboxClient.DisplayMember = "CLIENTS_NAME";
            cboxClient.ValueMember = "CLIENTS_ID";
            cboxClient.SelectedIndex = 0;
        }

        private void dgProductsSettings()
        {
            dg_Products.AutoGenerateColumns = false;
            dg_Products.DataSource = initializeProductsTable();
            dg_Products.Columns.Add("PRODUCTS_NAME", "Name");
            dg_Products.Columns["PRODUCTS_NAME"].DataPropertyName = "PRODUCTS_NAME";
            dg_Products.Columns["PRODUCTS_NAME"].Width = 100;
            dg_Products.Columns.Add("PRICE", "Price");
            dg_Products.Columns["PRICE"].DataPropertyName = "PRICE";
            dg_Products.Columns["PRICE"].Width = 80;
            dg_Products.Columns.Add("COUNT", "Count");
            dg_Products.Columns["COUNT"].DataPropertyName = "COUNT";
            dg_Products.Columns["COUNT"].Width = 80;
        }

        private DataView initializeProductsTable()
        {
            products = new DataTable();
            products.Columns.Add("PRODUCTS_ID", typeof(Int32));
            products.Columns.Add("PRODUCTS_NAME", typeof(string));
            products.Columns.Add("PRICE", typeof(decimal));
            products.Columns.Add("COUNT", typeof(Int32));
            return products.DefaultView;
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        #endregion

        #region Add/remove product actions
        private void btn_AddProduct_Click(object sender, EventArgs e)
        {
            AddProductToOrder addProductWindow = new AddProductToOrder();
            addProductWindow.ShowDialog();
            int productId = addProductWindow.ProductID;
            int count = addProductWindow.Count;
            if (productId != -1 && count > 0) //product was selected
            {
                DataRow sourceRow = DBAccess.data.Tables["PRODUCTS"].Select($"PRODUCTS_ID = {productId}").FirstOrDefault();
                if (sourceRow != null)
                {
                    DataRow dr = products.NewRow();
                    dr["PRODUCTS_ID"] = sourceRow["PRODUCTS_ID"];
                    dr["PRODUCTS_NAME"] = sourceRow["PRODUCTS_NAME"];
                    dr["PRICE"] = sourceRow["PRICE"];
                    dr["COUNT"] = count;
                    products.Rows.Add(dr);
                    TotalPrice += Convert.ToInt32(dr["PRICE"]) * count;
                }
            }
        }

        private void btn_RemoveProduct_Click(object sender, EventArgs e)
        {
            
            if (dg_Products.SelectedRows.Count > 0 && dg_Products.SelectedRows[0].Index >= 0)
            {
                DataGridViewRow curRow = dg_Products.SelectedRows[0];
                TotalPrice -= Convert.ToInt32(curRow.Cells["PRICE"].Value) * Convert.ToInt32(curRow.Cells["COUNT"].Value);
                dg_Products.Rows.RemoveAt(dg_Products.CurrentRow.Index);
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        #endregion

    }
}
