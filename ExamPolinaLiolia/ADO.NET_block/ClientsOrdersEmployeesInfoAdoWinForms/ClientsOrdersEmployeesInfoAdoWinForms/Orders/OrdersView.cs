﻿using ADOTestConnectionLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientsOrdersEmployeesInfoAdoWinForms.Orders
{
    public partial class OrdersView : Form
    {
        private BindingSource ordersBindingSource = new BindingSource();
        public OrdersView()
        {
            InitializeComponent();
            ordersBindingSource.DataSource = DBAccess.data;
            ordersBindingSource.DataMember = "ORDERS";
            dgv_orders.DataSource = ordersBindingSource;
        }

        private void dgv_orders_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv_orders.SelectedRows.Count > 0)
            {
                int selectedOrderID = Convert.ToInt32(dgv_orders.SelectedRows[0].Cells["ORDERS_ID"].Value);
                if (selectedOrderID >= 1)
                    dgv_products.DataSource = DBAccess.createOrderProductsTmpDataTable(selectedOrderID).AsDataView();
            }
        }

        private void btn_AddOrder_Click(object sender, EventArgs e)
        {
            AddOrder addOrderForm = new AddOrder();
            addOrderForm.ShowDialog();
        }

        private void btn_EditOrder_Click(object sender, EventArgs e)
        {
            showEditOrderWindow(dgv_orders);
        }

        private void showEditOrderWindow(DataGridView dg)
        {
            if (dg.SelectedRows.Count > 0 && dg.SelectedRows[0].Index >= 0)
            {
                int currentOrderID = Convert.ToInt32(dg.SelectedRows[0].Cells["ORDERS_ID"].Value);
                DataView dv = DBAccess.data.Tables["ORDERS"].DefaultView;
                dv.RowFilter = $"ORDERS_ID={currentOrderID}";
                EditOrder editOrderForm = new EditOrder(dv);
                editOrderForm.ShowDialog();

            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void btn_DeleteOrder_Click(object sender, EventArgs e)
        {
            if (dgv_orders.SelectedRows.Count > 0 && dgv_orders.SelectedRows[0].Index >= 0)
            {
                DialogResult result = MessageBox.Show("Do you want to delete selected order?",
                   "Confirmation",
                   MessageBoxButtons.YesNo,
                   MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    dgv_orders.Rows.RemoveAt(dgv_orders.CurrentRow.Index);
                    DBAccess.updateOrders();
                    //updating child tables (because of cascade deleting):
                    DBAccess.updateOrdersPositions();
                    dgv_orders.EndEdit();
                }
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to delete",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void OrdersView_FormClosing(object sender, FormClosingEventArgs e)
        {
            DBAccess.data.AcceptChanges();
        }
    }
}
