﻿using ADOTestConnectionLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientsOrdersEmployeesInfoAdoWinForms.Orders
{
    public partial class AddProductToOrder : Form
    {
        public int ProductID { get; set; }
        public int Count { get; set; }

        public AddProductToOrder()
        {
            InitializeComponent();
            ProductID = -1;
            Count = 1;
            cbox_ProductName.DataSource = DBAccess.data.Tables["PRODUCTS"].DefaultView;
            cbox_ProductName.DisplayMember = "PRODUCTS_NAME";
            cbox_ProductName.ValueMember = "PRODUCTS_ID";
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            if (cbox_ProductName.SelectedValue != null)
            {
                ProductID = Convert.ToInt32(cbox_ProductName.SelectedValue);
            }
            else
            {
                MessageBox.Show("Producte has to be selected first!", "Error", MessageBoxButtons.OK,  MessageBoxIcon.Error);
                return;
            }
            int count = -1;
            if (Int32.TryParse(txt_Count.Text, out count) == false)
            {
                MessageBox.Show("Wrong count!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
                Count = count;
            this.Close();
        }
    }
}
