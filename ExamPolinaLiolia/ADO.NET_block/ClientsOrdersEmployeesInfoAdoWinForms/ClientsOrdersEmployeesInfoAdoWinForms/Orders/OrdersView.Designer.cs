﻿namespace ClientsOrdersEmployeesInfoAdoWinForms.Orders
{
    partial class OrdersView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Products = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgv_products = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgv_orders = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_DeleteOrder = new System.Windows.Forms.Button();
            this.btn_EditOrder = new System.Windows.Forms.Button();
            this.btn_AddOrder = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_products)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_orders)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_Products
            // 
            this.lbl_Products.AutoSize = true;
            this.lbl_Products.Location = new System.Drawing.Point(12, 345);
            this.lbl_Products.Name = "lbl_Products";
            this.lbl_Products.Size = new System.Drawing.Size(90, 13);
            this.lbl_Products.TabIndex = 8;
            this.lbl_Products.Text = "Products in order:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgv_products);
            this.panel3.Location = new System.Drawing.Point(12, 361);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(707, 100);
            this.panel3.TabIndex = 7;
            // 
            // dgv_products
            // 
            this.dgv_products.AllowUserToAddRows = false;
            this.dgv_products.AllowUserToDeleteRows = false;
            this.dgv_products.AllowUserToOrderColumns = true;
            this.dgv_products.AllowUserToResizeColumns = false;
            this.dgv_products.AllowUserToResizeRows = false;
            this.dgv_products.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_products.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_products.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgv_products.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dgv_products.Location = new System.Drawing.Point(0, 0);
            this.dgv_products.Name = "dgv_products";
            this.dgv_products.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_products.Size = new System.Drawing.Size(707, 100);
            this.dgv_products.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv_orders);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(707, 275);
            this.panel2.TabIndex = 6;
            // 
            // dgv_orders
            // 
            this.dgv_orders.AllowUserToAddRows = false;
            this.dgv_orders.AllowUserToDeleteRows = false;
            this.dgv_orders.AllowUserToOrderColumns = true;
            this.dgv_orders.AllowUserToResizeColumns = false;
            this.dgv_orders.AllowUserToResizeRows = false;
            this.dgv_orders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_orders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_orders.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgv_orders.Location = new System.Drawing.Point(0, 0);
            this.dgv_orders.MultiSelect = false;
            this.dgv_orders.Name = "dgv_orders";
            this.dgv_orders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_orders.Size = new System.Drawing.Size(707, 275);
            this.dgv_orders.TabIndex = 0;
            this.dgv_orders.SelectionChanged += new System.EventHandler(this.dgv_orders_SelectionChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btn_DeleteOrder);
            this.panel4.Controls.Add(this.btn_EditOrder);
            this.panel4.Controls.Add(this.btn_AddOrder);
            this.panel4.Location = new System.Drawing.Point(14, 293);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(707, 38);
            this.panel4.TabIndex = 9;
            // 
            // btn_DeleteOrder
            // 
            this.btn_DeleteOrder.Location = new System.Drawing.Point(166, 4);
            this.btn_DeleteOrder.Name = "btn_DeleteOrder";
            this.btn_DeleteOrder.Size = new System.Drawing.Size(75, 23);
            this.btn_DeleteOrder.TabIndex = 2;
            this.btn_DeleteOrder.Text = "Delete";
            this.btn_DeleteOrder.UseVisualStyleBackColor = true;
            this.btn_DeleteOrder.Click += new System.EventHandler(this.btn_DeleteOrder_Click);
            // 
            // btn_EditOrder
            // 
            this.btn_EditOrder.Location = new System.Drawing.Point(85, 4);
            this.btn_EditOrder.Name = "btn_EditOrder";
            this.btn_EditOrder.Size = new System.Drawing.Size(75, 23);
            this.btn_EditOrder.TabIndex = 1;
            this.btn_EditOrder.Text = "Edit";
            this.btn_EditOrder.UseVisualStyleBackColor = true;
            this.btn_EditOrder.Click += new System.EventHandler(this.btn_EditOrder_Click);
            // 
            // btn_AddOrder
            // 
            this.btn_AddOrder.Location = new System.Drawing.Point(4, 4);
            this.btn_AddOrder.Name = "btn_AddOrder";
            this.btn_AddOrder.Size = new System.Drawing.Size(75, 23);
            this.btn_AddOrder.TabIndex = 0;
            this.btn_AddOrder.Text = "Add";
            this.btn_AddOrder.UseVisualStyleBackColor = true;
            this.btn_AddOrder.Click += new System.EventHandler(this.btn_AddOrder_Click);
            // 
            // OrdersView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 485);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.lbl_Products);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Name = "OrdersView";
            this.Text = "OrdersView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OrdersView_FormClosing);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_products)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_orders)).EndInit();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Products;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dgv_products;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgv_orders;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btn_DeleteOrder;
        private System.Windows.Forms.Button btn_EditOrder;
        private System.Windows.Forms.Button btn_AddOrder;
    }
}