﻿namespace ClientsOrdersEmployeesInfoAdoWinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Clients = new System.Windows.Forms.Button();
            this.btn_Orders = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Clients
            // 
            this.btn_Clients.Location = new System.Drawing.Point(13, 13);
            this.btn_Clients.Name = "btn_Clients";
            this.btn_Clients.Size = new System.Drawing.Size(75, 23);
            this.btn_Clients.TabIndex = 0;
            this.btn_Clients.Text = "Clients";
            this.btn_Clients.UseVisualStyleBackColor = true;
            this.btn_Clients.Click += new System.EventHandler(this.btn_Clients_Click);
            // 
            // btn_Orders
            // 
            this.btn_Orders.Location = new System.Drawing.Point(112, 13);
            this.btn_Orders.Name = "btn_Orders";
            this.btn_Orders.Size = new System.Drawing.Size(75, 23);
            this.btn_Orders.TabIndex = 1;
            this.btn_Orders.Text = "Orders";
            this.btn_Orders.UseVisualStyleBackColor = true;
            this.btn_Orders.Click += new System.EventHandler(this.btn_Orders_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 261);
            this.Controls.Add(this.btn_Orders);
            this.Controls.Add(this.btn_Clients);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Clients;
        private System.Windows.Forms.Button btn_Orders;
    }
}

