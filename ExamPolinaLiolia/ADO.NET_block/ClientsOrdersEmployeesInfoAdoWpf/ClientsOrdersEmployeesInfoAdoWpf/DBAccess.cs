﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    public class DBAccess
    {
        public static DataSet data;

        static DBAccess()
        {
            data = new DataSet("DB");
            data.Locale = System.Globalization.CultureInfo.InvariantCulture;
            fillDataSet();
            seedDataSetTables();
        }

        private static void seedDataSetTables()
        {
            if (data != null)
            {
                #region Banks seeders
                if (data.Tables["BANKS"].Rows.Count == 0)
                {
                    DataRow bank1 = data.Tables["BANKS"].NewRow();
                    bank1["BANKS_ID"] = 1;
                    bank1["BANKS_NAME"] = "bank1";
                    data.Tables["BANKS"].Rows.Add(bank1);
                    DataRow bank2 = data.Tables["BANKS"].NewRow();
                    bank2["BANKS_ID"] = 2;
                    bank2["BANKS_NAME"] = "bank2";
                    data.Tables["BANKS"].Rows.Add(bank2);
                    DataRow bank3 = data.Tables["BANKS"].NewRow();
                    bank3["BANKS_ID"] = 3;
                    bank3["BANKS_NAME"] = "bank3";
                    data.Tables["BANKS"].Rows.Add(bank3);
                    DataRow bank4 = data.Tables["BANKS"].NewRow();
                    bank4["BANKS_ID"] = 4;
                    bank4["BANKS_NAME"] = "bank4";
                    data.Tables["BANKS"].Rows.Add(bank4);
                    DataRow bank5 = data.Tables["BANKS"].NewRow();
                    bank5["BANKS_ID"] = 5;
                    bank5["BANKS_NAME"] = "bank5";
                    data.Tables["BANKS"].Rows.Add(bank5);
                    DBAccess.insertBanks();
                }
                #endregion
                
                #region Accounts seeders
                if (data.Tables["ACCOUNTS"].Rows.Count == 0)
                {
                    DataRow account1 = data.Tables["ACCOUNTS"].NewRow();
                    account1["ACCOUNTS_ID"] = 1;
                    account1["BANKS_ID"] = 1;
                    account1["ACCOUNTS_SUM"] = 12345.12;
                    account1["ACCOUNT"] = 994567344;
                    data.Tables["ACCOUNTS"].Rows.Add(account1);
                    DataRow account2 = data.Tables["ACCOUNTS"].NewRow();
                    account2["ACCOUNTS_ID"] = 2;
                    account2["BANKS_ID"] = 2;
                    account2["ACCOUNTS_SUM"] = 54;
                    account2["ACCOUNT"] = 34563322;
                    data.Tables["ACCOUNTS"].Rows.Add(account2);
                    DataRow account3 = data.Tables["ACCOUNTS"].NewRow();
                    account3["ACCOUNTS_ID"] = 3;
                    account3["BANKS_ID"] = 3;
                    account3["ACCOUNTS_SUM"] = 55543.98;
                    account3["ACCOUNT"] = 5543223456;
                    data.Tables["ACCOUNTS"].Rows.Add(account3);
                    DataRow account4 = data.Tables["ACCOUNTS"].NewRow();
                    account4["ACCOUNTS_ID"] = 4;
                    account4["BANKS_ID"] = 2;
                    account4["ACCOUNTS_SUM"] = 987;
                    account4["ACCOUNT"] = 1255643;
                    data.Tables["ACCOUNTS"].Rows.Add(account4);
                    DataRow account5 = data.Tables["ACCOUNTS"].NewRow();
                    account5["ACCOUNTS_ID"] = 5;
                    account5["BANKS_ID"] = 3;
                    account5["ACCOUNTS_SUM"] = 8766544;
                    account5["ACCOUNT"] = 23456753;
                    data.Tables["ACCOUNTS"].Rows.Add(account5);
                    DataRow account6 = data.Tables["ACCOUNTS"].NewRow();
                    account6["ACCOUNTS_ID"] = 6;
                    account6["BANKS_ID"] = 4;
                    account6["ACCOUNTS_SUM"] = 654;
                    account6["ACCOUNT"] = 87567788;
                    data.Tables["ACCOUNTS"].Rows.Add(account6);
                    DataRow account7 = data.Tables["ACCOUNTS"].NewRow();
                    account7["ACCOUNTS_ID"] = 7;
                    account7["BANKS_ID"] = 5;
                    account7["ACCOUNTS_SUM"] = 23.9;
                    account7["ACCOUNT"] = 6532245;
                    data.Tables["ACCOUNTS"].Rows.Add(account7);
                    DBAccess.insertAccounts();
                }
                #endregion

                #region Clients seeders
                if (data.Tables["CLIENTS"].Rows.Count == 0)
                {
                    DataRow client1 = data.Tables["CLIENTS"].NewRow();
                    client1["CLIENTS_ID"] = 1;
                    client1["CLIENTS_NAME"] = "Petr";
                    client1["CLIENTS_PHONE"] = "+380504563434";
                    client1["CLIENTS_MAIL"] = "petr@mail.ru";
                    data.Tables["CLIENTS"].Rows.Add(client1);
                    DataRow client2 = data.Tables["CLIENTS"].NewRow();
                    client2["CLIENTS_ID"] = 2;
                    client2["CLIENTS_NAME"] = "Ivan";
                    client2["CLIENTS_PHONE"] = "+380674563422";
                    client2["CLIENTS_MAIL"] = "ivan@gmail.com";
                    data.Tables["CLIENTS"].Rows.Add(client2);
                    DataRow client3 = data.Tables["CLIENTS"].NewRow();
                    client3["CLIENTS_ID"] = 3;
                    client3["CLIENTS_NAME"] = "Elena";
                    client3["CLIENTS_PHONE"] = "+38044567834";
                    client3["CLIENTS_MAIL"] = "elena@ukr.net";
                    data.Tables["CLIENTS"].Rows.Add(client3);
                    DBAccess.insertClients();
                }
                #endregion

                #region AccountsToClients seeders
                if (data.Tables["ACCOUNTS_TO_CLIENTS"].Rows.Count == 0)
                {
                    DataRow account_to_client1 = data.Tables["ACCOUNTS_TO_CLIENTS"].NewRow();
                    account_to_client1["ACCOUNTS_TO_CLIENTS_ID"] = 1;
                    account_to_client1["ACCOUNTS_ID"] = 1;
                    account_to_client1["CLIENTS_ID"] = 1;
                    data.Tables["ACCOUNTS_TO_CLIENTS"].Rows.Add(account_to_client1);
                    DataRow account_to_client2 = data.Tables["ACCOUNTS_TO_CLIENTS"].NewRow();
                    account_to_client2["ACCOUNTS_TO_CLIENTS_ID"] = 2;
                    account_to_client2["ACCOUNTS_ID"] = 2;
                    account_to_client2["CLIENTS_ID"] = 1;
                    data.Tables["ACCOUNTS_TO_CLIENTS"].Rows.Add(account_to_client2);
                    DataRow account_to_client3 = data.Tables["ACCOUNTS_TO_CLIENTS"].NewRow();
                    account_to_client3["ACCOUNTS_TO_CLIENTS_ID"] = 3;
                    account_to_client3["ACCOUNTS_ID"] = 3;
                    account_to_client3["CLIENTS_ID"] = 1;
                    data.Tables["ACCOUNTS_TO_CLIENTS"].Rows.Add(account_to_client3);
                    DataRow account_to_client4 = data.Tables["ACCOUNTS_TO_CLIENTS"].NewRow();
                    account_to_client4["ACCOUNTS_TO_CLIENTS_ID"] = 4;
                    account_to_client4["ACCOUNTS_ID"] = 4;
                    account_to_client4["CLIENTS_ID"] = 2;
                    data.Tables["ACCOUNTS_TO_CLIENTS"].Rows.Add(account_to_client4);
                    DataRow account_to_client5 = data.Tables["ACCOUNTS_TO_CLIENTS"].NewRow();
                    account_to_client5["ACCOUNTS_TO_CLIENTS_ID"] = 5;
                    account_to_client5["ACCOUNTS_ID"] = 5;
                    account_to_client5["CLIENTS_ID"] = 2;
                    data.Tables["ACCOUNTS_TO_CLIENTS"].Rows.Add(account_to_client5);
                    DataRow account_to_client6 = data.Tables["ACCOUNTS_TO_CLIENTS"].NewRow();
                    account_to_client6["ACCOUNTS_TO_CLIENTS_ID"] = 6;
                    account_to_client6["ACCOUNTS_ID"] = 6;
                    account_to_client6["CLIENTS_ID"] = 3;
                    data.Tables["ACCOUNTS_TO_CLIENTS"].Rows.Add(account_to_client6);
                    DataRow account_to_client7 = data.Tables["ACCOUNTS_TO_CLIENTS"].NewRow();
                    account_to_client7["ACCOUNTS_TO_CLIENTS_ID"] = 7;
                    account_to_client7["ACCOUNTS_ID"] = 7;
                    account_to_client7["CLIENTS_ID"] = 3;
                    data.Tables["ACCOUNTS_TO_CLIENTS"].Rows.Add(account_to_client7);
                    DBAccess.insertAccountsToClients();
                }
                #endregion

                #region Departments seeders
                if (data.Tables["DEPARTMENTS"].Rows.Count == 0)
                {
                    DataRow dept1 = data.Tables["DEPARTMENTS"].NewRow();
                    dept1["DEPARTMENTS_ID"] = 1;
                    dept1["DEPARTMENTS_NAME"] = "Development";
                    dept1["REGION_INFO"] = "Kharkiv";
                    data.Tables["DEPARTMENTS"].Rows.Add(dept1);
                    DataRow dept2 = data.Tables["DEPARTMENTS"].NewRow();
                    dept2["DEPARTMENTS_ID"] = 2;
                    dept2["DEPARTMENTS_NAME"] = "Sales";
                    dept2["REGION_INFO"] = "Kharkiv";
                    data.Tables["DEPARTMENTS"].Rows.Add(dept2);
                    DataRow dept3 = data.Tables["DEPARTMENTS"].NewRow();
                    dept3["DEPARTMENTS_ID"] = 3;
                    dept3["DEPARTMENTS_NAME"] = "Marketing";
                    dept3["REGION_INFO"] = "Kharkiv";
                    data.Tables["DEPARTMENTS"].Rows.Add(dept3);
                    DBAccess.insertDepartments();
                }

                #endregion

                #region Employees seeders
                if (data.Tables["EMPLOYEES"].Rows.Count == 0)
                {
                    DataRow empl1 = data.Tables["EMPLOYEES"].NewRow();
                    empl1["EMPLOYEES_ID"] = 1;
                    empl1["EMPLOYEES_NAME"] = "Gennadii";
                    empl1["PHONE"] = "111223";
                    empl1["MAIL"] = "gena@gmail.com";
                    empl1["SALARY"] = 1234.5;
                    empl1["DEPARTMENTS_ID"] = 1;
                    data.Tables["EMPLOYEES"].Rows.Add(empl1);
                    DataRow empl2 = data.Tables["EMPLOYEES"].NewRow();
                    empl2["EMPLOYEES_ID"] = 2;
                    empl2["EMPLOYEES_NAME"] = "Anna";
                    empl2["PHONE"] = "76546";
                    empl2["MAIL"] = "anna@gmail.com";
                    empl2["SALARY"] = 2356;
                    empl2["DEPARTMENTS_ID"] = 1;
                    data.Tables["EMPLOYEES"].Rows.Add(empl2);
                    DataRow empl3 = data.Tables["EMPLOYEES"].NewRow();
                    empl3["EMPLOYEES_ID"] = 3;
                    empl3["EMPLOYEES_NAME"] = "Vladimir";
                    empl3["PHONE"] = "9865468";
                    empl3["MAIL"] = "vova@gmail.com";
                    empl3["SALARY"] = 675;
                    empl3["DEPARTMENTS_ID"] = 2;
                    data.Tables["EMPLOYEES"].Rows.Add(empl3);
                    DataRow empl4 = data.Tables["EMPLOYEES"].NewRow();
                    empl4["EMPLOYEES_ID"] = 4;
                    empl4["EMPLOYEES_NAME"] = "Nadezhda";
                    empl4["PHONE"] = "456643";
                    empl4["MAIL"] = "nadia@gmail.com";
                    empl4["SALARY"] = 1342;
                    empl4["DEPARTMENTS_ID"] = 2;
                    data.Tables["EMPLOYEES"].Rows.Add(empl4);
                    DataRow empl5 = data.Tables["EMPLOYEES"].NewRow();
                    empl5["EMPLOYEES_ID"] = 1;
                    empl5["EMPLOYEES_NAME"] = "Anton";
                    empl5["PHONE"] = "53233";
                    empl5["MAIL"] = "anton@gmail.com";
                    empl5["SALARY"] = 598;
                    empl5["DEPARTMENTS_ID"] = 3;
                    data.Tables["EMPLOYEES"].Rows.Add(empl5);

                    DBAccess.insertEmployee();
                }
                #endregion

                #region Orders seeders
                if (data.Tables["ORDERS"].Rows.Count == 0)
                {
                    DataRow order1 = data.Tables["ORDERS"].NewRow();
                    order1["ORDERS_ID"] = 1;
                    order1["DESCRIPTION"] = "descr1";
                    order1["ORDERS_DATE"] = DateTime.Now;
                    order1["TOTAL_COSTS"] = 15;
                    order1["CLIENTS_ID"] = 1;
                    data.Tables["ORDERS"].Rows.Add(order1);
                    DataRow order2 = data.Tables["ORDERS"].NewRow();
                    order2["ORDERS_ID"] = 2;
                    order2["DESCRIPTION"] = "descr2";
                    order2["ORDERS_DATE"] = DateTime.Now;
                    order2["TOTAL_COSTS"] = 20;
                    order2["CLIENTS_ID"] = 1;
                    data.Tables["ORDERS"].Rows.Add(order2);
                    DataRow order3 = data.Tables["ORDERS"].NewRow();
                    order3["ORDERS_ID"] = 3;
                    order3["DESCRIPTION"] = "descr3";
                    order3["ORDERS_DATE"] = DateTime.Now;
                    order3["TOTAL_COSTS"] = 10;
                    order3["CLIENTS_ID"] = 2;
                    data.Tables["ORDERS"].Rows.Add(order3);
                    DataRow order4 = data.Tables["ORDERS"].NewRow();
                    order4["ORDERS_ID"] = 4;
                    order4["DESCRIPTION"] = "descr4";
                    order4["ORDERS_DATE"] = DateTime.Now;
                    order4["TOTAL_COSTS"] = 12;
                    order4["CLIENTS_ID"] = 2;
                    data.Tables["ORDERS"].Rows.Add(order4);
                    DataRow order5 = data.Tables["ORDERS"].NewRow();
                    order5["ORDERS_ID"] = 5;
                    order5["DESCRIPTION"] = "descr5";
                    order5["ORDERS_DATE"] = DateTime.Now;
                    order5["TOTAL_COSTS"] = 35;
                    order5["CLIENTS_ID"] = 3;
                    data.Tables["ORDERS"].Rows.Add(order5);

                    DBAccess.insertOrders();
                }
                #endregion

                #region Products seeders
                if (data.Tables["PRODUCTS"].Rows.Count == 0)
                {
                    DataRow product1 = data.Tables["PRODUCTS"].NewRow();
                    product1["PRODUCTS_ID"] = 1;
                    product1["PRODUCTS_NAME"] = "bread";
                    product1["PRICE"] = 5;
                    data.Tables["PRODUCTS"].Rows.Add(product1);
                    DataRow product2 = data.Tables["PRODUCTS"].NewRow();
                    product2["PRODUCTS_ID"] = 2;
                    product2["PRODUCTS_NAME"] = "water";
                    product2["PRICE"] = 10;
                    data.Tables["PRODUCTS"].Rows.Add(product2);
                    DataRow product3 = data.Tables["PRODUCTS"].NewRow();
                    product3["PRODUCTS_ID"] = 3;
                    product3["PRODUCTS_NAME"] = "milk";
                    product3["PRICE"] = 15;
                    data.Tables["PRODUCTS"].Rows.Add(product3);
                    DataRow product4 = data.Tables["PRODUCTS"].NewRow();
                    product4["PRODUCTS_ID"] = 4;
                    product4["PRODUCTS_NAME"] = "candy";
                    product4["PRICE"] = 1;
                    data.Tables["PRODUCTS"].Rows.Add(product4);
                    DataRow product5 = data.Tables["PRODUCTS"].NewRow();
                    product5["PRODUCTS_ID"] = 5;
                    product5["PRODUCTS_NAME"] = "nuts";
                    product5["PRICE"] = 30;
                    data.Tables["PRODUCTS"].Rows.Add(product5);

                    DBAccess.insertProducts();
                }
                #endregion

                #region Order positions seeders
                if (data.Tables["ORDERS_POSITIONS"].Rows.Count == 0)
                {
                    DataRow order_position1 = data.Tables["ORDERS_POSITIONS"].NewRow();
                    order_position1["ORDERS_POSITIONS_ID"] = 1;
                    order_position1["ORDERS_ID"] = 1;
                    order_position1["PRODUCTS_ID"] = 3;
                    order_position1["PRICE"] = 15;
                    order_position1["ITEM_COUNT"] = 1;
                    data.Tables["ORDERS_POSITIONS"].Rows.Add(order_position1);
                    DataRow order_position2 = data.Tables["ORDERS_POSITIONS"].NewRow();
                    order_position2["ORDERS_POSITIONS_ID"] = 2;
                    order_position2["ORDERS_ID"] = 2;
                    order_position2["PRODUCTS_ID"] = 1;
                    order_position2["PRICE"] = 5;
                    order_position2["ITEM_COUNT"] = 2;
                    data.Tables["ORDERS_POSITIONS"].Rows.Add(order_position2);
                    DataRow order_position3 = data.Tables["ORDERS_POSITIONS"].NewRow();
                    order_position3["ORDERS_POSITIONS_ID"] = 3;
                    order_position3["ORDERS_ID"] = 2;
                    order_position3["PRODUCTS_ID"] = 2;
                    order_position3["PRICE"] = 10;
                    order_position3["ITEM_COUNT"] = 1;
                    data.Tables["ORDERS_POSITIONS"].Rows.Add(order_position3);
                    DataRow order_position4 = data.Tables["ORDERS_POSITIONS"].NewRow();
                    order_position4["ORDERS_POSITIONS_ID"] = 4;
                    order_position4["ORDERS_ID"] = 3;
                    order_position4["PRODUCTS_ID"] = 2;
                    order_position4["PRICE"] = 10;
                    order_position4["ITEM_COUNT"] = 1;
                    data.Tables["ORDERS_POSITIONS"].Rows.Add(order_position4);
                    DataRow order_position5 = data.Tables["ORDERS_POSITIONS"].NewRow();
                    order_position5["ORDERS_POSITIONS_ID"] = 5;
                    order_position5["ORDERS_ID"] = 4;
                    order_position5["PRODUCTS_ID"] = 4;
                    order_position5["PRICE"] = 1;
                    order_position5["ITEM_COUNT"] = 12;
                    data.Tables["ORDERS_POSITIONS"].Rows.Add(order_position5);
                    DataRow order_position6 = data.Tables["ORDERS_POSITIONS"].NewRow();
                    order_position6["ORDERS_POSITIONS_ID"] = 6;
                    order_position6["ORDERS_ID"] = 5;
                    order_position6["PRODUCTS_ID"] = 5;
                    order_position6["PRICE"] = 30;
                    order_position6["ITEM_COUNT"] = 1;
                    data.Tables["ORDERS_POSITIONS"].Rows.Add(order_position6);
                    DataRow order_position7 = data.Tables["ORDERS_POSITIONS"].NewRow();
                    order_position7["ORDERS_POSITIONS_ID"] = 7;
                    order_position7["ORDERS_ID"] = 5;
                    order_position7["PRODUCTS_ID"] = 1;
                    order_position7["PRICE"] = 5;
                    order_position7["ITEM_COUNT"] = 1;
                    data.Tables["ORDERS_POSITIONS"].Rows.Add(order_position7);
                    DBAccess.insertOrdersPositions();
                }
                #endregion


            }
        }

        public static void fillDataSet()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using ( SQLiteConnection con = new SQLiteConnection(connectionString))
            {
                try
                {
                    con.Open();
                     
                    SQLiteDataAdapter adapter;

                    adapter = new SQLiteDataAdapter(BanksHelper.selectAll(), con);
                    adapter.Fill(data, "BANKS");

                    adapter = new SQLiteDataAdapter(AccountsHelper.selectAll(), con);
                    adapter.Fill(data, "ACCOUNTS");

                    // Establish a relationship between the two tables.
                    DataRelation relationBanksAccounts = new DataRelation("BanksAccounts",
                        data.Tables["BANKS"].Columns["BANKS_ID"],
                        data.Tables["ACCOUNTS"].Columns["BANKS_ID"]);    
                    data.Relations.Add(relationBanksAccounts);
                    relationBanksAccounts.ChildKeyConstraint.DeleteRule = Rule.Cascade;
                    relationBanksAccounts.ChildKeyConstraint.UpdateRule = Rule.Cascade;

                    adapter = new SQLiteDataAdapter(ClientsHelper.selectAll(), con);
                    adapter.Fill(data, "CLIENTS");

                    adapter = new SQLiteDataAdapter(AccountsToClientsHelper.selectAll(), con);
                    adapter.Fill(data, "ACCOUNTS_TO_CLIENTS");
                    
                    // Establish many-to-many relationship between tables.
                    DataRelation relationAccountsToClients = new DataRelation("Accounts_AccountsToClients",
                        data.Tables["ACCOUNTS"].Columns["ACCOUNTS_ID"],
                        data.Tables["ACCOUNTS_TO_CLIENTS"].Columns["ACCOUNTS_ID"]);
                    data.Relations.Add(relationAccountsToClients);
                    relationAccountsToClients.ChildKeyConstraint.DeleteRule = Rule.Cascade;
                    relationAccountsToClients.ChildKeyConstraint.UpdateRule = Rule.Cascade;                 

                    relationAccountsToClients = new DataRelation("Clients_AccountsToClients",
                        data.Tables["CLIENTS"].Columns["CLIENTS_ID"],
                        data.Tables["ACCOUNTS_TO_CLIENTS"].Columns["CLIENTS_ID"]);
                    data.Relations.Add(relationAccountsToClients);
                    relationAccountsToClients.ChildKeyConstraint.DeleteRule = Rule.Cascade;
                    relationAccountsToClients.ChildKeyConstraint.UpdateRule = Rule.Cascade;
                    
                    adapter = new SQLiteDataAdapter(OrdersHelper.selectAll(), con);
                    adapter.Fill(data, "ORDERS");

                    // Establish a relationship between the two tables.
                    DataRelation relationOrdersClients = new DataRelation("OrdersClients",
                        data.Tables["CLIENTS"].Columns["CLIENTS_ID"],
                        data.Tables["ORDERS"].Columns["CLIENTS_ID"]);
                    data.Relations.Add(relationOrdersClients);
                    relationOrdersClients.ChildKeyConstraint.DeleteRule = Rule.Cascade;
                    relationOrdersClients.ChildKeyConstraint.UpdateRule = Rule.Cascade;
                    
                    adapter = new SQLiteDataAdapter(DepartmentsHelper.selectAll(), con);
                    adapter.Fill(data, "DEPARTMENTS");

                    adapter = new SQLiteDataAdapter(EmployeesHelper.selectAll(), con);
                    adapter.Fill(data, "EMPLOYEES");

                    // Establish a relationship between the two tables.
                    DataRelation relationEmployeesDepatrments = new DataRelation("EmployeesDepatrments",
                        data.Tables["DEPARTMENTS"].Columns["DEPARTMENTS_ID"],
                        data.Tables["EMPLOYEES"].Columns["DEPARTMENTS_ID"]);
                    data.Relations.Add(relationEmployeesDepatrments);
                    

                    adapter = new SQLiteDataAdapter(ProductsHelper.selectAll(), con);
                    adapter.Fill(data, "PRODUCTS");

                    adapter = new SQLiteDataAdapter(OrdersPositionsHelper.selectAll(), con);
                    adapter.Fill(data, "ORDERS_POSITIONS");

                    // Establish many-to-many relationship between tables.
                    DataRelation relationOrdersToPositions = new DataRelation("Products_OrdersToPositions",
                        data.Tables["PRODUCTS"].Columns["PRODUCTS_ID"],
                        data.Tables["ORDERS_POSITIONS"].Columns["PRODUCTS_ID"]);
                    data.Relations.Add(relationOrdersToPositions);
                    relationOrdersToPositions.ChildKeyConstraint.DeleteRule = Rule.Cascade;
                    relationOrdersToPositions.ChildKeyConstraint.UpdateRule = Rule.Cascade;

                    relationOrdersToPositions = new DataRelation("Orders_OrdersToPositions",
                        data.Tables["ORDERS"].Columns["ORDERS_ID"],
                        data.Tables["ORDERS_POSITIONS"].Columns["ORDERS_ID"]);
                    data.Relations.Add(relationOrdersToPositions);
                    relationOrdersToPositions.ChildKeyConstraint.DeleteRule = Rule.Cascade;
                    relationOrdersToPositions.ChildKeyConstraint.UpdateRule = Rule.Cascade; 
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        #region Employees
        public static DataView getEmployees()
        {
            return data.Tables["EMPLOYEES"].AsDataView();
        }

        public static SQLiteDataAdapter getEmployeesAdapter(SQLiteConnection conn)
        {
            // Create data adapter
            SQLiteDataAdapter da = new SQLiteDataAdapter();

            // insert command
            SQLiteCommand insertCmd = new SQLiteCommand(EmployeesHelper.insertEmployee(), conn);
            // map parameters
            //parm = insertCmd.Parameters.Add("@ID", DbType.Int32, 0, "EMPLOYEES_ID");
            //parm.SourceVersion = DataRowVersion.Current;
            SQLiteParameter parm = insertCmd.Parameters.Add("@Name", DbType.String, 100, "EMPLOYEES_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Phone", DbType.String, 100, "PHONE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Mail", DbType.String, 100, "MAIL");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Salary", DbType.Int32, 0, "SALARY");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@DeptID", DbType.Int32, 0, "DEPARTMENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //delete command
            SQLiteCommand deleteCmd = new SQLiteCommand(EmployeesHelper.deleteEmployee(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@ID", DbType.Int32, 0, "EMPLOYEES_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SQLiteCommand updateCmd = new SQLiteCommand(EmployeesHelper.updateEmployees(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Name", DbType.String, 100, "EMPLOYEES_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Phone", DbType.String, 100, "PHONE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Mail", DbType.String, 100, "MAIL");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Salary", DbType.Int32, 0, "SALARY");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@DeptID", DbType.Int32, 0, "DEPARTMENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ID", DbType.Int32, 10, "EMPLOYEES_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SQLiteCommand getIDCmd = new SQLiteCommand(@"SELECT last_insert_rowid()", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateEmployees()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SQLiteDataAdapter da = DBAccess.getEmployeesAdapter(conn);

                    // Update database
                    da.Update(data, "EMPLOYEES");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertEmployee()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SQLiteDataAdapter da = DBAccess.getEmployeesAdapter(conn);
              
                    da.Update(data, "EMPLOYEES");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }  
            }
            return id;
        }
        #endregion

        #region Departments
        internal static DataView getDepartments()
        {
            return data.Tables["DEPARTMENTS"].AsDataView();
        }

        
        public static SQLiteDataAdapter getDepartmentsAdapter(SQLiteConnection conn)
        {
            // Create data adapter
            SQLiteDataAdapter da = new SQLiteDataAdapter();

            // insert command
            SQLiteCommand insertCmd = new SQLiteCommand(DepartmentsHelper.insertDepartment(), conn);
            // map parameters
            SQLiteParameter parm = insertCmd.Parameters.Add("@Name", DbType.String, 100, "DEPARTMENTS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Region", DbType.String, 100, "REGION_INFO");
            parm.SourceVersion = DataRowVersion.Current;

            //delete command
            SQLiteCommand deleteCmd = new SQLiteCommand(DepartmentsHelper.deleteDepartment(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@ID", DbType.Int32, 0, "DEPARTMENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SQLiteCommand updateCmd = new SQLiteCommand(DepartmentsHelper.updateDepartments(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Name", DbType.String, 100, "DEPARTMENTS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Region", DbType.String, 100, "REGION_INFO");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ID", DbType.Int32, 10, "DEPARTMENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SQLiteCommand getIDCmd = new SQLiteCommand(@"SELECT last_insert_rowid()", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateDepartments()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SQLiteDataAdapter da = DBAccess.getDepartmentsAdapter(conn);

                    // Update database
                    da.Update(data, "DEPARTMENTS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertDepartments()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SQLiteDataAdapter da = DBAccess.getDepartmentsAdapter(conn);

                    da.Update(data, "DEPARTMENTS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }

        #endregion

        #region Clients
        internal static DataView getClients()
        {
            return data.Tables["CLIENTS"].AsDataView();
        }

        public static SQLiteDataAdapter getClientsAdapter(SQLiteConnection conn)
        {
            // Create data adapter
            SQLiteDataAdapter da = new SQLiteDataAdapter();

            // insert command
            SQLiteCommand insertCmd = new SQLiteCommand(ClientsHelper.insertClient(), conn);
            // map parameters
            SQLiteParameter parm = insertCmd.Parameters.Add("@Name", DbType.String, 100, "CLIENTS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Phone", DbType.String, 100, "CLIENTS_PHONE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Mail", DbType.String, 100, "CLIENTS_MAIL");
            parm.SourceVersion = DataRowVersion.Current;

            //delete command
            SQLiteCommand deleteCmd = new SQLiteCommand(ClientsHelper.deleteClient(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@Id", DbType.Int32, 0, "CLIENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SQLiteCommand updateCmd = new SQLiteCommand(ClientsHelper.updateClients(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Name", DbType.String, 100, "CLIENTS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Phone", DbType.String, 100, "CLIENTS_PHONE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Mail", DbType.String, 100, "CLIENTS_MAIL");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Id", DbType.Int32, 10, "CLIENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SQLiteCommand getIDCmd = new SQLiteCommand(@"SELECT last_insert_rowid()", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateClients()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SQLiteDataAdapter da = DBAccess.getClientsAdapter(conn);

                    // Update database
                    da.Update(data, "CLIENTS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertClients()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SQLiteDataAdapter da = DBAccess.getClientsAdapter(conn);

                    da.Update(data, "CLIENTS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }
        #endregion

        #region AccountsToClients
        public static SQLiteDataAdapter getAccountsToClientsAdapter(SQLiteConnection conn)
        {
            // Create data adapter
            SQLiteDataAdapter da = new SQLiteDataAdapter();

            // insert command
            SQLiteCommand insertCmd = new SQLiteCommand(AccountsToClientsHelper.insertAccountsToClients(), conn);
            // map parameters
            SQLiteParameter parm = insertCmd.Parameters.Add("@Accounts_id", DbType.Int32, 0, "ACCOUNTS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Clients_id", DbType.Int32, 0, "CLIENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Indication", DbType.Int32, 0, "INDICATION");
            parm.SourceVersion = DataRowVersion.Current;

            //delete command
            SQLiteCommand deleteCmd = new SQLiteCommand(AccountsToClientsHelper.deleteAccountsToClients(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@ID", DbType.Int32, 0, "ACCOUNTS_TO_CLIENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SQLiteCommand updateCmd = new SQLiteCommand(ClientsHelper.updateClients(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Accounts_id", DbType.Int32, 0, "ACCOUNTS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Clients_id", DbType.Int32, 0, "CLIENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Indication", DbType.Int32, 0, "INDICATION");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ID", DbType.Int32, 0, "ACCOUNTS_TO_CLIENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SQLiteCommand getIDCmd = new SQLiteCommand(@"SELECT last_insert_rowid()", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateAccountsToClients()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SQLiteDataAdapter da = DBAccess.getAccountsToClientsAdapter(conn);

                    // Update database
                    da.Update(data, "ACCOUNTS_TO_CLIENTS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertAccountsToClients()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SQLiteDataAdapter da = DBAccess.getAccountsToClientsAdapter(conn);

                    da.Update(data, "ACCOUNTS_TO_CLIENTS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }
        #endregion

        #region Accounts
        public static DataView getAccounts()
        {
            return data.Tables["ACCOUNTS"].AsDataView();
        }

        public static SQLiteDataAdapter getAccountsAdapter(SQLiteConnection conn)
        {
            // Create data adapter
            SQLiteDataAdapter da = new SQLiteDataAdapter();

            // insert command
            SQLiteCommand insertCmd = new SQLiteCommand(AccountsHelper.insertAccount(), conn);
            // map parameters
            SQLiteParameter parm = insertCmd.Parameters.Add("@Description", DbType.String, 500, "DESCRIPTION");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Banks_id", DbType.Int32, 0, "BANKS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Accounts_sum", DbType.Decimal, 0, "ACCOUNTS_SUM");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Account", DbType.Int32, 0, "ACCOUNT");
            parm.SourceVersion = DataRowVersion.Current;

            //delete command
            SQLiteCommand deleteCmd = new SQLiteCommand(AccountsHelper.deleteAccount(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@ID", DbType.Int32, 0, "ACCOUNTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SQLiteCommand updateCmd = new SQLiteCommand(AccountsHelper.updateAccounts(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Description", DbType.String, 500, "DESCRIPTION");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Banks_id", DbType.Int32, 0, "BANKS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Accounts_sum", DbType.Decimal, 0, "ACCOUNTS_SUM");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Account", DbType.Int32, 0, "ACCOUNT");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ID", DbType.Int32, 0, "ACCOUNTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SQLiteCommand getIDCmd = new SQLiteCommand(@"SELECT last_insert_rowid()", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateAccounts()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SQLiteDataAdapter da = DBAccess.getAccountsAdapter(conn);

                    // Update database
                    da.Update(data, "ACCOUNTS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertAccounts()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SQLiteDataAdapter da = DBAccess.getAccountsAdapter(conn);

                    da.Update(data, "ACCOUNTS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }

        public static DataTable createClientsAccountsTmpDataTable(int client_id)
        {
            var accounts = from account in DBAccess.data.Tables["ACCOUNTS"].AsEnumerable()
                           join account_to_client in DBAccess.data.Tables["ACCOUNTS_TO_CLIENTS"].AsEnumerable()
                           on account["ACCOUNTS_ID"] equals account_to_client["ACCOUNTS_ID"]
                           join client in DBAccess.data.Tables["CLIENTS"].AsEnumerable()
                           on account_to_client["CLIENTS_ID"] equals client["CLIENTS_ID"]
                           where Convert.ToInt32(client["CLIENTS_ID"]) == client_id
                           select account;
            //creating temporary DataTable to collect all accounts of current client: 
            DataTable result_table = new DataTable();
            result_table.Columns.Add("ACCOUNTS_ID", typeof(Int32));
            result_table.Columns.Add("DESCRIPTION", typeof(string));
            result_table.Columns.Add("BANKS_ID", typeof(Int32));
            result_table.Columns.Add("ACCOUNTS_SUM", typeof(decimal));
            result_table.Columns.Add("ACCOUNT", typeof(Int32));

            foreach (DataRow source_dr in DBAccess.data.Tables["ACCOUNTS"].Rows)
            {
                foreach (DataRow dr in accounts)
                {
                    if (dr == source_dr)
                    {
                        DataRow row_copy = result_table.NewRow();
                        row_copy["ACCOUNTS_ID"] = dr["ACCOUNTS_ID"];
                        row_copy["DESCRIPTION"] = dr["DESCRIPTION"];
                        row_copy["BANKS_ID"] = dr["BANKS_ID"];
                        row_copy["ACCOUNTS_SUM"] = dr["ACCOUNTS_SUM"];
                        row_copy["ACCOUNT"] = dr["ACCOUNT"];
                        result_table.Rows.Add(row_copy);
                    }
                }
            }
            return result_table;
        }
        #endregion

        #region Banks
        public static DataView getBanks()
        {
            return data.Tables["BANKS"].AsDataView();
        }

        public static SQLiteDataAdapter getBanksAdapter(SQLiteConnection conn)
        {
            // Create data adapter
            SQLiteDataAdapter da = new SQLiteDataAdapter();

            // insert command
            SQLiteCommand insertCmd = new SQLiteCommand(BanksHelper.insertBank(), conn);
            // map parameters
            SQLiteParameter parm = insertCmd.Parameters.Add("@Name", DbType.String, 100, "BANKS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Region", DbType.String, 100, "BANKS_INFO");
            parm.SourceVersion = DataRowVersion.Current;

            //delete command
            SQLiteCommand deleteCmd = new SQLiteCommand(BanksHelper.deleteBank(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@ID", DbType.Int32, 0, "BANKS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SQLiteCommand updateCmd = new SQLiteCommand(BanksHelper.updateBanks(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Name", DbType.String, 100, "BANKS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Region", DbType.String, 100, "BANKS_INFO");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ID", DbType.Int32, 0, "BANKS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SQLiteCommand getIDCmd = new SQLiteCommand(@"SELECT last_insert_rowid()", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateBanks()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SQLiteDataAdapter da = DBAccess.getBanksAdapter(conn);

                    // Update database
                    da.Update(data, "BANKS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertBanks()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SQLiteDataAdapter da = DBAccess.getBanksAdapter(conn);

                    da.Update(data, "BANKS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }
        #endregion

        #region Products
        internal static DataView getProducts()
        {
            return data.Tables["PRODUCTS"].AsDataView();
        }


        public static SQLiteDataAdapter getProductsAdapter(SQLiteConnection conn)
        {
            // Create data adapter
            SQLiteDataAdapter da = new SQLiteDataAdapter();

            // insert command
            SQLiteCommand insertCmd = new SQLiteCommand(ProductsHelper.insertProduct(), conn);
            // map parameters
            SQLiteParameter parm = insertCmd.Parameters.Add("@Name", DbType.String, 100, "PRODUCTS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Price", DbType.Decimal, 0, "PRICE");
            parm.SourceVersion = DataRowVersion.Current;

            
            //delete command
            SQLiteCommand deleteCmd = new SQLiteCommand(ProductsHelper.deleteProduct(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@ID", DbType.Int32, 0, "PRODUCTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SQLiteCommand updateCmd = new SQLiteCommand(ProductsHelper.updateProducts(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Name", DbType.String, 100, "PRODUCTS_NAME");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Price", DbType.Decimal, 0, "PRICE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ID", DbType.Int32, 0, "PRODUCTS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SQLiteCommand getIDCmd = new SQLiteCommand(@"SELECT last_insert_rowid()", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateProducts()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SQLiteDataAdapter da = DBAccess.getProductsAdapter(conn);

                    // Update database
                    da.Update(data, "PRODUCTS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertProducts()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SQLiteDataAdapter da = DBAccess.getProductsAdapter(conn);

                    da.Update(data, "PRODUCTS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }
        #endregion

        #region Orders
        internal static DataView getOrders()
        {
            return data.Tables["ORDERS"].AsDataView();
        }

        //todo refactor
        public static SQLiteDataAdapter getOrdersAdapter(SQLiteConnection conn)
        {
            // Create data adapter
            SQLiteDataAdapter da = new SQLiteDataAdapter();

            // insert command
            SQLiteCommand insertCmd = new SQLiteCommand(OrdersHelper.insertOrder(), conn);
            // map parameters
            SQLiteParameter parm = insertCmd.Parameters.Add("@Description", DbType.String, 500, "DESCRIPTION");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@TotalCosts", DbType.Decimal, 0, "TOTAL_COSTS");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@ClientsId", DbType.Int32, 0, "CLIENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Date", DbType.DateTime, 0, "ORDERS_DATE");
            parm.SourceVersion = DataRowVersion.Current;


            //delete command
            SQLiteCommand deleteCmd = new SQLiteCommand(OrdersHelper.deleteOrder(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@Id", DbType.Int32, 0, "ORDERS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SQLiteCommand updateCmd = new SQLiteCommand(OrdersHelper.updateOrders(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Description", DbType.String, 500, "DESCRIPTION");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@TotalCosts", DbType.Decimal, 0, "TOTAL_COSTS");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ClientsId", DbType.Int32, 0, "CLIENTS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Date", DbType.DateTime, 0, "ORDERS_DATE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Id", DbType.Int32, 0, "ORDERS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SQLiteCommand getIDCmd = new SQLiteCommand(@"SELECT last_insert_rowid()", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateOrders()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SQLiteDataAdapter da = DBAccess.getOrdersAdapter(conn);

                    // Update database
                    da.Update(data, "ORDERS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertOrders()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SQLiteDataAdapter da = DBAccess.getOrdersAdapter(conn);

                    da.Update(data, "ORDERS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }

        
        #endregion

        #region Orders positions
        public static SQLiteDataAdapter getOrdersPositionsAdapter(SQLiteConnection conn)
        {
            // Create data adapter
            SQLiteDataAdapter da = new SQLiteDataAdapter();

            // insert command
            SQLiteCommand insertCmd = new SQLiteCommand(OrdersPositionsHelper.insertOrderPosition(), conn);
            // map parameters
            SQLiteParameter parm = insertCmd.Parameters.Add("@Orders_id", DbType.Int32, 0, "ORDERS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Products_id", DbType.Int32, 0, "PRODUCTS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Price", DbType.Decimal, 0, "PRICE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = insertCmd.Parameters.Add("@Item_count", DbType.Int32, 0, "ITEM_COUNT");
            parm.SourceVersion = DataRowVersion.Current;

            //delete command
            SQLiteCommand deleteCmd = new SQLiteCommand(OrdersPositionsHelper.deleteOrderPosition(), conn);
            // map parameters
            parm = deleteCmd.Parameters.Add("@ID", DbType.Int32, 0, "ORDERS_POSITIONS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            // update command
            SQLiteCommand updateCmd = new SQLiteCommand(OrdersPositionsHelper.updateOrdersPositions(), conn);
            // map parameters
            parm = updateCmd.Parameters.Add("@Orders_id", DbType.Int32, 0, "ORDERS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Products_id", DbType.Int32, 0, "PRODUCTS_ID");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Price", DbType.Decimal, 0, "PRICE");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@Item_count", DbType.Int32, 0, "ITEM_COUNT");
            parm.SourceVersion = DataRowVersion.Current;
            parm = updateCmd.Parameters.Add("@ID", DbType.Int32, 0, "ORDERS_POSITIONS_ID");
            parm.SourceVersion = DataRowVersion.Current;

            //select command
            SQLiteCommand getIDCmd = new SQLiteCommand(@"SELECT last_insert_rowid()", conn);

            // Update database
            da.InsertCommand = insertCmd;
            da.DeleteCommand = deleteCmd;
            da.UpdateCommand = updateCmd;
            da.SelectCommand = getIDCmd;

            return da;
        }

        public static void updateOrdersPositions()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    // get data adapter
                    SQLiteDataAdapter da = DBAccess.getOrdersPositionsAdapter(conn);

                    // Update database
                    da.Update(data, "ORDERS_POSITIONS");
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
        }

        public static int insertOrdersPositions()
        {
            int id = -1;
            string connectionString = ConfigurationManager.ConnectionStrings["SQLite"].ConnectionString;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SQLiteDataAdapter da = DBAccess.getOrdersPositionsAdapter(conn);

                    da.Update(data, "ORDERS_POSITIONS");
                    id = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Error: " + e);
                }
            }
            return id;
        }

        public static DataTable createOrderProductsTmpDataTable(int order_id)
        {
            var products = from product in DBAccess.data.Tables["PRODUCTS"].AsEnumerable()
                           join order_position in DBAccess.data.Tables["ORDERS_POSITIONS"].AsEnumerable()
                           on product["PRODUCTS_ID"] equals order_position["PRODUCTS_ID"]
                           join order in DBAccess.data.Tables["ORDERS"].AsEnumerable()
                           on order_position["ORDERS_ID"] equals order["ORDERS_ID"]
                           where Convert.ToInt32(order["ORDERS_ID"]) == order_id
                           select new
                           {
                               ProductsID = product["PRODUCTS_ID"],
                               Name = product["PRODUCTS_NAME"],
                               Price = product["PRICE"],
                               Count = order_position["ITEM_COUNT"]
                           };
            //creating temporary DataTable to collect all products from current order: 
            DataTable result_table = new DataTable();
            result_table.Columns.Add("PRODUCTS_ID", typeof(Int32));
            result_table.Columns.Add("PRODUCTS_NAME", typeof(string));
            result_table.Columns.Add("PRICE", typeof(decimal));
            result_table.Columns.Add("COUNT", typeof(Int32));

            foreach (DataRow source_dr in DBAccess.data.Tables["PRODUCTS"].Rows)
            {
                foreach (var dr in products)
                {
                    if (Convert.ToInt32(dr.ProductsID) == Convert.ToInt32(source_dr["PRODUCTS_ID"]))
                    {
                        DataRow row_copy = result_table.NewRow();
                        row_copy["PRODUCTS_ID"] = dr.ProductsID;
                        row_copy["PRODUCTS_NAME"] = dr.Name;
                        row_copy["PRICE"] = dr.Price;
                        row_copy["COUNT"] = dr.Count;
                        result_table.Rows.Add(row_copy);
                    }
                }
            }

            return result_table;
        }

        public static void detachAllProductsFromOrder(int orderID)
        {
            var rows_to_delete = DBAccess.data.Tables["ORDERS_POSITIONS"].AsEnumerable().Where(dr => Convert.ToInt32(dr["ORDERS_ID"]) == orderID );
            foreach (DataRow dr in rows_to_delete.ToList())
            {
               // DBAccess.data.Tables["ORDERS_POSITIONS"].Rows.Remove(dr);
                dr.Delete();
            }
            DBAccess.updateOrdersPositions();

        }

        public static bool attachProductsToOrder(int orderId, DataTable products)
        {
            bool attached = false;
            if (products.Rows.Count != 0)
            {
                foreach (DataRow product in products.Rows)
                {
                    int product_id = Convert.ToInt32(product["PRODUCTS_ID"]);
                    decimal price = Convert.ToDecimal(product["PRICE"]);
                    int count = Convert.ToInt32(product["COUNT"]);
                    DataRow ordersPositionsRow = DBAccess.data.Tables["ORDERS_POSITIONS"].NewRow();
                    ordersPositionsRow["ORDERS_POSITIONS_ID"] = -1; //temporary value
                    ordersPositionsRow["ORDERS_ID"] = orderId;
                    ordersPositionsRow["PRODUCTS_ID"] = product_id;
                    ordersPositionsRow["PRICE"] = price;
                    ordersPositionsRow["ITEM_COUNT"] = count;
                    DBAccess.data.Tables["ORDERS_POSITIONS"].Rows.Add(ordersPositionsRow);
                    int order_positions_id = DBAccess.insertOrdersPositions();
                    //changing temporary id value in dataset with real one
                    ordersPositionsRow["ORDERS_POSITIONS_ID"] = order_positions_id;
                }
                attached = true;
            }
            return attached;
        }
        #endregion



    }
}
