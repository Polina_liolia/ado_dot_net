﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ClientsOrdersEmployeesInfoAdoWpf.Converters
{
    public class ParentRelationsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DataRowView rowView = value as DataRowView;
            DataRow row = rowView.Row.GetParentRow(rowView.DataView.Table.ParentRelations[0]);
            string column = parameter as string;
            return row[column];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
