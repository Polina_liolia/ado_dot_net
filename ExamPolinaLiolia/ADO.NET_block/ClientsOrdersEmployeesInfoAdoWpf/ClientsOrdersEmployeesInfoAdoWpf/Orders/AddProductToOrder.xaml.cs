﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Orders
{
    /// <summary>
    /// Interaction logic for AddProductToOrder.xaml
    /// </summary>
    public partial class AddProductToOrder : Window
    {
        public int ProductID { get; set; }
        public int Count { get; set; }
        public AddProductToOrder()
        {
            InitializeComponent();
            ProductID = -1;
            Count = 1;
            cbox_ProductName.ItemsSource = DBAccess.data.Tables["PRODUCTS"].DefaultView;
            cbox_ProductName.DisplayMemberPath = "PRODUCTS_NAME";
            cbox_ProductName.SelectedValuePath = "PRODUCTS_ID";
        }

        private void btn_Add_Click(object sender, RoutedEventArgs e)
        {
            if (cbox_ProductName.SelectedValue != null)
            {
                ProductID = Convert.ToInt32(cbox_ProductName.SelectedValue);
            }
            else
            {
                MessageBox.Show("Producte has to be selected first!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            int count = -1;
            if (Int32.TryParse(txt_Count.Text, out count) == false)
            {
                MessageBox.Show("Wrong count!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
                Count = count;
            this.Close();
        }
    }
}
