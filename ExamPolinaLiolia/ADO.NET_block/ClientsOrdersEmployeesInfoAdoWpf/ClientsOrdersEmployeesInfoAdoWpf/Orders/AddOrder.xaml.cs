﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Orders
{
    /// <summary>
    /// Interaction logic for AddOrder.xaml
    /// </summary>
    public partial class AddOrder : Window, INotifyPropertyChanged
    {
        private DataTable products;
        private Decimal totalPrice;
        public Decimal TotalPrice
        {
            get { return totalPrice; }
            set
            {
                totalPrice = value;
                NotifyPropertyChanged("TotalPrice");
            }
        }
        public AddOrder()
        {
            InitializeComponent();
            txtTotalCosts.DataContext = this;
            TotalPrice = 0;
            dg_Products.ItemsSource = initializeProductsTable();
            cboxClient.ItemsSource = DBAccess.data.Tables["CLIENTS"].DefaultView;
            cboxClient.DisplayMemberPath = "CLIENTS_NAME";
            cboxClient.SelectedValuePath = "CLIENTS_ID";
        }

        private DataView initializeProductsTable()
        {
            products = new DataTable();
            products.Columns.Add("PRODUCTS_ID", typeof(Int32));
            products.Columns.Add("PRODUCTS_NAME", typeof(string));
            products.Columns.Add("PRICE", typeof(decimal));
            products.Columns.Add("COUNT", typeof(Int32));

            return products.DefaultView;
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            DataRow newRow = DBAccess.data.Tables["ORDERS"].NewRow();
            newRow[0] = -1; //temporary id

            string description = txtDescription.Text;
            newRow[1] = description;

            int clientID;
            if (cboxClient.SelectedValue != null)
            {
                clientID = Convert.ToInt32(cboxClient.SelectedValue);
                newRow[4] = clientID;
            }
            else
            {
                MessageBox.Show("Client id can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            decimal totalCosts = -1;
            Decimal.TryParse(txtTotalCosts.Text, out totalCosts);
            if (totalCosts != -1 && totalCosts > 0)
            {
                newRow[3] = totalCosts;
            }
            else
            {
                MessageBox.Show("Total costs can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DateTime date = txtDate.DisplayDate;
            if (date != null)
            {
                newRow[2] = date;
            }
            else
            {
                MessageBox.Show("Date can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DBAccess.data.Tables["ORDERS"].Rows.Add(newRow);
            int db_id = DBAccess.insertOrders();
            newRow[0] = db_id;


            //adding products to order
            if(!DBAccess.attachProductsToOrder(db_id, products))
                MessageBox.Show("Attention: your order was created, but it is empty yet! Don't forget to add products.",
                    "Attention!", MessageBoxButton.OK, MessageBoxImage.Information);
            this.Close();
        }

       

        #region Add/remove product actions
        private void btn_AddProduct_Click(object sender, RoutedEventArgs e)
        {
            AddProductToOrder addProductWindow = new AddProductToOrder();
            addProductWindow.ShowDialog();
            int productId = addProductWindow.ProductID;
            int count = addProductWindow.Count;
            if (productId != -1 && count > 0) //product was selected
            {
                DataRow sourceRow = DBAccess.data.Tables["PRODUCTS"].Select($"PRODUCTS_ID = {productId}").FirstOrDefault();
                if (sourceRow != null)
                {
                    DataRow dr = products.NewRow();
                    dr["PRODUCTS_ID"] = sourceRow["PRODUCTS_ID"];
                    dr["PRODUCTS_NAME"] = sourceRow["PRODUCTS_NAME"];
                    dr["PRICE"] = sourceRow["PRICE"];
                    dr["COUNT"] = count;
                    products.Rows.Add(dr);
                    TotalPrice += Convert.ToInt32(dr["PRICE"]) * count;
                }
            }
        }

        private void btn_RemoveProduct_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Products.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg_Products.SelectedItem;
                DataRow dr = curRow.Row;
                TotalPrice -= Convert.ToInt32(dr["PRICE"]) * Convert.ToInt32(dr["COUNT"]);
                products.Rows.Remove(dr);
                
            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        #endregion
    }
}
