﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Orders
{
    /// <summary>
    /// Interaction logic for EditOrder.xaml
    /// </summary>
    public partial class EditOrder : Window, INotifyPropertyChanged
    {
        private DataRow row;
        private DataTable products;
        private Decimal totalPrice;
        public Decimal TotalPrice
        {
            get { return totalPrice; }
            set
            {
                totalPrice = value;
                NotifyPropertyChanged("TotalPrice");
            }
        }
        private EditOrder()
        {
            InitializeComponent();
        }

        public EditOrder(DataRow dr)
        {
            InitializeComponent();
            row = dr;
            this.DataContext = row;
            dg_Products.ItemsSource = initializeProductsTable();
            txtTotalCosts.DataContext = this;
            TotalPrice = getTotalCosts();
            cboxClient.ItemsSource = DBAccess.data.Tables["CLIENTS"].DefaultView;
            cboxClient.DisplayMemberPath = "CLIENTS_NAME";
            cboxClient.SelectedValuePath = "CLIENTS_ID";

        }

        private decimal getTotalCosts()
        {
            return products.AsEnumerable().Sum(x => (x.Field<decimal>(2) * x.Field<int>(3)));
        }

        private DataView initializeProductsTable()
        {
            int order_id = Convert.ToInt32(row[0]);
            products = DBAccess.createOrderProductsTmpDataTable(order_id);
            return products.DefaultView;
        }

        private void btn_edit_Click(object sender, RoutedEventArgs e)
        {
            string description = txtDescription.Text;
            row[1] = description;

            int clientID;
            if (cboxClient.SelectedValue != null)
            {
                clientID = Convert.ToInt32(cboxClient.SelectedValue);
                row[4] = clientID;
            }
            else
            {
                MessageBox.Show("Client id can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            decimal totalCosts = -1;
            Decimal.TryParse(txtTotalCosts.Text, out totalCosts);
            if (totalCosts != -1 && totalCosts > 0)
            {
                row[3] = totalCosts;
            }
            else
            {
                MessageBox.Show("Total costs can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DateTime date = txtDate.DisplayDate;
            if (date != null)
            {
                row[2] = date;
            }
            else
            {
                MessageBox.Show("Date can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DBAccess.updateOrders();

            //editing products list of current order:
            int orderID = Convert.ToInt32(row[0]);
            DBAccess.detachAllProductsFromOrder(orderID);
            DBAccess.attachProductsToOrder(orderID, products);

            this.Close();
        }

        #region Add/remove product actions
        private void btn_AddProduct_Click(object sender, RoutedEventArgs e)
        {
            AddProductToOrder addProductWindow = new AddProductToOrder();
            addProductWindow.ShowDialog();
            int productId = addProductWindow.ProductID;
            int count = addProductWindow.Count;
            if (productId != -1 && count > 0) //product was selected
            {
                DataRow sourceRow = DBAccess.data.Tables["PRODUCTS"].Select($"PRODUCTS_ID = {productId}").FirstOrDefault();
                if (sourceRow != null)
                {
                    DataRow dr = products.NewRow();
                    dr["PRODUCTS_ID"] = sourceRow["PRODUCTS_ID"];
                    dr["PRODUCTS_NAME"] = sourceRow["PRODUCTS_NAME"];
                    dr["PRICE"] = sourceRow["PRICE"];
                    dr["COUNT"] = count;
                    products.Rows.Add(dr);
                    TotalPrice += Convert.ToInt32(dr["PRICE"]) * count;
                }
            }
        }

        private void btn_RemoveProduct_Click(object sender, RoutedEventArgs e)
        {
            int index = dg_Products.SelectedIndex;
            if (index >= 0)
            {
                DataRowView curRow = (DataRowView)dg_Products.SelectedItem;
                DataRow dr = curRow.Row;
                TotalPrice -= Convert.ToInt32(dr["PRICE"]) * Convert.ToInt32(dr["COUNT"]);
                products.Rows.Remove(dr);

            }
            else
                MessageBox.Show("No row was selected!", "Nothing to edit", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
        #endregion


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        #endregion

       
    }
}
