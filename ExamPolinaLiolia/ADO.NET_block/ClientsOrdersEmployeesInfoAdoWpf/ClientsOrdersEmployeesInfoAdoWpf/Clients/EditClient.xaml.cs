﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Clients
{
    /// <summary>
    /// Interaction logic for EditClient.xaml
    /// </summary>
    public partial class EditClient : Window
    {
        private DataRow row;
        private EditClient()
        {
            InitializeComponent();
        }
        public EditClient(DataRow dr)
        {
            InitializeComponent();
            row = dr;
            this.DataContext = row;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            if (name != string.Empty)
            {
                row[1] = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            string phone = txtPhone.Text;
            row[2] = phone;
            string mail = txtMail.Text;
            row[3] = mail;

            DBAccess.updateClients();

            this.Close();
        }
    }
}
