﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Departments
{
    /// <summary>
    /// Interaction logic for EditDepartment.xaml
    /// </summary>
    public partial class EditDepartment : Window
    {
        private DataRow row;
        private EditDepartment()
        {
            InitializeComponent();
        }
        public EditDepartment(DataRow dr)
        {
            InitializeComponent();
            this.row = dr;
            this.DataContext = dr;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            if (name != string.Empty)
            {
                row[1] = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string region_info = txtRegion.Text;
            row[2] = region_info;

            DBAccess.updateDepartments();

            this.Close();
        }
    }
}
