﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientsOrdersEmployeesInfoAdoWpf
{
    public class EmployeesHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM EMPLOYEES";
        }

        public static string updateEmployees()
        {
            return @"
                UPDATE EMPLOYEES   
                SET
                   EMPLOYEES_NAME = @Name,
                   PHONE = @Phone,
                   MAIL = @Mail,
                   SALARY = @Salary,                
                   DEPARTMENTS_ID = @DeptID
                WHERE
                    EMPLOYEES_ID = @ID
             ";
        }

        public static string insertEmployee()
        {
            return @"
                    INSERT INTO EMPLOYEES
                    (
                        EMPLOYEES_NAME,
                        PHONE,
                        MAIL,
                        SALARY,
                        DEPARTMENTS_ID
                    )
                    VALUES
                    (
                        @Name,
                        @Phone,
                        @Mail,
                        @Salary,
                        @DeptID
                    );";
        }

        public static string deleteEmployee()
        {
            return @"
                    DELETE FROM  EMPLOYEES 
					WHERE EMPLOYEES_ID = @ID;";
        }

    }

    public class DepartmentsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM  DEPARTMENTS";
        }

        public static string updateDepartments()
        {
            return @"
                    UPDATE DEPARTMENTS
                    SET
                        DEPARTMENTS_NAME = @Name,
                        REGION_INFO = @Region
                    WHERE 
                        DEPARTMENTS_ID = @ID;";
        } 

        public static string insertDepartment()
        {
            return @"
                    INSERT INTO  DEPARTMENTS
                    (
	                    DEPARTMENTS_NAME,
	                    REGION_INFO
                    )
                    VALUES
                    (
	                    @Name,
	                    @Region
                    );";
        }

        public static string deleteDepartment()
        {
            return @"
                    DELETE FROM  DEPARTMENTS
                    WHERE DEPARTMENTS_ID = @ID;";
        }
    }

    public class ClientsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM  CLIENTS";
        }

        public static string updateClients()
        {
            return @"
                    UPDATE  CLIENTS
                    SET 
	                    CLIENTS_NAME = @Name,
	                    CLIENTS_PHONE = @Phone,
	                    CLIENTS_MAIL = @Mail
                    WHERE
	                    CLIENTS_ID = @Id;";
        }

        public static string insertClient()
        {
            return @"
                    INSERT INTO  CLIENTS
                    (
	                    CLIENTS_NAME,
	                    CLIENTS_PHONE,
	                    CLIENTS_MAIL
                    )
                    VALUES
                    (
	                    @Name,
	                    @Phone,
	                    @Mail
                    );";
        }

        public static string deleteClient()
        {
            return @"
                    DELETE FROM  CLIENTS
                    WHERE
	                    CLIENTS_ID = @Id;";
        }
    }

    public class OrdersHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM  ORDERS";
        }

        public static string updateOrders()
        {
            return @"
                    UPDATE  ORDERS
                    SET 
	                    DESCRIPTION = @Description,
	                    ORDERS_DATE = @Date,
	                    TOTAL_COSTS = @TotalCosts,
	                    CLIENTS_ID = @ClientsId
                    WHERE ORDERS_ID = @Id;";
        }

        public static string insertOrder()
        {
            return @"
                    INSERT INTO  ORDERS
                    (
	                    DESCRIPTION,
	                    ORDERS_DATE,
	                    TOTAL_COSTS,
	                    CLIENTS_ID
                    )
                    VALUES
                    (
	                    @Description,
	                    @Date,
	                    @TotalCosts,
	                    @ClientsId
                    );";
        }

        public static string deleteOrder()
        {
            return @"
                    DELETE FROM  Orders
                    WHERE ORDERS_ID = @Id;";
        }
    }

    public class ProductsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM  PRODUCTS";
        }

        public static string updateProducts()
        {
            return @"
                    UPDATE  PRODUCTS
                    SET
	                    PRODUCTS_NAME = @Name,
	                    PRICE = @Price
                    WHERE 
	                    PRODUCTS_ID = @ID;";
        }

        public static string insertProduct()
        {
            return @"
                    INSERT INTO  PRODUCTS
                    (
	                    PRODUCTS_NAME,
	                    PRICE
                    )
                    VALUES
                    (
	                    @Name,
	                    @Price
                    );";
        }
        public static string deleteProduct()
        {
            return @"
                    DELETE FROM  PRODUCTS
                    WHERE 
	                    PRODUCTS_ID = @ID;";
        }
    }

    public class BanksHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM  BANKS";
        }

        public static string updateBanks()
        {
            return @"
                    UPDATE  BANKS
                    SET
	                    BANKS_NAME = @Name,
	                    BANKS_INFO = @Region
                    WHERE 
	                    BANKS_ID = @ID;";
        }

        public static string insertBank()
        {
            return @"
                    INSERT INTO  BANKS
                    (
	                    BANKS_NAME,
	                    BANKS_INFO
                    )
                    VALUES
                    (
	                    @Name,
	                    @Region
                    );";
        }

        public static string deleteBank()
        {
            return @"
                    DELETE FROM  BANKS
                    WHERE 
	                    BANKS_ID = @ID;";
        }
    }

    public class AccountsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM  ACCOUNTS";
        }

        public static string updateAccounts()
        {
            return @"UPDATE ACCOUNTS SET 
	                    DESCRIPTION = @Description,
	                    BANKS_ID = @Banks_id,
	                    ACCOUNTS_SUM = @Accounts_sum,
	                    ACCOUNT = @Account
                    WHERE
	                    ACCOUNTS_ID = @ID;";
        }

        public static string insertAccount()
        {
            return @"
                    INSERT INTO  ACCOUNTS
                    (
	                    DESCRIPTION,
	                    BANKS_ID,
	                    ACCOUNTS_SUM,
	                    ACCOUNT
                    )
                    VALUES
                    (
	                    @Description,
	                    @Banks_id,
	                    @Accounts_sum,
	                    @Account
                    );";
        }

        public static string deleteAccount()
        {
            return @"
                    DELETE FROM  ACCOUNTS
                    WHERE
	                    ACCOUNTS_ID = @ID;";
        }
    }

    public class OrdersPositionsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM  ORDERS_POSITIONS";
        }

        public static string updateOrdersPositions()
        {
            return @"
                    UPDATE  ORDERS_POSITIONS
                    SET 
	                    ORDERS_ID = @Orders_id,
	                    PRODUCTS_ID = @Products_id,
	                    PRICE = @Price,
	                    ITEM_COUNT = @Item_count
                    WHERE 
	                    ORDERS_POSITIONS_ID = @ID;";
        }

        public static string insertOrderPosition()
        {
            return @"
                    INSERT INTO  ORDERS_POSITIONS
                    (
	                    ORDERS_ID,
	                    PRODUCTS_ID,
	                    PRICE,
	                    ITEM_COUNT
                    )
                    VALUES
                    (
	                    @Orders_id,
	                    @Products_id,
	                    @Price,
	                    @Item_count
                    );";
        }

        public static string deleteOrderPosition()
        {
            return @"
                    DELETE FROM ORDERS_POSITIONS
                    WHERE 
	                    ORDERS_POSITIONS_ID = @ID;";
        }
    }

    public class AccountsToClientsHelper
    {
        public static string selectAll()
        {
            return "SELECT * FROM  ACCOUNTS_TO_CLIENTS";
        }

        public static string updateAccountsToClients()
        {
            return @"
                    UPDATE  ACCOUNTS_TO_CLIENTS
                    SET 
	                    ACCOUNTS_ID = @Accounts_id,
	                    CLIENTS_ID = @Clients_id,
	                    INDICATION = @Indication
                    WHERE 
	                    ACCOUNTS_TO_CLIENTS_ID = @ID;";
        }

        public static string insertAccountsToClients()
        {
            return @"
                    INSERT INTO  ACCOUNTS_TO_CLIENTS
                    (
	                    ACCOUNTS_ID,
	                    CLIENTS_ID,
	                    INDICATION
                    )
                    VALUES
                    (
	                    @Accounts_id,
	                    @Clients_id,
	                    @Indication
                    );";
        }

        public static string deleteAccountsToClients()
        {
            return @"DELETE FROM  ACCOUNTS_TO_CLIENTS
                    WHERE 
	                    ACCOUNTS_TO_CLIENTS_ID = @ID;";
        }
    }
}
