﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Accounts
{
    /// <summary>
    /// Interaction logic for EditAccount.xaml
    /// </summary>
    public partial class EditAccount : Window
    {
        private DataRow dr;

        private EditAccount()
        {
            InitializeComponent();
        }

        public EditAccount(DataRow dr)
        {
            InitializeComponent();
            this.dr = dr;
            this.DataContext = this.dr;
            cboxBank.ItemsSource = DBAccess.data.Tables["BANKS"].DefaultView;
            cboxBank.DisplayMemberPath = "BANKS_NAME";
            cboxBank.SelectedValuePath = "BANKS_ID";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            dr[1] = txtDescr.Text;

            int bankID;
            if (cboxBank.SelectedValue != null)
            {
                bankID = Convert.ToInt32(cboxBank.SelectedValue);
                dr[2] = bankID;
            }
            else
            {
                MessageBox.Show("Bank can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            decimal sum = -1;
            decimal.TryParse(txtSum.Text, out sum);
            if (sum != -1 && sum > 0)
            {
                dr[3] = sum;
            }
            else
            {
                MessageBox.Show("Sum can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            int account = -1;
            if (txtAccount.Text != string.Empty)
            {
                Int32.TryParse(txtAccount.Text, out account);
                if (account >= 0)
                    dr[4] = account;
                else
                {
                    MessageBox.Show("Wrong account!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Account can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DBAccess.updateAccounts();
            
            this.Close();
        }
    }
}
