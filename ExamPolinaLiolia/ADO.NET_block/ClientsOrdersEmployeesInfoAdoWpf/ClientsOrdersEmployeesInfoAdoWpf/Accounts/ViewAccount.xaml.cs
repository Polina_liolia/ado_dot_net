﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Accounts
{
    /// <summary>
    /// Interaction logic for ViewAccount.xaml
    /// </summary>
    public partial class ViewAccount : Window
    {
        private DataRow dr;

        private ViewAccount()
        {
            InitializeComponent();
        }

        public ViewAccount(DataRow dr)
        {
            InitializeComponent();
            this.dr = dr;
            this.DataContext = this.dr;
        }
    }
}
