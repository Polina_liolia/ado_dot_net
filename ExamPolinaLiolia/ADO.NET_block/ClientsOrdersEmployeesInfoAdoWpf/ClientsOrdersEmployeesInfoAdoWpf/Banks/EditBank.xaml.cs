﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Banks
{
    /// <summary>
    /// Interaction logic for EditBank.xaml
    /// </summary>
    public partial class EditBank : Window
    {
        private DataRow dr;

        private EditBank()
        {
            InitializeComponent();
        }

        public EditBank(DataRow dr)
        {
            InitializeComponent();
            this.dr = dr;
            this.DataContext = this.dr;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            if (name != string.Empty)
            {
                dr[1] = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            dr[2] = txtInfo.Text;

            DBAccess.updateBanks();
            
            this.Close();
        }
    }
}
