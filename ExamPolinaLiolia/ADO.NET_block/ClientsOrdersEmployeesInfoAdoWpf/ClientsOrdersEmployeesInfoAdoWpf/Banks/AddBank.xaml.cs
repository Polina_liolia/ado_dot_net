﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Banks
{
    /// <summary>
    /// Interaction logic for AddBank.xaml
    /// </summary>
    public partial class AddBank : Window
    {
        public AddBank()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DataRow newRow = DBAccess.data.Tables["BANKS"].NewRow();
            newRow[0] = -1; //temporary id

            string name = txtName.Text;
            if (name != string.Empty)
            {
                newRow[1] = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            newRow[2] = txtInfo.Text;

            //adding row to dataset (with temporary id = -1)
            DBAccess.data.Tables["BANKS"].Rows.Add(newRow);
            //updating database (id is generating with identity autoincrement)
            int db_id = DBAccess.insertBanks();
            //changing id value in dataset
            newRow[0] = db_id;
            this.Close();
        }
    }
}
