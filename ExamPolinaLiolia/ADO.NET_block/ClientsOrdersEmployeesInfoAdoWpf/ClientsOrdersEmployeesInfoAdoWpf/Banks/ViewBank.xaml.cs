﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Banks
{
    /// <summary>
    /// Interaction logic for ViewBank.xaml
    /// </summary>
    public partial class ViewBank : Window
    {
        private DataRow dr;

        private ViewBank()
        {
            InitializeComponent();
        }

        public ViewBank(DataRow dr)
        {
            InitializeComponent();
            this.dr = dr;
            this.DataContext = this.dr;
        }
    }
}
