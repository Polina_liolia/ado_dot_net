﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Products
{
    /// <summary>
    /// Interaction logic for AddProduct.xaml
    /// </summary>
    public partial class AddProduct : Window
    {
        public AddProduct()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DataRow newRow = DBAccess.data.Tables["PRODUCTS"].NewRow();
            newRow[0] = -1; //temporary id

            string name = txtName.Text;
            if (name != string.Empty)
            {
                newRow[1] = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            decimal price = 0;
            if (txtPrice.Text != string.Empty)
                price = Convert.ToDecimal(txtPrice.Text);
            else
            {
                MessageBox.Show("Price can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            newRow[2] = price;

            //adding row to dataset (with temporary id = -1)
            DBAccess.data.Tables["PRODUCTS"].Rows.Add(newRow);
            //updating database (id is generating with identity autoincrement)
            int db_id = DBAccess.insertProducts();
            //changing id value in dataset
            newRow[0] = db_id;
            this.Close();
        }
    }
}
