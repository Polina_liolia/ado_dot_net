﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Products
{
    /// <summary>
    /// Interaction logic for ViewProduct.xaml
    /// </summary>
    public partial class ViewProduct : Window
    {
        private ViewProduct()
        {
            InitializeComponent();
        }

        public ViewProduct(DataRow dr)
        {
            InitializeComponent();
            this.DataContext = dr;
        }
    }
}
