﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientsOrdersEmployeesInfoAdoWpf.Products
{
    /// <summary>
    /// Interaction logic for EditProduct.xaml
    /// </summary>
    public partial class EditProduct : Window
    {
        private DataRow dr;

        private EditProduct()
        {
            InitializeComponent();
        }

        public EditProduct(DataRow dr)
        {
            InitializeComponent();
            this.dr = dr;
            this.DataContext = this.dr;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            if (name != string.Empty)
            {
                dr[1] = name;
            }
            else
            {
                MessageBox.Show("Name can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            decimal price = 0;
            if (txtPrice.Text != string.Empty)
                price = Convert.ToDecimal(txtPrice.Text);
            else
            {
                MessageBox.Show("Price can not be empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            dr[2] = price;

            DBAccess.updateProducts();
            this.Close();
        }
    }
}
