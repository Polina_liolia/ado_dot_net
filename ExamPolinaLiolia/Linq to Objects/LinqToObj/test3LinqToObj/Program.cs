﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test3LinqToObj
{
    public class Program
    {
        static void Main(string[] args)
        {
            HowToOrderJoins source = new HowToOrderJoins();
            //для каждой категории вывести все товары, которые к ней относятся
            var query1 = from category in source.Categories
                         join product in source.Products
                         on category.ID equals product.CategoryID into profFromJoin
                         group profFromJoin by category.ID into groupResult
                         select groupResult;

            foreach (var group in query1)
            {
                Console.WriteLine($"{group.Key}:");
                foreach(var productList in group)
                {
                    foreach(var product in productList)
                        Console.WriteLine(product.Name);
                }
            }

        }
    }
}
