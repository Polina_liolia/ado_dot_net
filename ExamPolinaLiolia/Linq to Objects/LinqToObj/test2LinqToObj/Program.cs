﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test2LinqToObj
{
    class Program
    {
        static void Main(string[] args)
        {
            LeftOuterJoinExample();
        }

        public static void LeftOuterJoinExample()
        {
            Person magnus = new Person { FirstName = "Magnus", LastName = "Hedlund" };
            Person terry = new Person { FirstName = "Terry", LastName = "Adams" };
            Person charlotte = new Person { FirstName = "Charlotte", LastName = "Weiss" };
            Person arlene = new Person { FirstName = "Arlene", LastName = "Huff" };

            Pet barley = new Pet { Name = "Barley", Owner = terry };
            Pet boots = new Pet { Name = "Boots", Owner = terry };
            Pet whiskers = new Pet { Name = "Whiskers", Owner = charlotte };
            Pet bluemoon = new Pet { Name = "Blue Moon", Owner = terry };
            Pet daisy = new Pet { Name = "Daisy", Owner = magnus };

            // Create two lists.
            List<Person> people = new List<Person> { magnus, terry, charlotte, arlene };
            List<Pet> pets = new List<Pet> { barley, boots, whiskers, bluemoon, daisy };

            // This code produces the following output:
            //
            // Magnus:         Daisy
            // Terry:          Barley
            // Terry:          Boots
            // Terry:          Blue Moon
            // Charlotte:      Whiskers
            // Arlene:
            var query = from man in people
                        join pet in pets
                        on man equals pet.Owner into mp
                        from p in mp.DefaultIfEmpty()
                        select new
                        {
                            FirstName = man.FirstName,
                            PetName = p == null ? "" : p.Name
                        };


            foreach (var v in query)
            {
                Console.WriteLine("{0,-15}{1}", v.FirstName + ":", v.PetName);
            }

        }
    }
}
