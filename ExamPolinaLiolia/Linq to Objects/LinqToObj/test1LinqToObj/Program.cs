﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test1LinqToObj
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomJoins source = new CustomJoins();
            //1.Вывести все продукты и их категории
            var query = from product in source.Products
                        join category in source.Categories
                        on product.CategoryID equals category.ID into pc
                        from c in pc.DefaultIfEmpty()
                        orderby product.Name
                        select new
                        {
                            Name = product.Name,
                            Category = c == null ? "(No category)" : c.Name
                        };
            Console.WriteLine("All products with categories:");
            foreach (var item in query)
                Console.WriteLine(item);

            Console.WriteLine();

            //2.Вывести продукты только из указанной категории
            string categoryFilter = "Vegetables";
            query = from product in source.Products
                    join category in source.Categories
                    on product.CategoryID equals category.ID
                    where category.Name.Equals(categoryFilter)
                    orderby product.Name
                    select new
                    {
                        Name = product.Name,
                        Category = category.Name
                    };

            Console.WriteLine("All products from Vegetables category:");
            foreach (var item in query)
                Console.WriteLine(item);

            Console.WriteLine();


            //3.Вывести только те продукты, для которых задана категория
            query = from product in source.Products
                    join category in source.Categories
                    on product.CategoryID equals category.ID into pc
                    from c in pc.DefaultIfEmpty()
                    where c != null
                    orderby product.Name
                    select new
                    {
                        Name = product.Name,
                        Category = c.Name
                    };

            Console.WriteLine("All products with category filled in:");
            foreach (var item in query)
                Console.WriteLine(item);

            Console.WriteLine();

            //4.Вывести только те продукты, для которых не задана категория
            var query1 = from product in source.Products
                         join category in source.Categories
                         on product.CategoryID equals category.ID into pc
                         from c in pc.DefaultIfEmpty()
                         where c==null
                         orderby product.Name
                         select new
                         {
                             Name = product.Name,
                         };

            Console.WriteLine("All products without category filled in:");
            foreach (var item in query1)
                Console.WriteLine(item);

            Console.WriteLine();
        }
    }
}
