﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace CF.ManyToMany
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SuperMarketContext db = new SuperMarketContext())
            {
                Product P1 = new Product();
                P1.ProductName = "Dell products";

                Vendor V1 = new Vendor();
                V1.VendorName = "Vivek";
                V1.VendorProducts.Add(P1);
                db.Vendor.Add(V1);
                db.SaveChanges();
                Console.WriteLine("Press any key...");
                Console.ReadKey();
            }
        }
    }
}
