﻿using System.Data.Entity;

namespace CF.ManyToMany
{
    public class SuperMarketContext : DbContext
    {
        //если не зарегистрировать конструктор, БД будет создана в localdb (View - SQL Server Object Explorer), 
        //в качестве имени БД будет использовано полное имя проекта (CF.ManyToMany.SuperMarketContext)
        public SuperMarketContext() : base("name=ConnString")
        {
                
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vendor>()
                .HasMany(v => v.VendorProducts)
                .WithMany(p => p.ProductVendors)
                .Map(
                m =>
                {
                    m.MapLeftKey("VendorId");
                    m.MapRightKey("ProductId");
                    m.ToTable("VendorProduct");
                });

        }

        public DbSet<Product> Product
        {
            get;
            set;
        }
        public DbSet<Vendor> Vendor
        {
            get;
            set;
        }
    }
}