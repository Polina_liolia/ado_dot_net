﻿using System.Collections.Generic;

namespace CF.ManyToMany
{
    public class Product
    {
        public Product()
        {
            ProductVendors = new List<Vendor>();
        }
        public virtual int ProductId
        {
            get;
            set;
        }
        public virtual string ProductName
        {
            get;
            set;
        }
        public virtual ICollection<Vendor> ProductVendors
        {
            get;
            set;
        }
    }
}