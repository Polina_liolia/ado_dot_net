﻿using System.Collections.Generic;

namespace CF.ManyToMany
{
    public class Vendor
    {
        public Vendor()
        {
            VendorProducts = new List<Product>();
        }

        public virtual int VendorId
        {
            get;
            set;
        }
        public virtual string VendorName
        {
            get;
            set;
        }
        public virtual ICollection<Product> VendorProducts
        {
            get;
            set;
        }
    }
}