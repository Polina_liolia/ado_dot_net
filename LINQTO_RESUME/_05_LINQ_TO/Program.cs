﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_LINQ_TO
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            // Построить запрос.
            // Разделение чисел на четные и нечетные.
            var query = from x in numbers
                        group x by x % 2 into partition
                        select new { Key = partition.Key, Count = partition.Count(), Group = partition };
            Console.WriteLine("Вариант 1");
            foreach (var item in query)
            {
                Console.WriteLine("mod2 == {0}", item.Key);
                Console.WriteLine("Count == {0}", item.Count);

                foreach (var number in item.Group)
                    Console.Write("{0}, ", number);

                Console.WriteLine("\n");
            }
            // Разделение чисел на четные и нечетные.
            var query1 = from x in numbers
                         group x by x % 2 into partition
                         where partition.Key == 0
                         select new { Key = partition.Key, Count = partition.Count(), Group = partition };

            Console.WriteLine("Вариант 2");
            foreach (var item in query1)
            {
                Console.WriteLine("mod2 == {0}", item.Key);
                Console.WriteLine("Count == {0}", item.Count);

                foreach (var number in item.Group)
                    Console.Write("{0}, ", number);

                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
