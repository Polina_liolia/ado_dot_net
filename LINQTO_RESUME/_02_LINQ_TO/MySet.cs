﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_LINQ_TO
{
    public static class MySet //static class + static methods = extension methods
    {
        public static IEnumerable<T> Where<T> (this IEnumerable<T> source, Func<T, bool> predicate)
        {
            Console.WriteLine("My Where method was called");
            return Enumerable.Where(source, predicate);
        }

        public static IEnumerable<R> Select<T,R>(this IEnumerable<T> source, Func<T, R> selector)
        {
            Console.WriteLine("My Select method was called");
            return Enumerable.Select(source, selector);
        }
    }
}
