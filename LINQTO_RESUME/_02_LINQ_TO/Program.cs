﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_LINQ_TO
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 1, 1, 2, 3, 5, 8, 23, 21, 34 };
            var query = from x in numbers
                       where x % 2 == 0
                       select x * 2;
            foreach (var item in query)
                Console.WriteLine(item);
        }
    }
}
