﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_LINQ_TO
{
    public class Employee
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
    }
}
