﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_LINQ_TO
{
    class Program
    {
        static void Main(string[] args)
        {
            var employees = new List<Employee>
            {
                new Employee {LastName = "Ivanov", FirstName = "Ivan"},
                new Employee {LastName = "Andreev", FirstName = "Andrew"},
                new Employee {LastName = "Petrov", FirstName = "Petr"}
            };

            var query = from emp in employees
                        let fullName = emp.FirstName + " " + emp.LastName // let - новый локальный идентификатор.
                        orderby fullName descending
                        select fullName;

            foreach (var person in query)
                Console.WriteLine(person);

            Console.WriteLine();

            var query1 = from x in Enumerable.Range(0, 10)
                         let innerRange = Enumerable.Range(0, 10)
                         from y in innerRange
                         select new { X = x, Y = y, Product = x * y };
            foreach (var item in query1)
                Console.WriteLine($"{item.X} * {item.Y} = {item.Product}");
        }
    }
}
