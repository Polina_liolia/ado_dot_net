﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_LINQ_TO
{
    public class EmployeeID
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
    public class EmployeeNationality
    {
        public string Id { get; set; }
        public string Nationality { get; set; }
    }
}
