﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using System.Data.Common;

namespace WpfDataGridForMasterDetail
{
    /// <summary>
    /// Interaction logic for EditEmployeeWindow.xaml
    /// </summary>
    public partial class EditEmployeeWindow : Window
    {
        public EditEmployeeWindow()
        {
            InitializeComponent();
        }
        public EditEmployeeWindow(DataRow dataRow1, DataSet ds)
        {
            InitializeComponent();
            this.DataContext = dataRow1;

            comboBox1.DisplayMemberPath = "DepartmentsName";
            comboBox1.ItemsSource = ds.Tables["Departments"].DefaultView;
            comboBox1.SelectedValuePath = "DepartmentsID";

        }
    }
}