﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SQLiteCodeFirst
{
    /// <summary>
    /// Interaction logic for EditDepartmantWindow.xaml
    /// </summary>
    public partial class EditDepartmantWindow : Window
    {
        public Department curDep { set; get; }

        public EditDepartmantWindow()
        {
            InitializeComponent();
        }
        public EditDepartmantWindow(Department dep)
        {
            InitializeComponent();
            this.curDep = dep;
            this.DataContext = this.curDep;
        }
        //Closing="Window_Closing"
        //избыточность - этот обработчик не назначен для элемента, а значит, не запустится.
        //эти действия выполнятся в MainWindow.xaml.cs
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.DialogResult == true)
            {
                using (MyDataModel db = new MyDataModel())
                {
                    try
                    {
                        Department editingDep = db.Departments.Find(curDep.DepartmentID);
                        editingDep.DepartmentName = curDep.DepartmentName;
                        editingDep.RegionInfo = curDep.RegionInfo;
                        db.Entry(editingDep).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        e.Cancel = true;
                    }
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

    }
}
