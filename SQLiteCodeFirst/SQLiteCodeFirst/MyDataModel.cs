namespace SQLiteCodeFirst
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    public class MyDataModel : DbContext
    {
        // �������� �������� ��� ������������� ������ ����������� "MyDataModel" �� ����� ������������  
        // ���������� (App.config ��� Web.config). �� ��������� ��� ������ ����������� ��������� �� ���� ������ 
        // "MySQLite3EmptyCodeFirstTest.MyDataModel" � ���������� LocalDb. 
        // 
        // ���� ��������� ������� ������ ���� ������ ��� ��������� ���� ������, �������� ������ ����������� "MyDataModel" 
        // � ����� ������������ ����������.
        public MyDataModel()
            : base("name=MyDataModel")
        {

        }
        // �������� DbSet ��� ������� ���� ��������, ������� ��������� �������� � ������. �������������� �������� 
        // � ��������� � ������������� ������ Code First ��. � ������ http://go.microsoft.com/fwlink/?LinkId=390109.
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<EMPLOYE> Employees { get; set; }
        public virtual DbSet<PRODUCTS> Products { get; set; }
    }

    //EF5 style:
    [Table("DEPARTMENTS")]
    public class Department
    {
        [Key, Column("DEPARTMENTS_ID")]
        public long DepartmentID { get; set; }
        [Column("DEPARTMENTS_NAME")]
        public string DepartmentName { get; set; }
        [Column("REGION_INFO")]
        public string RegionInfo { get; set; }
        [NotMapped]//�� ������� � ���� ������
        public string DepartmentsEmployeesDescription { get; set; }
        #region ������������� ��������
        [ForeignKey("DEPARTMENTS_ID")]//��� �������� � ������ EMPLOYEES
        public virtual ICollection<EMPLOYE> DepartmentsEmployees { get; set; }
        #endregion
    }
    [Table("EMPLOYEES")]
    public partial class EMPLOYE
    {
        [Key, Column("EMPLOYEES_ID")]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long EMPLOYEES_ID { get; set; }
        [Column("EMPLOYEES_NAME")]
        [StringLength(2147483647)]
        public string EMPLOYEES_NAME { get; set; }
        [Column("PHONE")]
        [StringLength(2147483647)]
        public string PHONE { get; set; }
        [Column("MAIL")]
        [StringLength(2147483647)]
        public string MAIL { get; set; }
        [Column("SALARY", TypeName = "real")]
        public double? SALARY { get; set; }
        //EMPLOYEES - Detail ��� ������ DEPARTMENT, �������� ������ �� ������
        [Column("DEPARTMENTS_ID")]
        public long? DEPARTMENTS_ID { get; set; }
        
        #region ������������� ��-�� - ��������� One-To-Many
        //����� master; Department - Master; �������� ��������� detail 
        public Department Department { get; set; }
        #endregion

        [NotMapped]//�� ������� � ���� ������
        public string DepartmentsName { get; set; }
    }
    [Table("CLIENTS")]
    public class Client
    {
        [Key, Column("CLIENTS_ID")]
        public long ClientID { get; set; }
        [Column("CLIENTS_NAME")]
        public string Name { get; set; }
        [Column("CLIENTS_PHONE")]
        public string Phone { get; set; }
        [Column("CLIENTS_MAIL")]
        public string Mail { get; set; }
        [ForeignKey("ClientID")]
        public virtual ICollection<Order> Orders { get; set; }
    }
    [Table("ORDERS")]
    public class Order
    {
        [Key, Column("ORDERS_ID")]
        public long OrderID { get; set; }
        [Column("DESCRIPTION")]
        public string Description { get; set; }
        [Column("ORDERS_DATE")]
        public DateTime OrderDateTime { get; set; }
        [Column("TOTAL_COSTS")]
        public decimal TotalCost { get; set; }
        [Column("CLIENTS_ID")]
        public long? ClientID { get; set; }
        public virtual Client Client { get; set; }
        [ForeignKey("ORDERS_ID")]
        public virtual ICollection<ORDERS_POSITIONS> Order_Details { get; set; }
    }
    [Table("ORDERS_POSITIONS")]
    public partial class ORDERS_POSITIONS
    {
        [Key, Column("ORDERS_POSITIONS_ID", Order = 0)]
        public long ORDERS_POSITIONS_ID { get; set; }
        [Key, Column("ORDERS_ID", Order = 1)]
        public long? ORDERS_ID { get; set; }
        public virtual Order Order { get; set; }

        [Key, Column("PRODUCTS_ID", Order = 2)]
        public long? PRODUCTS_ID { get; set; }
        public virtual PRODUCTS Product { get; set; }

        [Column("PRICE", TypeName = "real")]
        public double? PRICE { get; set; }
        [Column("ITEM_COUNT")]
        public long? ITEM_COUNT { get; set; }
    }
    [Table("PRODUCTS")]
    public partial class PRODUCTS
    {
        [Key, Column("PRODUCTS_ID")]
        public long PRODUCTS_ID { get; set; }
        [Column("PRODUCTS_NAME")]
        [StringLength(2147483647)]
        public string PRODUCTS_NAME { get; set; }
        [Column("PRICE", TypeName = "real")]
        public double? PRICE { get; set; }
        //������, � ������� ������������ ���� �����
        public virtual ICollection<ORDERS_POSITIONS> OrdersLines { get; set; }
    }
}