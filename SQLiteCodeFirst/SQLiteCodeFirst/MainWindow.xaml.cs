﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SQLiteCodeFirst
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MyDataModel _entities = new MyDataModel();

        public MainWindow()
        {
            InitializeComponent();
            Refresh();
            testDepartments();
            testEmployees();
            testOrders();
            testProducts();
        }

        private void testProducts()
        {
            _entities.Products.Load();
            dgProducts.ItemsSource = _entities.Products.ToList<PRODUCTS>();

            foreach (PRODUCTS p in _entities.Products)
            {
                StringBuilder sb = new StringBuilder();
                foreach (ORDERS_POSITIONS op in p.OrdersLines)
                {
                    sb.Append(String.Format("product {0} in order {1}", p.PRODUCTS_NAME, op.Order.Description));
                }
            }
        }

        private void testEmployees()
        {
            _entities.Employees.Load();

            List<EMPLOYE> emps = new List<EMPLOYE>();

            foreach (var e in _entities.Employees)
            {
                e.DepartmentsName = e.Department.DepartmentName;
                emps.Add(e);
            }
            this.dgEmps.ItemsSource = emps;
        }

        private void Refresh()
        {
            _entities?.Dispose();
            _entities = new MyDataModel();
            _entities.Database.Log = (s => System.Diagnostics.Debug.WriteLine(s));
        }

        private void testDepartments()
        {
            _entities.Departments.Load();
            List<Department> deps = new List<Department>();
            foreach (var d in _entities.Departments)
            {
                var empls = d.DepartmentsEmployees;
                foreach (var e in empls)
                {
                    d.DepartmentsEmployeesDescription += e.EMPLOYEES_NAME;
                }
                deps.Add(d);
            }
            this.dgDeps.ItemsSource = deps;
        }

        private void testOrders()
        {
            _entities.Clients.Load();
            dgOrders.ItemsSource = _entities.Orders.ToList<Order>();

            List<PRODUCTS> productsList = new List<PRODUCTS>();
            foreach (Order o in _entities.Orders)
            {
                foreach (ORDERS_POSITIONS pos in o.Order_Details)
                {
                    PRODUCTS productFromQuery = _entities.Products.Where(p => p.PRODUCTS_ID == pos.PRODUCTS_ID).FirstOrDefault();
                    productsList.Add(productFromQuery);
                    PRODUCTS productFromOrdersDetails = pos.Product;
                }
            }
        }

        private void dgDeps_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            int rowIndex = dgDeps.SelectedIndex;
            Department curDep = (Department)dgDeps.SelectedItem;
            //EditDepartmantWindow form = new EditDepartmantWindow(curDep);
            //form.ShowDialog();

            EditDepartmantWindow editForm = new EditDepartmantWindow(curDep);
            editForm.Title = "редактирование сведений об отделе";
            // Обработчик закрытия формы
            editForm.Closing += delegate (object fSender, System.ComponentModel.CancelEventArgs fe)
            {
                if (editForm.DialogResult == true)
                {
                    try
                    {
                        using (MyDataModel db = new MyDataModel())
                        {
                            Department editingDep = db.Departments.Find(curDep.DepartmentID);
                            editingDep.DepartmentName = curDep.DepartmentName;
                            editingDep.RegionInfo = curDep.RegionInfo;
                            db.Entry(editingDep).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        // отображаем ошибку
                        MessageBox.Show(ex.Message, "Error");
                        // не закрываем форму для возможности исправления ошибки
                        fe.Cancel = true;
                    }
                }
            };
            // показываем модальную форму
            editForm.ShowDialog();
        }

        private void dgDeps_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Department curRow = (Department)e.AddedItems[0];
            List<EMPLOYE> empl = _entities.Departments.//вся коллекция Подразделений
                Find(curRow.DepartmentID).//нашли выделенное в гриде
                DepartmentsEmployees.ToList(); //получили список сотрудников
            dgEmps.ItemsSource = empl;
        }
    }
}
