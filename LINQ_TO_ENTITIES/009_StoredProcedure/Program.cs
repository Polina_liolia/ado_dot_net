﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdventureWorksModel;
using System.Data.SqlClient;

namespace _009_StoredProcedure
{
    class Program
    {
        static void Main(string[] args)
        {
            //запуск функции "вручную"
            using (var context = new AdventureWorksLT2012Entities())
            {
                Console.WriteLine(context.Database.Connection.ConnectionString);
                Console.WriteLine("------------------------------------");
                SqlParameter parameter = new SqlParameter("@id_c", 1);
                //функция возвращает таблицу со столбцами CustomerID, FirstName, LastName, что не совпадает со структурой Customer
                //поэтому тип возвращаемого значения - ufnGetCustomerInformation_Result 
                //это класс, сгенерированный EF исходя из того, как описано возвращаемое значение данной функции в БД
                var customers = context.Database.
                    SqlQuery<ufnGetCustomerInformation_Result>("select * from [dbo].[ufnGetCustomerInformation](@id_c)", parameter);
                foreach (var item in customers)
                {
                    Console.WriteLine("{0}.{1} - {2}", item.CustomerID, item.FirstName, item.LastName);
                }
                Console.WriteLine("Press any key...");
                Console.ReadKey();
            }
        }
    }
}
