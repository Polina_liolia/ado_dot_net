﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdventureWorksModel;

namespace _006_LazyLoad
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new AdventureWorksLT2012Entities())
            {
                context.Configuration.LazyLoadingEnabled = true; //Code-First defaults: true, DB/ModelFirst - See EDMX Props
                //SELECT
                //...
                //FROM [SalesLT].[SalesOrderHeader] AS [Extent1]
                var query = from order in context.SalesOrderHeaders
                            select order;
                Console.WriteLine(query);
                var ordersList = query.ToList();
                //если установлено LazyLoadingEnabled = true, то коллекция создается, но только из 
                //объектов-заместителей, каждый из которых умеет грузить себя из базы самостоятельно
                //загрузка прохоисходит при первом фактическом обращении к данным
                //это механизм создания лёгких клиентов
                foreach (var order in ordersList)
                {
                    // На каждой итерации обращение к БД!!!!
                    if (order.Customer != null)
                        //exec sp_executesql N'SELECT 
                        //...
                        //FROM [SalesLT].[Customer] AS [Extent1]
                        //WHERE [Extent1].[CustomerID] = @EntityKeyValue1',N'@EntityKeyValue1 int',@EntityKeyValue1=30102
                        Console.WriteLine(order.Customer.LastName);
                }
            }

        }
    }
}
