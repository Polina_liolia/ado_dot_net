﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdventureWorksModel;

namespace _02_LocalQuery
{
    static class Program
    {
        //Методы расширений LINQ могут возвращать два объекта: IEnumerable и IQueryable.
        //Интерфейс IEnumerable находится в пространстве имен System.Collections. 
        //Интерфейс IQueryable располагается в пространстве имен System.Linq.
        static void Main()
        {
            IEnumerable<Customer> customers;

            using (var context = new AdventureWorksLT2012Entities())
                customers = context.GetCustomersFromLocal();

            // уже нет необходимости соединяться с базой данных
            foreach (var customer in customers)
                Console.WriteLine(customer.FirstName);
            int customersCount = customers.Count();

            Console.WriteLine("Total number of records:{0}", customersCount);
        }


        private static IEnumerable<Customer> GetCustomersFromLocal(this AdventureWorksLT2012Entities context)
        {
            IQueryable<Customer> query = from c in context.Customers select c;
            IEnumerable<Customer> customers = query.ToList(); //SELECT ... FROM [SalesLT].[Customer] AS [Extent1]
            return customers;
        }

    }
}
