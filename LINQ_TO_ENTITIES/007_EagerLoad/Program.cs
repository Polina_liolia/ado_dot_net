﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdventureWorksModel;

namespace _007_EagerLoad
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new AdventureWorksLT2012Entities())
            {
                //Eager - антоним для Lazy (энергичный)
                context.Configuration.LazyLoadingEnabled = false;

                var query = from order in context.SalesOrderHeaders.Include("Customer")
                            select order;
                //SELECT 
                //...
                //FROM  [SalesLT].[SalesOrderHeader] AS [Extent1]
                //INNER JOIN [SalesLT].[Customer] AS [Extent2] ON [Extent1].[CustomerID] = [Extent2].[CustomerID]
                //нужно выполнять асинхронно
                //в итоге мы фактически будем работать с LinqToObject
                //задача уведомления сервером MS SQL всех клиентов об изменении данных решается сервером приложений,
                //на котором запущен MS SQL Server
                //для ASP сервером приложений является IIS
                var ordersList = query.ToList();
                foreach (var order in ordersList)
                {
                    //Нет запросов к БД!!!
                    Console.WriteLine(order.Customer.LastName);
                }
            }

        }
    }
}
