﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdventureWorksModel;
using System.Data.Entity;

namespace _008_StrictLoad
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new AdventureWorksLT2012Entities())
            {
                context.Configuration.LazyLoadingEnabled = false;

                //здесь происходит загрузка содержимого Customers (обращение к БД)
                context.Customers.Load();

                var query = from order in context.SalesOrderHeaders
                            select order;
                //SELECT 
                //...
                //FROM  [SalesLT].[SalesOrderHeader] AS [Extent1]

                Console.WriteLine(query);
                var ordersList = query.ToList(); //здесь делается выборка всех заказов (обращение к БД)

                foreach (var order in ordersList)
                {
                    //Нет запросов к БД!!! т.к. все необходимые данные уже загружены
                    if (order.Customer != null)
                        Console.WriteLine(order.Customer.LastName);
                }
            }

        }
    }
}
