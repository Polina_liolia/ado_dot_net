﻿using AdventureWorksModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_First
{
    class Program
    {
        static void Main()
        {
            using (var context = new AdventureWorksLT2012Entities())
            {
                IQueryable<Customer> query = from c in context.Customers select c;

                //Customer firstCustomer = query.First(); //SELECT TOP (1) ... FROM [SalesLT].[Customer] AS [c] (Look @ profiler) //DURATOIN 0
                Customer firstCustomer = query.ToList().First(); //DURATOIN 44!!
                Console.WriteLine("First Customer: {0} {1}", firstCustomer.FirstName, firstCustomer.LastName);
            }
        }

    }
}
