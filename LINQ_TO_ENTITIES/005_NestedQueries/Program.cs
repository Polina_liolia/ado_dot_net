﻿using AdventureWorksModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_NestedQueries
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new AdventureWorksLT2012Entities())
            {
                var query = from customer in context.Customers
                            orderby customer.FirstName
                            select customer;

                Console.WriteLine(query);
                Console.WriteLine();

                var nestedQuery = from a in query
                                  where a.CustomerID < 10
                                  select a;
                Console.WriteLine(nestedQuery);

                //SELECT ...
                //FROM [SalesLT].[Customer] AS [Extent1]
                //WHERE [Extent1].[CustomerID] < 1000
                //ORDER BY [Extent1].[FirstName] ASC

                //несмотря на то, что при написании запросов был использован вложенный запрос, 
                //сформированный в результате запрос не имеет вложенности
                foreach (var customer in nestedQuery)//"пошли в базу"
                {
                    Console.WriteLine((int)customer.CustomerID);
                }

            }

        }

    }
}
