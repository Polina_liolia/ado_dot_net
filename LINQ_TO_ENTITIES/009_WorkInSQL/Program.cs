﻿using AdventureWorksModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_WorkInSQL
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new AdventureWorksLT2012Entities())
            {
                Console.WriteLine(context.Database.Connection.ConnectionString);
                Console.WriteLine("------------------------------------");
                var customers = context.Database.SqlQuery<Customer>("select * from [AdventureWorksLT2012].[SalesLT].[Customer]");
                foreach (var item in customers)
                {
                    Console.WriteLine("{0}.{1} - {2}", item.CustomerID, item.FirstName, item.LastName);
                }
                Console.WriteLine("Press any key...");
                Console.ReadKey();
            }

            //ДАЛЬШЕ ПРОСТО ПРИМЕР СИНТАКСИСА, НЕ КОМПИЛИРУЕТСЯ В ДАННОМ ПРОЕКТЕ

            //insert example
            //метод ExecuteSqlCommand() - предоставляет возможность 
            //удалять, обновлять уже существующие или вставлять новые записи;
            //возвращает количество затронутых командой строк;
            // вставка

            /*
            int inserted = db.Database.ExecuteSqlCommand("INSERT INTO Companies (Name) VALUES ('HTC')");
            Console.WriteLine(inserted);

            var query2 = db.Database.SqlQuery<Companies>("SELECT * FROM Companies");

            foreach (var item in query2)
            {
                Console.WriteLine("{0}.{1}", item.Id, item.Name);
            }
            

            */
        }
    }
}
