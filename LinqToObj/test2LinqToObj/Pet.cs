﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test2LinqToObj
{
    public class Pet
    {
        public string Name { get; set; }
        public Person Owner { get; set; }
    }
}
