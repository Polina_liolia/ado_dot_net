﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Ado.Net
using System.Data;
using System.Data.Common;
using System.Data.SQLite;

using System.Diagnostics;

namespace AdoTester
{
    class DBAccess
    {
        //DataView - представление данных - для интерфейсных компонентов
        //WinForms: DataGridView, ComboBox; WPF: GridView
        public static DataView GetDepartments()
        {
            //DataSet - отсоединенный режим работы в Ado.Net
            DataSet data;//"аналог" базы данных
            data = new DataSet("DB");
            data.Locale = System.Globalization.CultureInfo.InvariantCulture;
            #region Создание DataSet вручную
            /*
            //
            // Структуры таблиц в базе данных
            //
            DataTable table = new DataTable("Departments");
            table.Columns.Add("DepartmentsID", typeof(int));//
            table.Columns.Add("DepartmentsName", typeof(string));//
            table.Columns.Add("DepartmentsLocation", typeof(string));//
            //первичные ключи: и другие ограничения целостности
            DataColumn[] keys = new DataColumn[1];//если ключ составной
                                                  //на два и более столбцов,
                                                  //то размер массива другой - DataColumn[3];
            keys[0] = table.Columns["DepartmentsID"];
            //keys[1] = table.Columns["RegionsID"];
            table.PrimaryKey = keys;//первичные ключи - это массив
            table.Columns["DepartmentsName"].AllowDBNull = false;
            table.Columns["DepartmentsName"].Unique = true;
            table.Columns["DepartmentsName"].Caption = "Название Отдела";
            //
            //добавление данных (совпадаем с реальной базой данных)
            //
            table.Rows.Add(1, "Head Office", "New York");
            table.Rows.Add(2, "IT Dep", "Kharkiv");
            table.Rows.Add(3, "Accoun Dep", "London");
            data.Tables.Add(table);
            //*/
            #endregion
            #region Заполнение Data из базы данных
            ///*
            var connectionString = DBConfiguration.DbConnectionString;
            SQLiteConnection connection =
                new SQLiteConnection(connectionString);
            connection.Open();
            SQLiteDataAdapter masterDataAdapter = new
                SQLiteDataAdapter(DepartmentsSQLs.selectAllTxt(), 
                                   connection);
            masterDataAdapter.Fill(data, "Departments");
            //*/
            #endregion
            //data.WriteXml("dataset.dat");
            //data.ReadXml("dataset.dat");
            DataView dv = data.Tables["Departments"].DefaultView;
            return dv;
        }
        public static void InsertEmployee(DataSet ds)
        {
            // SQL to insert into customers
            #region фомирование тестов запросов
            string insert = @"
                INSERT into Employees
                (
                   EmployeesName,
                   DepartmantsID,
                   Job,
                   Salary
                )
                values
                (
                   @EmployeesName,
                   @DepartmantsID,
                   @Job,
                   @Salary
                )
             ";
            string getID = @"SELECT @@IDENTITY";
            #endregion
            // create connection
            SQLiteConnection conn =
            new SQLiteConnection(DBConfiguration.DbConnectionString);
            // Create data adapter
            SQLiteDataAdapter da = new SQLiteDataAdapter();
            // create commands
            SQLiteCommand insertCmd = new SQLiteCommand(insert,
                                                        conn);
            //SQLiteCommand getIDCmd = new SQLiteCommand(getID, conn);
            // map parameters
            SQLiteParameter parm1 = insertCmd.Parameters.Add("@EmployeesName", DbType.String, 100, "EmployeesName");
            parm1.SourceVersion = DataRowVersion.Current;

            SQLiteParameter parm2 = insertCmd.Parameters.Add("@Job", DbType.String, 100, "Job");
            parm2.SourceVersion = DataRowVersion.Current;
            SQLiteParameter parm3 = insertCmd.Parameters.Add("@Salary", DbType.Double, 0, "Salary");
            parm3.SourceVersion = DataRowVersion.Current;

            SQLiteParameter parm4 = insertCmd.Parameters.Add("@DepartmantsID", DbType.Int32, 0, "DepartmantsID");
            parm1.SourceVersion = DataRowVersion.Current;

            // EmployeeID
            //SQLiteParameter parm = updateCmd.Parameters.Add("@EmployeesID", DbType.Int32, 0, "EmployeesID");
            //parm.SourceVersion = DataRowVersion.Current;

            // Update database
            try
            {
                da.InsertCommand = insertCmd;
                // da.SelectCommand = getIDCmd;
                da.Update(ds, "Employees");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
            }
            finally
            {
                // close connection
                conn.Close();
            }
        }


    }
}
